EESchema Schematic File Version 4
LIBS:eLoad-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 5 6
Title ""
Date "2018-08-10"
Rev "0.1"
Comp "Peter Yin"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eload:USB_B_Micro J?
U 1 1 5B6C5A25
P 1850 1350
AR Path="/5B5FBC03/5B6C5A25" Ref="J?"  Part="1" 
AR Path="/5B6C0220/5B6C5A25" Ref="J501"  Part="1" 
F 0 "J501" H 1905 1817 50  0000 C CNN
F 1 "USB_B_Micro" H 1905 1726 50  0000 C CNN
F 2 "eload_fp:Amphenol_10118193-0001LF" H 2000 1300 50  0001 C CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/10118193.pdf" H 2000 1300 50  0001 C CNN
F 4 "~" H 750 0   50  0001 C CNN "Stuff"
F 5 "1.8A" H 750 0   50  0001 C CNN "Current"
F 6 "609-4616-1-ND" H 750 0   50  0001 C CNN "Digi-Key Part Number"
F 7 "Amphenol FCI" H 750 0   50  0001 C CNN "Manufacturer"
F 8 "10118193-0001LF" H 750 0   50  0001 C CNN "Manufacturer Part Number"
F 9 "-30°C ~ 80°C" H 750 0   50  0001 C CNN "Operating Temperature"
F 10 "649-10118193-0001LF " H 750 0   50  0001 C CNN "Mouser Part Number"
	1    1850 1350
	1    0    0    -1  
$EndComp
$Comp
L eload:Ferrite_Bead_Small L?
U 1 1 5B6C5A31
P 4150 1150
AR Path="/5B5FBC03/5B6C5A31" Ref="L?"  Part="1" 
AR Path="/5B6C0220/5B6C5A31" Ref="L501"  Part="1" 
F 0 "L501" V 3913 1150 50  0000 C CNN
F 1 "CIS41P600AE" V 4004 1150 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 4080 1150 50  0001 C CNN
F 3 "http://ds.yuden.co.jp/TYCOMPAS/ut/detail.do?productNo=FBMJ2125HM330-T&fileName=FBMJ2125HM330-T_SS&mode=specSheetDownload" H 4150 1150 50  0001 C CNN
F 4 "4A" H 800 50  50  0001 C CNN "Current"
F 5 "587-1766-1-ND" H 800 50  50  0001 C CNN "Digi-Key Part Number"
F 6 "Taiyo Yuden" H 800 50  50  0001 C CNN "Manufacturer"
F 7 "FBMJ2125HM330-T" H 800 50  50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 125°C" H 800 50  50  0001 C CNN "Operating Temperature"
F 9 "~" H 1200 0   50  0001 C CNN "Stuff"
F 10 "FBMJ2125HM330-T" H 1200 0   50  0001 C CNN "Mouser Part Number"
	1    4150 1150
	0    1    1    0   
$EndComp
$Comp
L eload:C C?
U 1 1 5B6C5A38
P 3850 1400
AR Path="/5B5FBC03/5B6C5A38" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B6C5A38" Ref="C501"  Part="1" 
F 0 "C501" H 3965 1446 50  0000 L CNN
F 1 "10nF" H 3965 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3888 1250 50  0001 C CNN
F 3 "~" H 3850 1400 50  0001 C CNN
F 4 "~" H 1200 0   50  0001 C CNN "Stuff"
F 5 "77-VJ0805Y103MXXCBC" H 1200 0   50  0001 C CNN "Mouser Part Number"
	1    3850 1400
	1    0    0    -1  
$EndComp
$Comp
L eload:C C?
U 1 1 5B6C5A3F
P 4450 1400
AR Path="/5B5FBC03/5B6C5A3F" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B6C5A3F" Ref="C502"  Part="1" 
F 0 "C502" H 4565 1446 50  0000 L CNN
F 1 "0.1uF" H 4565 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4488 1250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 4450 1400 50  0001 C CNN
F 4 "399-1177-1-ND" H 1200 0   50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 1200 0   50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 1200 0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 1200 0   50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 1200 0   50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 1200 0   50  0001 C CNN "Tolerance"
F 10 "50V" H 1200 0   50  0001 C CNN "Voltage"
F 11 "~" H 1200 0   50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 1200 0   50  0001 C CNN "Mouser Part Number"
	1    4450 1400
	1    0    0    -1  
$EndComp
$Comp
L eload:D_TVS D?
U 1 1 5B6C5A5A
P 2750 1400
AR Path="/5B6010A5/5B6C5A5A" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5B6C5A5A" Ref="D?"  Part="1" 
AR Path="/5B5FBC03/5B6C5A5A" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5B6C5A5A" Ref="D501"  Part="1" 
F 0 "D501" V 2659 1479 50  0000 L CNN
F 1 "824501500" V 2750 1479 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 2750 1400 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/824501500.pdf" H 2750 1400 50  0001 C CNN
F 4 "43.5A " H -5600 -3000 50  0001 C CNN "Current"
F 5 "732-9939-1-ND" H -5600 -3000 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -5600 -3000 50  0001 C CNN "Manufacturer"
F 7 "824501500" H -5600 -3000 50  0001 C CNN "Manufacturer Part Number"
F 8 "-65°C ~ 150°C" H -5600 -3000 50  0001 C CNN "Operating Temperature"
F 9 "400W " H -5600 -3000 50  0001 C CNN "Power"
F 10 "5V" V 2841 1479 50  0000 L CNN "Voltage"
F 11 "~" H 750 0   50  0001 C CNN "Stuff"
F 12 "710-824501500 " H 750 0   50  0001 C CNN "Mouser Part Number"
	1    2750 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 1250 3850 1150
Wire Wire Line
	3850 1150 4050 1150
Wire Wire Line
	4250 1150 4450 1150
Wire Wire Line
	5050 1150 5050 1250
Wire Wire Line
	4450 1250 4450 1150
Connection ~ 4450 1150
Wire Wire Line
	4450 1150 4750 1150
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5A6E
P 3850 1600
AR Path="/5B5FBC03/5B6C5A6E" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5A6E" Ref="#PWR0504"  Part="1" 
F 0 "#PWR0504" H 3850 1350 50  0001 C CNN
F 1 "GNDD" H 3855 1427 50  0000 C CNN
F 2 "" H 3850 1600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3850 1600 50  0001 C CNN
	1    3850 1600
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5A74
P 4450 1600
AR Path="/5B5FBC03/5B6C5A74" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5A74" Ref="#PWR0505"  Part="1" 
F 0 "#PWR0505" H 4450 1350 50  0001 C CNN
F 1 "GNDD" H 4455 1427 50  0000 C CNN
F 2 "" H 4450 1600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4450 1600 50  0001 C CNN
	1    4450 1600
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5A7A
P 5050 1600
AR Path="/5B5FBC03/5B6C5A7A" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5A7A" Ref="#PWR0506"  Part="1" 
F 0 "#PWR0506" H 5050 1350 50  0001 C CNN
F 1 "GNDD" H 5055 1427 50  0000 C CNN
F 2 "" H 5050 1600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 5050 1600 50  0001 C CNN
	1    5050 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1550 3850 1600
Wire Wire Line
	4450 1550 4450 1600
Wire Wire Line
	5050 1550 5050 1600
Wire Wire Line
	2750 1150 2750 1250
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5A84
P 2750 1600
AR Path="/5B5FBC03/5B6C5A84" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5A84" Ref="#PWR0503"  Part="1" 
F 0 "#PWR0503" H 2750 1350 50  0001 C CNN
F 1 "GNDD" H 2755 1427 50  0000 C CNN
F 2 "" H 2750 1600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2750 1600 50  0001 C CNN
	1    2750 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 1550 2750 1600
Wire Wire Line
	5150 1150 5150 1100
Wire Wire Line
	5050 1150 5150 1150
Connection ~ 5050 1150
Connection ~ 3850 1150
$Comp
L eload:Polyfuse F?
U 1 1 5B6C5A96
P 2500 1150
AR Path="/5B5FBC03/5B6C5A96" Ref="F?"  Part="1" 
AR Path="/5B6C0220/5B6C5A96" Ref="F501"  Part="1" 
F 0 "F501" V 2185 1150 50  0000 C CNN
F 1 "0ZCG0260BF2B" V 2276 1150 50  0000 C CNN
F 2 "eload_fp:Fuse_1812_4532Metric" H 2550 950 50  0001 L CNN
F 3 "https://www.belfuse.com/resources/CircuitProtection/datasheets/0ZCG%20Nov2016.pdf" H 2500 1150 50  0001 C CNN
F 4 "2.1A" V 2367 1150 50  0000 C CNN "Current"
F 5 "507-1780-1-ND" H -450 50  50  0001 C CNN "Digi-Key Part Number"
F 6 "Bel Fuse Inc." H -450 50  50  0001 C CNN "Manufacturer"
F 7 "0ZCG0260BF2B" H -450 50  50  0001 C CNN "Manufacturer Part Number"
F 8 "-40°C ~ 85°C" H -450 50  50  0001 C CNN "Operating Temperature"
F 9 "16V" H -450 50  50  0001 C CNN "Voltage"
F 10 "~" H 750 0   50  0001 C CNN "Stuff"
F 11 "530-0ZCG0110FF2C" H 750 0   50  0001 C CNN "Mouser Part Number"
	1    2500 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 1150 2350 1150
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5A9E
P 1750 1950
AR Path="/5B5FBC03/5B6C5A9E" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5A9E" Ref="#PWR0507"  Part="1" 
F 0 "#PWR0507" H 1750 1700 50  0001 C CNN
F 1 "GNDD" H 1755 1777 50  0000 C CNN
F 2 "" H 1750 1950 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1750 1950 50  0001 C CNN
	1    1750 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1750 1850 1800
Wire Wire Line
	1850 1800 1750 1800
Wire Wire Line
	1750 1800 1750 1750
Wire Wire Line
	1750 1800 1750 1950
Connection ~ 1750 1800
NoConn ~ 2150 1350
NoConn ~ 2150 1450
Wire Wire Line
	2750 1150 2650 1150
$Comp
L eload:LED D?
U 1 1 5B6C5AB6
P 3900 7150
AR Path="/5B5FBC03/5B6C5AB6" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5B6C5AB6" Ref="D505"  Part="1" 
F 0 "D505" V 3938 7032 50  0000 R CNN
F 1 "GREEN" V 3847 7032 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 3900 7150 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120VS75000.pdf" H 3900 7150 50  0001 C CNN
F 4 "20mA" H -1600 5150 50  0001 C CNN "Current"
F 5 "732-4993-1-ND" H -1600 5150 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -1600 5150 50  0001 C CNN "Manufacturer"
F 7 "150120VS75000" H -1600 5150 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H -1600 5150 50  0001 C CNN "Voltage"
F 9 "~" H 2850 1650 50  0001 C CNN "Stuff"
F 10 "710-156120VS75000" H 2850 1650 50  0001 C CNN "Mouser Part Number"
	1    3900 7150
	0    -1   -1   0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B6C5AC4
P 3900 6800
AR Path="/5B6010A5/5B6C5AC4" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B6C5AC4" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B6C5AC4" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B6C5AC4" Ref="R513"  Part="1" 
F 0 "R513" H 3832 6709 50  0000 R CNN
F 1 "1k" H 3832 6800 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3940 6790 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 3900 6800 50  0001 C CNN
F 4 "Yageo" V 3900 6800 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 3900 6800 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 3900 6800 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 3832 6891 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 3900 6800 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 3900 6800 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 3900 6800 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2850 1650 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H 2850 1650 50  0001 C CNN "Mouser Part Number"
	1    3900 6800
	-1   0    0    1   
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5ACB
P 3900 7350
AR Path="/5B5FBC03/5B6C5ACB" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5ACB" Ref="#PWR0541"  Part="1" 
F 0 "#PWR0541" H 3900 7100 50  0001 C CNN
F 1 "GNDD" H 3905 7177 50  0000 C CNN
F 2 "" H 3900 7350 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3900 7350 50  0001 C CNN
	1    3900 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 7350 3900 7300
Wire Wire Line
	3900 7000 3900 6950
Wire Wire Line
	3900 6550 3900 6650
$Comp
L eload:Q_PMOS_GSD Q501
U 1 1 5B6D4359
P 7900 1300
F 0 "Q501" V 8243 1300 50  0000 C CNN
F 1 "IRLML2246TRPBF" V 8152 1300 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8100 1400 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irlml2246pbf.pdf?fileId=5546d462533600a401535664c91f25f2" H 7900 1300 50  0001 C CNN
F 4 "IRLML2246TRPBFCT-ND" H 2300 0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Infineon Technologies" H 2300 0   50  0001 C CNN "Manufacturer"
F 6 "IRLML2246TRPBF" H 2300 0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 150°C (TJ)" H 2300 0   50  0001 C CNN "Operating Temperature"
F 8 "1.3W (Ta)" H 2300 0   50  0001 C CNN "Power"
F 9 "20V" H 2300 0   50  0001 C CNN "Voltage"
F 10 "~" H 2300 0   50  0001 C CNN "Stuff"
F 11 "755-RSC002P03T316" H 2300 0   50  0001 C CNN "Mouser Part Number"
	1    7900 1300
	0    1    -1   0   
$EndComp
$Comp
L eload:R_US R501
U 1 1 5B6D7338
P 7400 1450
F 0 "R501" H 7468 1541 50  0000 L CNN
F 1 "10k" H 7468 1450 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7440 1440 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 7400 1450 50  0001 C CNN
F 4 "DNS" H 7468 1359 50  0000 L CNN "Stuff"
F 5 " 541-3976-1-ND " H 2300 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 2300 0   50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H 2300 0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 2300 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 2300 0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 2300 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 2300 0   50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H 2300 0   50  0001 C CNN "Mouser Part Number"
	1    7400 1450
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R503
U 1 1 5B6D73B4
P 7900 1950
F 0 "R503" H 7968 1996 50  0000 L CNN
F 1 "100R" H 7968 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7940 1940 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7900 1950 50  0001 C CNN
F 4 "~" H 2300 0   50  0001 C CNN "Stuff"
F 5 "1276-5224-1-ND" H 2300 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Samsung Electro-Mechanics" H 2300 0   50  0001 C CNN "Manufacturer"
F 7 "RC2012F101CS" H 2300 0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 2300 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 2300 0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 2300 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 2300 0   50  0001 C CNN "Tolerance"
F 12 "756-GWCR0805-100RFT5" H 2300 0   50  0001 C CNN "Mouser Part Number"
	1    7900 1950
	1    0    0    -1  
$EndComp
$Comp
L eload:C C504
U 1 1 5B6D743B
P 8400 1400
F 0 "C504" H 8515 1446 50  0000 L CNN
F 1 "0.1uF" H 8515 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8438 1250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 8400 1400 50  0001 C CNN
F 4 "399-1177-1-ND" H 2450 -50 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 2450 -50 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 2450 -50 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 2450 -50 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 2450 -50 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 2450 -50 50  0001 C CNN "Tolerance"
F 10 "50V" H 2450 -50 50  0001 C CNN "Voltage"
F 11 "~" H 2450 -50 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 2450 -50 50  0001 C CNN "Mouser Part Number"
	1    8400 1400
	1    0    0    -1  
$EndComp
$Comp
L eload:C C505
U 1 1 5B6D74A7
P 7000 1450
F 0 "C505" H 7115 1541 50  0000 L CNN
F 1 "0.1uF" H 7115 1450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7038 1300 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 7000 1450 50  0001 C CNN
F 4 "399-1177-1-ND" H 2300 0   50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 2300 0   50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 2300 0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 2300 0   50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 2300 0   50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 2300 0   50  0001 C CNN "Tolerance"
F 10 "50V" H 2300 0   50  0001 C CNN "Voltage"
F 11 "DNS" H 7115 1359 50  0000 L CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 2300 0   50  0001 C CNN "Mouser Part Number"
	1    7000 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 1200 7400 1200
Wire Wire Line
	7000 1200 7000 1300
Wire Wire Line
	7400 1300 7400 1200
Connection ~ 7400 1200
Wire Wire Line
	7400 1200 7000 1200
Wire Wire Line
	7400 1600 7400 1700
Wire Wire Line
	7400 1700 7900 1700
Connection ~ 7900 1700
Wire Wire Line
	7000 1600 7000 1700
Wire Wire Line
	7000 1700 7400 1700
Connection ~ 7400 1700
$Comp
L eload:C C?
U 1 1 5B6E5FAB
P 1600 3400
AR Path="/5B6010A5/5B6E5FAB" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B6E5FAB" Ref="C?"  Part="1" 
AR Path="/5B5FBC06/5B6E5FAB" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B6E5FAB" Ref="C509"  Part="1" 
F 0 "C509" H 1715 3446 50  0000 L CNN
F 1 "0.1uF" H 1715 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1638 3250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 1600 3400 50  0001 C CNN
F 4 "KEMET" H 1600 3400 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 1600 3400 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 1600 3400 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 1600 3400 50  0001 C CNN "Tolerance"
F 8 "50V" H 1600 3400 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 1600 3400 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 1600 3400 50  0001 C CNN "Operating Temperature"
F 11 "~" H -200 650 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -200 650 50  0001 C CNN "Mouser Part Number"
	1    1600 3400
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B6E5FB2
P 4200 3250
AR Path="/5B6010A5/5B6E5FB2" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B6E5FB2" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B6E5FB2" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6E5FB2" Ref="#PWR0514"  Part="1" 
F 0 "#PWR0514" H 4200 3000 50  0001 C CNN
F 1 "GNDA" H 4205 3077 50  0000 C CNN
F 2 "" H 4200 3250 50  0001 C CNN
F 3 "" H 4200 3250 50  0001 C CNN
	1    4200 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2800 4200 2850
$Comp
L eload:GNDA #PWR?
U 1 1 5B6E5FBB
P 2800 3350
AR Path="/5B6010A5/5B6E5FBB" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B6E5FBB" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B6E5FBB" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6E5FBB" Ref="#PWR0515"  Part="1" 
F 0 "#PWR0515" H 2800 3100 50  0001 C CNN
F 1 "GNDA" H 2805 3177 50  0000 C CNN
F 2 "" H 2800 3350 50  0001 C CNN
F 3 "" H 2800 3350 50  0001 C CNN
	1    2800 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 2950 4000 2950
Wire Wire Line
	5500 2800 5500 2750
$Comp
L eload:R_US R?
U 1 1 5B6E5FD4
P 1600 2800
AR Path="/5B6E5FD4" Ref="R?"  Part="1" 
AR Path="/5B6010A5/5B6E5FD4" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B6E5FD4" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B6E5FD4" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B6E5FD4" Ref="R506"  Part="1" 
F 0 "R506" V 1713 2800 50  0000 C CNN
F 1 "10k" V 1804 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1640 2790 50  0001 L CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1600 2800 50  0001 L CNN
F 4 "±1%" H 1650 2700 50  0001 L CNN "Tolerance"
F 5 "±100ppm/°C" H 1600 2800 50  0001 L CNN "Temperature Coefficient"
F 6 "CRCW080510K0FKEAC" H 1600 2800 50  0001 L CNN "Manufacturer Part Number"
F 7 "Vishay Dale" H 1600 2800 50  0001 L CNN "Manufacturer"
F 8 " 541-3976-1-ND " H 1600 2800 50  0001 L CNN "Digi-Key Part Number"
F 9 "-55°C ~ 155°C" H 300 250 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W " H 300 250 50  0001 C CNN "Power"
F 11 "~" H -250 -350 50  0001 C CNN "Stuff"
F 12 "GWCR0805-10KFT5 " H -250 -350 50  0001 C CNN "Mouser Part Number"
	1    1600 2800
	0    1    1    0   
$EndComp
$Comp
L eload:C C?
U 1 1 5B6E5FE2
P 4200 3000
AR Path="/5B6010A5/5B6E5FE2" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B6E5FE2" Ref="C?"  Part="1" 
AR Path="/5B5FBC06/5B6E5FE2" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B6E5FE2" Ref="C506"  Part="1" 
F 0 "C506" H 4315 3046 50  0000 L CNN
F 1 "0.1uF" H 4315 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4238 2850 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 4200 3000 50  0001 C CNN
F 4 "KEMET" H 4200 3000 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 4200 3000 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 4200 3000 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 4200 3000 50  0001 C CNN "Tolerance"
F 8 "50V" H 4200 3000 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 4200 3000 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 4200 3000 50  0001 C CNN "Operating Temperature"
F 11 "~" H 750 -350 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 750 -350 50  0001 C CNN "Mouser Part Number"
	1    4200 3000
	1    0    0    -1  
$EndComp
$Comp
L eload:TestPoint TP?
U 1 1 5B6E5FE9
P 5150 2700
AR Path="/5B6010A5/5B6E5FE9" Ref="TP?"  Part="1" 
AR Path="/5B5FBD8A/5B6E5FE9" Ref="TP?"  Part="1" 
AR Path="/5B5FBC06/5B6E5FE9" Ref="TP?"  Part="1" 
AR Path="/5B6C0220/5B6E5FE9" Ref="TP501"  Part="1" 
F 0 "TP501" H 5150 3025 50  0000 C CNN
F 1 "+3V3A" H 5150 2934 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 5350 2700 50  0001 C CNN
F 3 "~" H 5350 2700 50  0001 C CNN
F 4 "~" H 1900 -300 50  0001 C CNN "Stuff"
	1    5150 2700
	1    0    0    -1  
$EndComp
$Comp
L eload:MAX6070BAUT33 U?
U 1 1 5B6E5FF2
P 3050 2650
AR Path="/5B5FBC06/5B6E5FF2" Ref="U?"  Part="1" 
AR Path="/5B6C0220/5B6E5FF2" Ref="U501"  Part="1" 
F 0 "U501" H 3425 2915 50  0000 C CNN
F 1 "MAX6070BAUT33" H 3425 2824 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 3150 2750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX6070-MAX6071.pdf" H 3150 2750 50  0001 C CNN
F 4 "MAX6070BAUT33+TCT-ND" H 750 -350 50  0001 C CNN "Digi-Key Part Number"
F 5 "Maxim Integrated" H 750 -350 50  0001 C CNN "Manufacturer"
F 6 "MAX6070BAUT33+T" H 750 -350 50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 125°C (TA)" H 750 -350 50  0001 C CNN "Operating Temperature"
F 8 "8ppm/°C" H 750 -350 50  0001 C CNN "Temperature Coefficient"
F 9 "±0.08%" H 750 -350 50  0001 C CNN "Tolerance"
F 10 "3.3V" H 750 -350 50  0001 C CNN "Voltage"
F 11 "~" H 750 -350 50  0001 C CNN "Stuff"
F 12 "700-MAX6070BAUT33T" H 750 -350 50  0001 C CNN "Mouser Part Number"
	1    3050 2650
	1    0    0    -1  
$EndComp
$Comp
L eload:+3V3_A #PWR?
U 1 1 5B6E5FF9
P 5500 2750
AR Path="/5B5FBC06/5B6E5FF9" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6E5FF9" Ref="#PWR0510"  Part="1" 
F 0 "#PWR0510" H 5500 2600 50  0001 C CNN
F 1 "+3V3_A" H 5505 2923 50  0000 C CNN
F 2 "" H 5500 2750 50  0001 C CNN
F 3 "" H 5500 2750 50  0001 C CNN
	1    5500 2750
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B6E6009
P 1000 3600
AR Path="/5B6010A5/5B6E6009" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B6E6009" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B6E6009" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6E6009" Ref="#PWR0518"  Part="1" 
F 0 "#PWR0518" H 1000 3350 50  0001 C CNN
F 1 "GNDA" H 1005 3427 50  0000 C CNN
F 2 "" H 1000 3600 50  0001 C CNN
F 3 "" H 1000 3600 50  0001 C CNN
	1    1000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 2800 1050 2750
$Comp
L eload:C C?
U 1 1 5B6E6022
P 2800 3150
AR Path="/5B6010A5/5B6E6022" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B6E6022" Ref="C?"  Part="1" 
AR Path="/5B5FBC06/5B6E6022" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B6E6022" Ref="C507"  Part="1" 
F 0 "C507" H 2685 3104 50  0000 R CNN
F 1 "0.1uF" H 2685 3195 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2838 3000 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 2800 3150 50  0001 C CNN
F 4 "KEMET" H 2800 3150 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 2800 3150 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 2800 3150 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 2800 3150 50  0001 C CNN "Tolerance"
F 8 "50V" H 2800 3150 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 2800 3150 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 2800 3150 50  0001 C CNN "Operating Temperature"
F 11 "~" H 650 -350 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 650 -350 50  0001 C CNN "Mouser Part Number"
	1    2800 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2800 3350 2800 3300
$Comp
L eload:MIC2288 U503
U 1 1 5B70FB06
P 7300 4000
F 0 "U503" H 7525 4165 50  0000 C CNN
F 1 "MIC2288" H 7525 4074 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:TSOT-23-5" H 7600 3300 50  0001 C CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en580187" H 7300 4000 50  0001 C CNN
F 4 "576-1729-1-ND" H -450 700 50  0001 C CNN "Digi-Key Part Number"
F 5 "Microchip Technology" H -450 700 50  0001 C CNN "Manufacturer"
F 6 "MIC2288YD5-TR" H -450 700 50  0001 C CNN "Manufacturer Part Number"
F 7 " -40°C ~ 125°C (TJ)" H -450 700 50  0001 C CNN "Operating Temperature"
F 8 "~" H -450 700 50  0001 C CNN "Stuff"
F 9 "998-MIC2288YD5TR" H -450 700 50  0001 C CNN "Mouser Part Number"
	1    7300 4000
	1    0    0    -1  
$EndComp
$Comp
L eload:D_Schottky D502
U 1 1 5B715E65
P 8350 3600
F 0 "D502" H 8350 3384 50  0000 C CNN
F 1 "DSS13UTR" H 8350 3475 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 8350 3600 50  0001 C CNN
F 3 "http://www.smc-diodes.com/propdf/DSS12U%20THRU%20DSS125U%20N1873%20REV.A.pdf" H 8350 3600 50  0001 C CNN
F 4 "1A" H -450 700 50  0001 C CNN "Current"
F 5 "1655-1926-1-ND" H -450 700 50  0001 C CNN "Digi-Key Part Number"
F 6 "SMC Diode Solutions" H -450 700 50  0001 C CNN "Manufacturer"
F 7 "DSS13UTR" H -450 700 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 150°C" H -450 700 50  0001 C CNN "Operating Temperature"
F 9 "30V" H -450 700 50  0001 C CNN "Voltage"
F 10 "~" H -450 700 50  0001 C CNN "Stuff"
F 11 "841-PMEG4010EGWX" H -450 700 50  0001 C CNN "Mouser Part Number"
	1    8350 3600
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R509
U 1 1 5B715FC9
P 8550 3950
F 0 "R509" H 8618 3996 50  0000 L CNN
F 1 "8k66" H 8618 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8590 3940 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 8550 3950 50  0001 C CNN
F 4 "~" H -450 700 50  0001 C CNN "Stuff"
F 5 "652-CR0805FX-8661ELF" H -450 700 50  0001 C CNN "Mouser Part Number"
F 6 "311-8.66KCRCT-ND" H -450 700 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H -450 700 50  0001 C CNN "Manufacturer"
F 8 "RC0805FR-078K66L" H -450 700 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H -450 700 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H -450 700 50  0001 C CNN "Power"
F 11 " ±1%" H -450 700 50  0001 C CNN "Tolerance"
	1    8550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4100 7050 4100
Wire Wire Line
	7050 4100 7050 3600
Wire Wire Line
	7050 3600 7350 3600
Wire Wire Line
	7650 3600 8000 3600
Wire Wire Line
	8000 3600 8000 4100
Wire Wire Line
	8000 4100 7850 4100
Wire Wire Line
	8200 3600 8000 3600
Connection ~ 8000 3600
Wire Wire Line
	8500 3600 8550 3600
Wire Wire Line
	8550 3600 8550 3800
Wire Wire Line
	8550 4100 8550 4350
Wire Wire Line
	7850 4350 8550 4350
Connection ~ 8550 4350
Wire Wire Line
	8550 4350 8550 4450
$Comp
L eload:GNDD #PWR?
U 1 1 5B730226
P 8550 4800
AR Path="/5B5FBC03/5B730226" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B730226" Ref="#PWR0532"  Part="1" 
F 0 "#PWR0532" H 8550 4550 50  0001 C CNN
F 1 "GNDD" H 8555 4627 50  0000 C CNN
F 2 "" H 8550 4800 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 8550 4800 50  0001 C CNN
	1    8550 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 4750 8550 4800
$Comp
L eload:GNDD #PWR?
U 1 1 5B734BED
P 7150 4600
AR Path="/5B5FBC03/5B734BED" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B734BED" Ref="#PWR0531"  Part="1" 
F 0 "#PWR0531" H 7150 4350 50  0001 C CNN
F 1 "GNDD" H 7155 4427 50  0000 C CNN
F 2 "" H 7150 4600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 7150 4600 50  0001 C CNN
	1    7150 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 4550 7150 4550
Wire Wire Line
	7150 4550 7150 4600
Wire Wire Line
	8550 3600 8900 3600
Wire Wire Line
	9200 3600 9200 4150
Connection ~ 8550 3600
$Comp
L eload:GNDD #PWR?
U 1 1 5B739D84
P 9200 4550
AR Path="/5B5FBC03/5B739D84" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B739D84" Ref="#PWR0530"  Part="1" 
F 0 "#PWR0530" H 9200 4300 50  0001 C CNN
F 1 "GNDD" H 9205 4377 50  0000 C CNN
F 2 "" H 9200 4550 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 9200 4550 50  0001 C CNN
	1    9200 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 4450 9200 4550
$Comp
L eload:+5V0_FILT #PWR0513
U 1 1 5B751E93
P 1050 3200
F 0 "#PWR0513" H 1050 3050 50  0001 C CNN
F 1 "+5V0_FILT" H 1055 3373 50  0000 C CNN
F 2 "" H 1050 3200 50  0001 C CNN
F 3 "" H 1050 3200 50  0001 C CNN
	1    1050 3200
	1    0    0    -1  
$EndComp
$Comp
L eload:+5V0_FILT #PWR0501
U 1 1 5B751FBA
P 5150 1100
F 0 "#PWR0501" H 5150 950 50  0001 C CNN
F 1 "+5V0_FILT" H 5155 1273 50  0000 C CNN
F 2 "" H 5150 1100 50  0001 C CNN
F 3 "" H 5150 1100 50  0001 C CNN
	1    5150 1100
	1    0    0    -1  
$EndComp
$Comp
L eload:+5V0_FILT #PWR0509
U 1 1 5B7527FE
P 1050 2750
F 0 "#PWR0509" H 1050 2600 50  0001 C CNN
F 1 "+5V0_FILT" H 1055 2923 50  0000 C CNN
F 2 "" H 1050 2750 50  0001 C CNN
F 3 "" H 1050 2750 50  0001 C CNN
	1    1050 2750
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B75DC0E
P 3150 3450
AR Path="/5B6010A5/5B75DC0E" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B75DC0E" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B75DC0E" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B75DC0E" Ref="#PWR0516"  Part="1" 
F 0 "#PWR0516" H 3150 3200 50  0001 C CNN
F 1 "GNDA" H 3155 3277 50  0000 C CNN
F 2 "" H 3150 3450 50  0001 C CNN
F 3 "" H 3150 3450 50  0001 C CNN
	1    3150 3450
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B75DC65
P 3700 3450
AR Path="/5B5FBC03/5B75DC65" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B75DC65" Ref="#PWR0517"  Part="1" 
F 0 "#PWR0517" H 3700 3200 50  0001 C CNN
F 1 "GNDD" H 3705 3277 50  0000 C CNN
F 2 "" H 3700 3450 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3700 3450 50  0001 C CNN
	1    3700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3450 3150 3400
Wire Wire Line
	3700 3400 3700 3450
Wire Wire Line
	7050 3600 6700 3600
Wire Wire Line
	6700 3600 6700 3650
Connection ~ 7050 3600
$Comp
L eload:GNDD #PWR?
U 1 1 5B78069D
P 6700 4000
AR Path="/5B5FBC03/5B78069D" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B78069D" Ref="#PWR0523"  Part="1" 
F 0 "#PWR0523" H 6700 3750 50  0001 C CNN
F 1 "GNDD" H 6705 3827 50  0000 C CNN
F 2 "" H 6700 4000 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 6700 4000 50  0001 C CNN
	1    6700 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4000 6700 3950
$Comp
L eload:+5V0 #PWR0502
U 1 1 5B784AA1
P 9050 1150
F 0 "#PWR0502" H 9050 1000 50  0001 C CNN
F 1 "+5V0" H 9055 1323 50  0000 C CNN
F 2 "" H 9050 1150 50  0001 C CNN
F 3 "" H 9050 1150 50  0001 C CNN
	1    9050 1150
	1    0    0    -1  
$EndComp
$Comp
L eload:+12V0 #PWR0520
U 1 1 5B794DB3
P 9300 3550
F 0 "#PWR0520" H 9300 3400 50  0001 C CNN
F 1 "+12V0" H 9305 3723 50  0000 C CNN
F 2 "" H 9300 3550 50  0001 C CNN
F 3 "" H 9300 3550 50  0001 C CNN
	1    9300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3600 9300 3600
Wire Wire Line
	9300 3600 9300 3550
Connection ~ 9200 3600
Connection ~ 7000 1200
$Comp
L eload:LED D?
U 1 1 5B7BC8A6
P 3200 7150
AR Path="/5B5FBC03/5B7BC8A6" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5B7BC8A6" Ref="D504"  Part="1" 
F 0 "D504" V 3238 7032 50  0000 R CNN
F 1 "GREEN" V 3147 7032 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 3200 7150 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120VS75000.pdf" H 3200 7150 50  0001 C CNN
F 4 "20mA" H -2300 5150 50  0001 C CNN "Current"
F 5 "732-4993-1-ND" H -2300 5150 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -2300 5150 50  0001 C CNN "Manufacturer"
F 7 "150120VS75000" H -2300 5150 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H -2300 5150 50  0001 C CNN "Voltage"
F 9 "~" H -50 1600 50  0001 C CNN "Stuff"
F 10 "710-156120VS75000" H -50 1600 50  0001 C CNN "Mouser Part Number"
	1    3200 7150
	0    -1   -1   0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B7BC8B4
P 3200 6800
AR Path="/5B6010A5/5B7BC8B4" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B7BC8B4" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B7BC8B4" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B7BC8B4" Ref="R512"  Part="1" 
F 0 "R512" H 3132 6709 50  0000 R CNN
F 1 "1k" H 3132 6800 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3240 6790 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 3200 6800 50  0001 C CNN
F 4 "Yageo" V 3200 6800 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 3200 6800 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 3200 6800 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 3132 6891 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 3200 6800 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 3200 6800 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 3200 6800 50  0001 C CNN "Operating Temperature"
F 11 "~" H -50 1600 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -50 1600 50  0001 C CNN "Mouser Part Number"
	1    3200 6800
	-1   0    0    1   
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B7BC8BB
P 3200 7350
AR Path="/5B5FBC03/5B7BC8BB" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B7BC8BB" Ref="#PWR0540"  Part="1" 
F 0 "#PWR0540" H 3200 7100 50  0001 C CNN
F 1 "GNDD" H 3205 7177 50  0000 C CNN
F 2 "" H 3200 7350 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3200 7350 50  0001 C CNN
	1    3200 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 7350 3200 7300
Wire Wire Line
	3200 7000 3200 6950
Wire Wire Line
	3200 6550 3200 6650
$Comp
L eload:+12V0 #PWR0535
U 1 1 5B7C5462
P 3200 6550
F 0 "#PWR0535" H 3200 6400 50  0001 C CNN
F 1 "+12V0" H 3205 6723 50  0000 C CNN
F 2 "" H 3200 6550 50  0001 C CNN
F 3 "" H 3200 6550 50  0001 C CNN
	1    3200 6550
	1    0    0    -1  
$EndComp
Text Notes 3600 2300 0    50   ~ 0
*Place OUTS trace close to MCU VDDA
$Comp
L eload:+3V3 #PWR?
U 1 1 5B6C5B57
P 4600 6550
AR Path="/5B5FBC03/5B6C5B57" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5B57" Ref="#PWR0537"  Part="1" 
F 0 "#PWR0537" H 4600 6400 50  0001 C CNN
F 1 "+3V3" H 4605 6723 50  0000 C CNN
F 2 "" H 4600 6550 50  0001 C CNN
F 3 "" H 4600 6550 50  0001 C CNN
	1    4600 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6550 4600 6650
Wire Wire Line
	4600 7000 4600 6950
Wire Wire Line
	4600 7350 4600 7300
$Comp
L eload:GNDD #PWR?
U 1 1 5B6C5B4E
P 4600 7350
AR Path="/5B5FBC03/5B6C5B4E" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B6C5B4E" Ref="#PWR0542"  Part="1" 
F 0 "#PWR0542" H 4600 7100 50  0001 C CNN
F 1 "GNDD" H 4605 7177 50  0000 C CNN
F 2 "" H 4600 7350 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4600 7350 50  0001 C CNN
	1    4600 7350
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B6C5B47
P 4600 6800
AR Path="/5B6010A5/5B6C5B47" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B6C5B47" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B6C5B47" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B6C5B47" Ref="R514"  Part="1" 
F 0 "R514" H 4532 6709 50  0000 R CNN
F 1 "1k" H 4532 6800 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4640 6790 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 4600 6800 50  0001 C CNN
F 4 "Yageo" V 4600 6800 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 4600 6800 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 4600 6800 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 4532 6891 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 4600 6800 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 4600 6800 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 4600 6800 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2850 1650 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H 2850 1650 50  0001 C CNN "Mouser Part Number"
	1    4600 6800
	-1   0    0    1   
$EndComp
$Comp
L eload:LED D?
U 1 1 5B6C5B39
P 4600 7150
AR Path="/5B5FBC03/5B6C5B39" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5B6C5B39" Ref="D506"  Part="1" 
F 0 "D506" V 4638 7032 50  0000 R CNN
F 1 "GREEN" V 4547 7032 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 4600 7150 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120VS75000.pdf" H 4600 7150 50  0001 C CNN
F 4 "20mA" H -900 5150 50  0001 C CNN "Current"
F 5 "732-4993-1-ND" H -900 5150 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -900 5150 50  0001 C CNN "Manufacturer"
F 7 "150120VS75000" H -900 5150 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H -900 5150 50  0001 C CNN "Voltage"
F 9 "~" H 2850 1650 50  0001 C CNN "Stuff"
F 10 "710-156120VS75000" H 2850 1650 50  0001 C CNN "Mouser Part Number"
	1    4600 7150
	0    -1   -1   0   
$EndComp
$Comp
L eload:PWR_FLAG #FLG0501
U 1 1 5BC279A0
P 4750 1150
F 0 "#FLG0501" H 4750 1225 50  0001 C CNN
F 1 "PWR_FLAG" H 4750 1324 50  0001 C CNN
F 2 "" H 4750 1150 50  0001 C CNN
F 3 "~" H 4750 1150 50  0001 C CNN
	1    4750 1150
	1    0    0    -1  
$EndComp
Connection ~ 4750 1150
Wire Wire Line
	4750 1150 5050 1150
$Comp
L eload:PWR_FLAG #FLG0502
U 1 1 5BC31D30
P 8600 1200
F 0 "#FLG0502" H 8600 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 8600 1374 50  0001 C CNN
F 2 "" H 8600 1200 50  0001 C CNN
F 3 "~" H 8600 1200 50  0001 C CNN
	1    8600 1200
	1    0    0    -1  
$EndComp
Connection ~ 4200 2800
$Comp
L eload:PWR_FLAG #FLG0504
U 1 1 5BC65C55
P 8900 3600
F 0 "#FLG0504" H 8900 3675 50  0001 C CNN
F 1 "PWR_FLAG" H 8900 3774 50  0001 C CNN
F 2 "" H 8900 3600 50  0001 C CNN
F 3 "~" H 8900 3600 50  0001 C CNN
	1    8900 3600
	1    0    0    -1  
$EndComp
Connection ~ 8900 3600
Wire Wire Line
	8900 3600 9200 3600
$Comp
L eload:R_US R507
U 1 1 5BC70833
P 4600 2800
F 0 "R507" V 4550 2550 50  0000 C CNN
F 1 "0R" V 4550 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4640 2790 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 4600 2800 50  0001 C CNN
F 4 "~" H 750 -350 50  0001 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H 750 -350 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H 750 -350 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H 750 -350 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H 750 -350 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H 750 -350 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H 750 -350 50  0001 C CNN "Power"
	1    4600 2800
	0    1    1    0   
$EndComp
$Comp
L eload:PWR_FLAG #FLG0503
U 1 1 5BC7572E
P 5250 2900
F 0 "#FLG0503" H 5250 2975 50  0001 C CNN
F 1 "PWR_FLAG" H 5250 3073 50  0001 C CNN
F 2 "" H 5250 2900 50  0001 C CNN
F 3 "~" H 5250 2900 50  0001 C CNN
	1    5250 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 2800 4450 2800
Wire Wire Line
	4750 2800 5000 2800
Wire Wire Line
	5250 2900 5250 2800
Connection ~ 5250 2800
Wire Wire Line
	5250 2800 5500 2800
Wire Wire Line
	3800 2800 4200 2800
Wire Wire Line
	5000 2650 5000 2800
Connection ~ 5000 2800
Wire Wire Line
	5000 2800 5150 2800
$Comp
L eload:R_US R?
U 1 1 5B8D32A4
P 8550 4600
AR Path="/5B6010A5/5B8D32A4" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B8D32A4" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B8D32A4" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B8D32A4" Ref="R511"  Part="1" 
F 0 "R511" H 8482 4509 50  0000 R CNN
F 1 "1k" H 8482 4600 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8590 4590 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 8550 4600 50  0001 C CNN
F 4 "Yageo" V 8550 4600 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 8550 4600 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 8550 4600 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 8482 4691 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 8550 4600 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 8550 4600 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 8550 4600 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2800 1900 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -450 700 50  0001 C CNN "Mouser Part Number"
	1    8550 4600
	-1   0    0    1   
$EndComp
Text Label 2150 1150 0    50   ~ 0
VBUS
Text Label 2900 1150 0    50   ~ 0
VBUS_F
Text Label 7450 1700 0    50   ~ 0
IR_D
Text Label 1800 2800 0    50   ~ 0
VR_EN
Wire Wire Line
	2800 2950 2800 3000
Wire Wire Line
	2800 2950 3050 2950
Text Label 2850 2950 0    50   ~ 0
VR_C
$Comp
L eload:Net-Tie_2 NT501
U 1 1 5BD05FF5
P 3550 3400
F 0 "NT501" H 3550 3486 50  0000 C CNN
F 1 "Net-Tie_2" H 3550 3487 50  0001 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 3550 3400 50  0001 C CNN
F 3 "~" H 3550 3400 50  0001 C CNN
	1    3550 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3400 3700 3400
Wire Wire Line
	7900 1700 7900 1800
Wire Wire Line
	7900 1500 7900 1600
Wire Wire Line
	8100 1200 8400 1200
$Comp
L eload:R_US R502
U 1 1 5B9006E9
P 8150 1600
F 0 "R502" V 8350 1600 50  0000 C CNN
F 1 "0R" V 8250 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8190 1590 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 8150 1600 50  0001 C CNN
F 4 "~" V 8263 1600 50  0001 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H 3200 -2300 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H 3200 -2300 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H 3200 -2300 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H 3200 -2300 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H 3200 -2300 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H 3200 -2300 50  0001 C CNN "Power"
	1    8150 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 1600 7900 1600
Connection ~ 7900 1600
Wire Wire Line
	7900 1600 7900 1700
Wire Wire Line
	8300 1600 8400 1600
Wire Wire Line
	8400 1600 8400 1550
Wire Wire Line
	8400 1250 8400 1200
Connection ~ 8400 1200
Wire Wire Line
	8400 1200 8600 1200
Connection ~ 8600 1200
Text Label 8400 1600 0    50   ~ 0
IR_DCRG
$Comp
L eload:LD3985M33R U502
U 1 1 5B7EF3E7
P 2900 4350
F 0 "U502" H 2900 4692 50  0000 C CNN
F 1 "LD3985M33R" H 2900 4601 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 2900 4675 50  0001 C CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/group2/96/39/79/10/c9/1a/48/92/CD00003395/files/CD00003395.pdf/jcr:content/translations/en.CD00003395.pdf" H 2900 4350 50  0001 C CNN
	1    2900 4350
	1    0    0    -1  
$EndComp
$Comp
L eload:C C508
U 1 1 5B7EFCFE
P 1200 3400
F 0 "C508" H 1315 3446 50  0000 L CNN
F 1 "1uF" H 1315 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1238 3250 50  0001 C CNN
F 3 "~" H 1200 3400 50  0001 C CNN
F 4 "~" H -5950 -500 50  0001 C CNN "Stuff"
F 5 "710-885012207022" H -5950 -500 50  0001 C CNN "Mouser Part Number"
	1    1200 3400
	1    0    0    -1  
$EndComp
$Comp
L eload:C C512
U 1 1 5B7EFF8E
P 3800 4550
F 0 "C512" H 3915 4596 50  0000 L CNN
F 1 "1uF" H 3915 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3838 4400 50  0001 C CNN
F 3 "~" H 3800 4550 50  0001 C CNN
F 4 "~" H -3350 650 50  0001 C CNN "Stuff"
F 5 "710-885012207022" H -3350 650 50  0001 C CNN "Mouser Part Number"
	1    3800 4550
	1    0    0    -1  
$EndComp
$Comp
L eload:C C514
U 1 1 5B7F8409
P 3300 4550
F 0 "C514" H 3415 4596 50  0000 L CNN
F 1 "10nF" H 3415 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3338 4400 50  0001 C CNN
F 3 "~" H 3300 4550 50  0001 C CNN
	1    3300 4550
	1    0    0    -1  
$EndComp
$Comp
L eload:D_Schottky D?
U 1 1 5B817622
P 1050 6400
AR Path="/5B5FBC06/5B817622" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5B817622" Ref="D503"  Part="1" 
F 0 "D503" V 1000 6500 50  0000 L CNN
F 1 "D_Schottky" V 1100 6500 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-523" H 1050 6400 50  0001 C CNN
F 3 "~" H 1050 6400 50  0001 C CNN
	1    1050 6400
	0    1    1    0   
$EndComp
Wire Wire Line
	1050 2800 1450 2800
Wire Wire Line
	1050 3200 1200 3200
Wire Wire Line
	1000 3600 1200 3600
Wire Wire Line
	1600 3600 1600 3550
Wire Wire Line
	1200 3600 1200 3550
Connection ~ 1200 3600
Wire Wire Line
	1200 3250 1200 3200
Connection ~ 1200 3200
$Comp
L eload:+5V0_FILT #PWR0508
U 1 1 5B8642C3
P 2100 2600
F 0 "#PWR0508" H 2100 2450 50  0001 C CNN
F 1 "+5V0_FILT" H 2105 2773 50  0000 C CNN
F 2 "" H 2100 2600 50  0001 C CNN
F 3 "" H 2100 2600 50  0001 C CNN
	1    2100 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 2600 2100 2650
Text Label 3850 2800 0    50   ~ 0
VR_3V3_R
Wire Wire Line
	3200 4350 3300 4350
Wire Wire Line
	3300 4350 3300 4400
Wire Wire Line
	3300 4750 3300 4700
Wire Wire Line
	3800 4700 3800 4750
Wire Wire Line
	4750 4650 4750 4750
Wire Wire Line
	4750 4250 4750 4350
$Comp
L eload:+3V3 #PWR?
U 1 1 5B89BABC
P 4850 4200
AR Path="/5B5FBC03/5B89BABC" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B89BABC" Ref="#PWR0521"  Part="1" 
F 0 "#PWR0521" H 4850 4050 50  0001 C CNN
F 1 "+3V3" H 4855 4373 50  0000 C CNN
F 2 "" H 4850 4200 50  0001 C CNN
F 3 "" H 4850 4200 50  0001 C CNN
	1    4850 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 4250 4850 4250
Wire Wire Line
	4850 4250 4850 4200
Connection ~ 4750 4250
$Comp
L eload:C C511
U 1 1 5B8A2522
P 2100 4450
F 0 "C511" H 2215 4496 50  0000 L CNN
F 1 "1uF" H 2215 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2138 4300 50  0001 C CNN
F 3 "~" H 2100 4450 50  0001 C CNN
F 4 "~" H -5050 550 50  0001 C CNN "Stuff"
F 5 "710-885012207022" H -5050 550 50  0001 C CNN "Mouser Part Number"
	1    2100 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4250 2100 4250
Wire Wire Line
	2100 4250 2100 4300
$Comp
L eload:+5V0_FILT #PWR0522
U 1 1 5B8A9035
P 2000 4250
F 0 "#PWR0522" H 2000 4100 50  0001 C CNN
F 1 "+5V0_FILT" H 2005 4423 50  0000 C CNN
F 2 "" H 2000 4250 50  0001 C CNN
F 3 "" H 2000 4250 50  0001 C CNN
	1    2000 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4250 2100 4250
Connection ~ 2100 4250
Wire Wire Line
	2100 4650 2100 4600
Wire Wire Line
	2900 4700 2900 4650
$Comp
L eload:+3V3_A #PWR?
U 1 1 5B8C4A6A
P 1050 6200
AR Path="/5B5FBC06/5B8C4A6A" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8C4A6A" Ref="#PWR0534"  Part="1" 
F 0 "#PWR0534" H 1050 6050 50  0001 C CNN
F 1 "+3V3_A" H 1055 6373 50  0000 C CNN
F 2 "" H 1050 6200 50  0001 C CNN
F 3 "" H 1050 6200 50  0001 C CNN
	1    1050 6200
	1    0    0    -1  
$EndComp
$Comp
L eload:+3V3 #PWR?
U 1 1 5B8C4AD5
P 1050 6600
AR Path="/5B5FBC03/5B8C4AD5" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8C4AD5" Ref="#PWR0538"  Part="1" 
F 0 "#PWR0538" H 1050 6450 50  0001 C CNN
F 1 "+3V3" H 1055 6773 50  0000 C CNN
F 2 "" H 1050 6600 50  0001 C CNN
F 3 "" H 1050 6600 50  0001 C CNN
	1    1050 6600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 6200 1050 6250
Wire Wire Line
	1050 6550 1050 6600
Text Notes 1400 6300 0    50   ~ 0
VDD of MCU cannot be more\nthan +0.4V than VDDA
$Comp
L eload:R_US R515
U 1 1 5B8F60E7
P 1050 7350
F 0 "R515" H 982 7304 50  0000 R CNN
F 1 "0R" H 982 7395 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1090 7340 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 1050 7350 50  0001 C CNN
F 4 "DNS" H -2800 4200 50  0001 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H -2800 4200 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H -2800 4200 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H -2800 4200 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H -2800 4200 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H -2800 4200 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H -2800 4200 50  0001 C CNN "Power"
	1    1050 7350
	-1   0    0    1   
$EndComp
$Comp
L eload:+3V3_A #PWR?
U 1 1 5B8F6941
P 1050 7150
AR Path="/5B5FBC06/5B8F6941" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8F6941" Ref="#PWR0539"  Part="1" 
F 0 "#PWR0539" H 1050 7000 50  0001 C CNN
F 1 "+3V3_A" H 1055 7323 50  0000 C CNN
F 2 "" H 1050 7150 50  0001 C CNN
F 3 "" H 1050 7150 50  0001 C CNN
	1    1050 7150
	1    0    0    -1  
$EndComp
$Comp
L eload:+3V3 #PWR?
U 1 1 5B8F69AE
P 1050 7550
AR Path="/5B5FBC03/5B8F69AE" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8F69AE" Ref="#PWR0543"  Part="1" 
F 0 "#PWR0543" H 1050 7400 50  0001 C CNN
F 1 "+3V3" H 1055 7723 50  0000 C CNN
F 2 "" H 1050 7550 50  0001 C CNN
F 3 "" H 1050 7550 50  0001 C CNN
	1    1050 7550
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 7500 1050 7550
Wire Wire Line
	1050 7200 1050 7150
Text Notes 1400 7400 0    50   ~ 0
Bridge +3V3 rails if\nnot using a reference
$Comp
L eload:R_US R?
U 1 1 5B90EF18
P 1500 5050
AR Path="/5B90EF18" Ref="R?"  Part="1" 
AR Path="/5B6010A5/5B90EF18" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B90EF18" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B90EF18" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B90EF18" Ref="R510"  Part="1" 
F 0 "R510" V 1613 5050 50  0000 C CNN
F 1 "10k" V 1704 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1540 5040 50  0001 L CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1500 5050 50  0001 L CNN
F 4 "±1%" H 1550 4950 50  0001 L CNN "Tolerance"
F 5 "±100ppm/°C" H 1500 5050 50  0001 L CNN "Temperature Coefficient"
F 6 "CRCW080510K0FKEAC" H 1500 5050 50  0001 L CNN "Manufacturer Part Number"
F 7 "Vishay Dale" H 1500 5050 50  0001 L CNN "Manufacturer"
F 8 " 541-3976-1-ND " H 1500 5050 50  0001 L CNN "Digi-Key Part Number"
F 9 "-55°C ~ 155°C" H 200 2500 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W " H 200 2500 50  0001 C CNN "Power"
F 11 "~" H -350 1900 50  0001 C CNN "Stuff"
F 12 "GWCR0805-10KFT5 " H -350 1900 50  0001 C CNN "Mouser Part Number"
	1    1500 5050
	0    1    1    0   
$EndComp
$Comp
L eload:C C516
U 1 1 5B90EFF7
P 2100 5250
F 0 "C516" H 2215 5296 50  0000 L CNN
F 1 "1uF" H 2215 5205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2138 5100 50  0001 C CNN
F 3 "~" H 2100 5250 50  0001 C CNN
F 4 "~" H -5050 1350 50  0001 C CNN "Stuff"
F 5 "710-885012207022" H -5050 1350 50  0001 C CNN "Mouser Part Number"
	1    2100 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 5500 2100 5400
Wire Wire Line
	2100 5050 2100 5100
Wire Wire Line
	2100 5050 2500 5050
Wire Wire Line
	2500 5050 2500 4350
Wire Wire Line
	2500 4350 2600 4350
Connection ~ 2100 5050
$Comp
L eload:+3V3_A #PWR?
U 1 1 5B92CC3E
P 1250 5000
AR Path="/5B5FBC06/5B92CC3E" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B92CC3E" Ref="#PWR0529"  Part="1" 
F 0 "#PWR0529" H 1250 4850 50  0001 C CNN
F 1 "+3V3_A" H 1255 5173 50  0000 C CNN
F 2 "" H 1250 5000 50  0001 C CNN
F 3 "" H 1250 5000 50  0001 C CNN
	1    1250 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 5000 1250 5050
Wire Wire Line
	1250 5050 1350 5050
Text Notes 2500 5400 0    50   ~ 0
VDD of MCU must be turn on\nafter VDDA
Text Notes 1400 3150 0    50   ~ 0
For Voltage Ref
$Comp
L eload:R_US R508
U 1 1 5B981E14
P 4100 4250
F 0 "R508" V 4050 4000 50  0000 C CNN
F 1 "0R" V 4050 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4140 4240 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 4100 4250 50  0001 C CNN
F 4 "~" H 250 1100 50  0001 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H 250 1100 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H 250 1100 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H 250 1100 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H 250 1100 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H 250 1100 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H 250 1100 50  0001 C CNN "Power"
	1    4100 4250
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 4250 3800 4250
Text Label 3250 4250 0    50   ~ 0
+3V3_OUT_R
Wire Wire Line
	7200 4350 7050 4350
Wire Wire Line
	7050 4350 7050 4100
Connection ~ 7050 4100
Text Notes 7400 750  0    50   ~ 0
In rush current limit and switch for\nsystem +5V0 rail
Text HLabel 6950 2500 0    50   Input ~ 0
+5V0_EN
$Comp
L eload:RK7002BMT116 Q?
U 1 1 5B9FAFEF
P 7800 2500
AR Path="/5B6010A5/5B9FAFEF" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5B9FAFEF" Ref="Q?"  Part="1" 
AR Path="/5B6C0220/5B9FAFEF" Ref="Q502"  Part="1" 
F 0 "Q502" H 8006 2546 50  0000 L CNN
F 1 "RK7002BMT116" H 8006 2455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8600 2750 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RK7002BM/rk7002bmt116-e" H 8000 2550 50  0001 C CNN
F 4 "Rohm Semiconductor" H 8450 3050 50  0001 C CNN "Manufacturer"
F 5 "RK7002BMT116 " H 8350 2850 50  0001 C CNN "Manufacturer Part Number"
F 6 "RK7002BMT116CT-ND" H 8500 2950 50  0001 C CNN "Digi-Key Part Number"
F 7 "~" H 6050 750 50  0001 C CNN "Stuff"
F 8 "755-RK7002BMT116" H 6050 750 50  0001 C CNN "Mouser Part Number"
	1    7800 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2700 7900 2750
$Comp
L eload:R_US R504
U 1 1 5BA07A1A
P 7200 2500
F 0 "R504" V 6995 2500 50  0000 C CNN
F 1 "100R" V 7086 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7240 2490 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7200 2500 50  0001 C CNN
F 4 "~" H 1600 550 50  0001 C CNN "Stuff"
F 5 "1276-5224-1-ND" H 1600 550 50  0001 C CNN "Digi-Key Part Number"
F 6 "Samsung Electro-Mechanics" H 1600 550 50  0001 C CNN "Manufacturer"
F 7 "RC2012F101CS" H 1600 550 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 1600 550 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 1600 550 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 1600 550 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 1600 550 50  0001 C CNN "Tolerance"
F 12 "756-GWCR0805-100RFT5" H 1600 550 50  0001 C CNN "Mouser Part Number"
	1    7200 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 2500 7050 2500
Wire Wire Line
	7350 2500 7500 2500
$Comp
L eload:R_US R505
U 1 1 5BA227FD
P 7500 2700
F 0 "R505" H 7568 2746 50  0000 L CNN
F 1 "100k" H 7568 2655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7540 2690 50  0001 C CNN
F 3 "~" H 7500 2700 50  0001 C CNN
	1    7500 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2550 7500 2500
Connection ~ 7500 2500
Wire Wire Line
	7500 2500 7600 2500
Text Label 6600 1200 0    50   ~ 0
VBUS_F
Wire Wire Line
	6600 1200 7000 1200
$Comp
L eload:+5V0 #PWR0519
U 1 1 5BA960C2
P 6500 3550
F 0 "#PWR0519" H 6500 3400 50  0001 C CNN
F 1 "+5V0" H 6505 3723 50  0000 C CNN
F 2 "" H 6500 3550 50  0001 C CNN
F 3 "" H 6500 3550 50  0001 C CNN
	1    6500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 3550 6500 3600
Wire Wire Line
	6500 3600 6700 3600
Connection ~ 6700 3600
$Comp
L eload:+5V0 #PWR0536
U 1 1 5BA9EA2E
P 3900 6550
F 0 "#PWR0536" H 3900 6400 50  0001 C CNN
F 1 "+5V0" H 3905 6723 50  0000 C CNN
F 2 "" H 3900 6550 50  0001 C CNN
F 3 "" H 3900 6550 50  0001 C CNN
	1    3900 6550
	1    0    0    -1  
$EndComp
Text Label 7950 4350 0    50   ~ 0
+12V0_FB
Text Label 7750 3600 0    50   ~ 0
+12V0_LD
Text Label 2050 5050 0    50   ~ 0
+3V3_EN
Text Label 3250 4350 0    50   ~ 0
+3V3_BP
Text Label 7450 2500 0    50   ~ 0
IR_SW
$Comp
L eload:PWR_FLAG #FLG0101
U 1 1 5C00C4C0
P 4550 4250
F 0 "#FLG0101" H 4550 4325 50  0001 C CNN
F 1 "PWR_FLAG" H 4550 4424 50  0001 C CNN
F 2 "" H 4550 4250 50  0001 C CNN
F 3 "~" H 4550 4250 50  0001 C CNN
	1    4550 4250
	1    0    0    -1  
$EndComp
Connection ~ 4550 4250
Wire Wire Line
	4550 4250 4750 4250
Wire Wire Line
	3150 3400 3250 3400
$Comp
L eload:PWR_FLAG #FLG0102
U 1 1 5C0222EE
P 3250 3350
F 0 "#FLG0102" H 3250 3425 50  0001 C CNN
F 1 "PWR_FLAG" H 3250 3523 50  0001 C CNN
F 2 "" H 3250 3350 50  0001 C CNN
F 3 "~" H 3250 3350 50  0001 C CNN
	1    3250 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3350 3250 3400
Connection ~ 3250 3400
Wire Wire Line
	3250 3400 3450 3400
Wire Wire Line
	7900 2100 7900 2300
Text Label 7900 2200 0    50   ~ 0
IR_SW_D
$Comp
L eload:LM7705 U504
U 1 1 5B82EC19
P 7350 5600
F 0 "U504" H 7350 5750 50  0000 L CNN
F 1 "LM7705" H 7350 5650 50  0000 L CNN
F 2 "Package_SO:VSSOP-8_3.0x3.0mm_P0.65mm" H 7850 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm7705.pdf" H 7250 5850 50  0001 C CNN
	1    7350 5600
	1    0    0    -1  
$EndComp
$Comp
L eload:C C?
U 1 1 5B8323C9
P 7100 6200
AR Path="/5B6010A5/5B8323C9" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B8323C9" Ref="C?"  Part="1" 
AR Path="/5B5FBC06/5B8323C9" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B8323C9" Ref="C524"  Part="1" 
F 0 "C524" H 7215 6246 50  0000 L CNN
F 1 "0.1uF" H 7215 6155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7138 6050 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 7100 6200 50  0001 C CNN
F 4 "KEMET" H 7100 6200 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 7100 6200 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 7100 6200 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 7100 6200 50  0001 C CNN "Tolerance"
F 8 "50V" H 7100 6200 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 7100 6200 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 7100 6200 50  0001 C CNN "Operating Temperature"
F 11 "~" H 3650 2850 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 3650 2850 50  0001 C CNN "Mouser Part Number"
	1    7100 6200
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B832582
P 7100 6400
AR Path="/5B6010A5/5B832582" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B832582" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B832582" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B832582" Ref="#PWR0551"  Part="1" 
F 0 "#PWR0551" H 7100 6150 50  0001 C CNN
F 1 "GNDA" H 7105 6227 50  0000 C CNN
F 2 "" H 7100 6400 50  0001 C CNN
F 3 "" H 7100 6400 50  0001 C CNN
	1    7100 6400
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B8325F1
P 8100 6050
AR Path="/5B6010A5/5B8325F1" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8325F1" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B8325F1" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8325F1" Ref="#PWR0547"  Part="1" 
F 0 "#PWR0547" H 8100 5800 50  0001 C CNN
F 1 "GNDA" H 8105 5877 50  0000 C CNN
F 2 "" H 8100 6050 50  0001 C CNN
F 3 "" H 8100 6050 50  0001 C CNN
	1    8100 6050
	1    0    0    -1  
$EndComp
$Comp
L eload:C C518
U 1 1 5B835280
P 7650 5350
F 0 "C518" V 7600 5200 50  0000 C CNN
F 1 "4.7uF" V 7600 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 5200 50  0001 C CNN
F 3 "~" H 7650 5350 50  0001 C CNN
	1    7650 5350
	0    1    1    0   
$EndComp
$Comp
L eload:C C517
U 1 1 5B83DB57
P 7650 5150
F 0 "C517" V 7600 5000 50  0000 C CNN
F 1 "4.7uF" V 7600 5350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7688 5000 50  0001 C CNN
F 3 "~" H 7650 5150 50  0001 C CNN
	1    7650 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	7250 5700 7200 5700
Wire Wire Line
	7200 5350 7500 5350
Wire Wire Line
	7800 5350 8100 5350
Wire Wire Line
	8100 5700 8050 5700
Wire Wire Line
	7500 5150 7200 5150
Wire Wire Line
	7200 5150 7200 5350
Connection ~ 7200 5350
Wire Wire Line
	7800 5150 8100 5150
Wire Wire Line
	8100 5150 8100 5350
Connection ~ 8100 5350
Wire Wire Line
	7200 5350 7200 5700
Wire Wire Line
	8100 5350 8100 5700
$Comp
L eload:GNDA #PWR?
U 1 1 5B879FC5
P 5800 6000
AR Path="/5B6010A5/5B879FC5" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B879FC5" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B879FC5" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B879FC5" Ref="#PWR0545"  Part="1" 
F 0 "#PWR0545" H 5800 5750 50  0001 C CNN
F 1 "GNDA" H 5805 5827 50  0000 C CNN
F 2 "" H 5800 6000 50  0001 C CNN
F 3 "" H 5800 6000 50  0001 C CNN
	1    5800 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 6400 7100 6350
Wire Wire Line
	8050 6000 8100 6000
Wire Wire Line
	8100 6000 8100 6050
$Comp
L eload:C C520
U 1 1 5B89AF18
P 8700 5550
F 0 "C520" V 8650 5400 50  0000 C CNN
F 1 "4.7uF" V 8650 5750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8738 5400 50  0001 C CNN
F 3 "~" H 8700 5550 50  0001 C CNN
	1    8700 5550
	0    1    1    0   
$EndComp
$Comp
L eload:C C519
U 1 1 5B89AFE2
P 8700 5350
F 0 "C519" V 8650 5200 50  0000 C CNN
F 1 "4.7uF" V 8650 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8738 5200 50  0001 C CNN
F 3 "~" H 8700 5350 50  0001 C CNN
	1    8700 5350
	0    1    1    0   
$EndComp
Wire Wire Line
	8050 5800 8350 5800
Connection ~ 8350 5550
Wire Wire Line
	8350 5550 8350 5350
Wire Wire Line
	7250 6000 7100 6000
Wire Wire Line
	7100 6000 7100 6050
$Comp
L eload:GNDA #PWR?
U 1 1 5B8F4C55
P 9050 5600
AR Path="/5B6010A5/5B8F4C55" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8F4C55" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B8F4C55" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8F4C55" Ref="#PWR0544"  Part="1" 
F 0 "#PWR0544" H 9050 5350 50  0001 C CNN
F 1 "GNDA" H 9055 5427 50  0000 C CNN
F 2 "" H 9050 5600 50  0001 C CNN
F 3 "" H 9050 5600 50  0001 C CNN
	1    9050 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 5350 9050 5350
Wire Wire Line
	9050 5350 9050 5550
Wire Wire Line
	8850 5550 9050 5550
$Comp
L eload:Ferrite_Bead_Small L?
U 1 1 5B9090C5
P 9550 5900
AR Path="/5B5FBC03/5B9090C5" Ref="L?"  Part="1" 
AR Path="/5B6C0220/5B9090C5" Ref="L503"  Part="1" 
F 0 "L503" V 9313 5900 50  0000 C CNN
F 1 "600R @100MHZ" V 9404 5900 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 9480 5900 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/bead/CIC21J601NE.jsp" H 9550 5900 50  0001 C CNN
F 4 "1A" H 6400 1250 50  0001 C CNN "Current"
F 5 "1276-6371-1-ND" H 6400 1250 50  0001 C CNN "Digi-Key Part Number"
F 6 "Samsung Electro-Mechanics" H 6400 1250 50  0001 C CNN "Manufacturer"
F 7 "CIC21J601NE" H 6400 1250 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 125°C" H 6400 1250 50  0001 C CNN "Operating Temperature"
F 9 "~" H 7850 3050 50  0001 C CNN "Stuff"
F 10 "875-MI0805K601R-10" H 7850 3050 50  0001 C CNN "Mouser Part Number"
	1    9550 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	9050 5550 9050 5600
Wire Wire Line
	8350 5550 8350 5800
Connection ~ 9050 5550
$Comp
L eload:C C?
U 1 1 5B961CDA
P 8400 6100
AR Path="/5B6010A5/5B961CDA" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B961CDA" Ref="C?"  Part="1" 
AR Path="/5B5FBC06/5B961CDA" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B961CDA" Ref="C521"  Part="1" 
F 0 "C521" H 8515 6146 50  0000 L CNN
F 1 "0.1uF" H 8515 6055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8438 5950 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 8400 6100 50  0001 C CNN
F 4 "KEMET" H 8400 6100 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 8400 6100 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 8400 6100 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 8400 6100 50  0001 C CNN "Tolerance"
F 8 "50V" H 8400 6100 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 8400 6100 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 8400 6100 50  0001 C CNN "Operating Temperature"
F 11 "~" H 4950 2750 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 4950 2750 50  0001 C CNN "Mouser Part Number"
	1    8400 6100
	1    0    0    -1  
$EndComp
$Comp
L eload:C C522
U 1 1 5B961F99
P 8850 6100
F 0 "C522" H 8965 6146 50  0000 L CNN
F 1 "22uF" H 8965 6055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8888 5950 50  0001 C CNN
F 3 "~" H 8850 6100 50  0001 C CNN
	1    8850 6100
	1    0    0    -1  
$EndComp
$Comp
L eload:C C523
U 1 1 5B96204D
P 6650 6200
F 0 "C523" H 6765 6246 50  0000 L CNN
F 1 "1uF" H 6765 6155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6688 6050 50  0001 C CNN
F 3 "~" H 6650 6200 50  0001 C CNN
	1    6650 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 5950 8400 5900
Connection ~ 8400 5900
Wire Wire Line
	8400 5900 8050 5900
Wire Wire Line
	8850 5950 8850 5900
Connection ~ 8850 5900
Wire Wire Line
	8850 5900 8400 5900
$Comp
L eload:GNDA #PWR?
U 1 1 5B97CFDF
P 8400 6300
AR Path="/5B6010A5/5B97CFDF" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B97CFDF" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B97CFDF" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B97CFDF" Ref="#PWR0548"  Part="1" 
F 0 "#PWR0548" H 8400 6050 50  0001 C CNN
F 1 "GNDA" H 8405 6127 50  0000 C CNN
F 2 "" H 8400 6300 50  0001 C CNN
F 3 "" H 8400 6300 50  0001 C CNN
	1    8400 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 6300 8400 6250
$Comp
L eload:GNDA #PWR?
U 1 1 5B9860F1
P 8850 6300
AR Path="/5B6010A5/5B9860F1" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B9860F1" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B9860F1" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B9860F1" Ref="#PWR0549"  Part="1" 
F 0 "#PWR0549" H 8850 6050 50  0001 C CNN
F 1 "GNDA" H 8855 6127 50  0000 C CNN
F 2 "" H 8850 6300 50  0001 C CNN
F 3 "" H 8850 6300 50  0001 C CNN
	1    8850 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 6300 8850 6250
Wire Wire Line
	7100 6000 6650 6000
Wire Wire Line
	6650 6000 6650 6050
Connection ~ 7100 6000
$Comp
L eload:GNDA #PWR?
U 1 1 5B9C28AC
P 6650 6400
AR Path="/5B6010A5/5B9C28AC" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B9C28AC" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B9C28AC" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B9C28AC" Ref="#PWR0550"  Part="1" 
F 0 "#PWR0550" H 6650 6150 50  0001 C CNN
F 1 "GNDA" H 6655 6227 50  0000 C CNN
F 2 "" H 6650 6400 50  0001 C CNN
F 3 "" H 6650 6400 50  0001 C CNN
	1    6650 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 6400 6650 6350
$Comp
L eload:R_US R?
U 1 1 5B9CCE29
P 6700 5900
AR Path="/5B9CCE29" Ref="R?"  Part="1" 
AR Path="/5B6010A5/5B9CCE29" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B9CCE29" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B9CCE29" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B9CCE29" Ref="R516"  Part="1" 
F 0 "R516" V 6650 5700 50  0000 C CNN
F 1 "10k" V 6650 6100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 6740 5890 50  0001 L CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 6700 5900 50  0001 L CNN
F 4 "±1%" H 6750 5800 50  0001 L CNN "Tolerance"
F 5 "±100ppm/°C" H 6700 5900 50  0001 L CNN "Temperature Coefficient"
F 6 "CRCW080510K0FKEAC" H 6700 5900 50  0001 L CNN "Manufacturer Part Number"
F 7 "Vishay Dale" H 6700 5900 50  0001 L CNN "Manufacturer"
F 8 " 541-3976-1-ND " H 6700 5900 50  0001 L CNN "Digi-Key Part Number"
F 9 "-55°C ~ 155°C" H 5400 3350 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W " H 5400 3350 50  0001 C CNN "Power"
F 11 "~" H 4850 2750 50  0001 C CNN "Stuff"
F 12 "GWCR0805-10KFT5 " H 4850 2750 50  0001 C CNN "Mouser Part Number"
	1    6700 5900
	0    1    1    0   
$EndComp
$Comp
L eload:-0V232 #PWR0546
U 1 1 5BA0844E
P 9850 6000
F 0 "#PWR0546" H 9850 5850 50  0001 C CNN
F 1 "-0V232" H 9855 6173 50  0000 C CNN
F 2 "" H 9850 6000 50  0001 C CNN
F 3 "" H 9850 6000 50  0001 C CNN
	1    9850 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	9650 5900 9800 5900
Wire Wire Line
	9850 5900 9850 6000
Wire Wire Line
	8850 5900 9450 5900
Wire Wire Line
	8350 5550 8550 5550
Wire Wire Line
	8350 5350 8550 5350
Text Label 7200 5500 1    50   ~ 0
NR_CF+
Text Label 8100 5250 3    50   ~ 0
NR_CF-
Text Label 8350 5700 1    50   ~ 0
NR_CRES
Text Label 7000 5900 0    50   ~ 0
NR_SD
Wire Wire Line
	6850 5900 7250 5900
Text Label 8550 5900 0    50   ~ 0
-0V232_L
$Comp
L eload:+5V0_FILT #PWR0101
U 1 1 5BA8074E
P 6250 6250
F 0 "#PWR0101" H 6250 6100 50  0001 C CNN
F 1 "+5V0_FILT" H 6255 6423 50  0000 C CNN
F 2 "" H 6250 6250 50  0001 C CNN
F 3 "" H 6250 6250 50  0001 C CNN
	1    6250 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 6000 6500 6000
Wire Wire Line
	6500 6000 6500 6300
Wire Wire Line
	6500 6300 6250 6300
Wire Wire Line
	6250 6300 6250 6250
Connection ~ 6650 6000
Wire Wire Line
	5800 5800 5800 6000
Wire Wire Line
	5800 5800 6250 5800
Wire Wire Line
	6550 5900 6250 5900
Wire Wire Line
	6250 5900 6250 5800
Connection ~ 6250 5800
Wire Wire Line
	6250 5800 7250 5800
$Comp
L eload:PWR_FLAG #FLG0103
U 1 1 5BAAB62C
P 9800 5900
F 0 "#FLG0103" H 9800 5975 50  0001 C CNN
F 1 "PWR_FLAG" H 9800 6074 50  0001 C CNN
F 2 "" H 9800 5900 50  0001 C CNN
F 3 "~" H 9800 5900 50  0001 C CNN
	1    9800 5900
	1    0    0    -1  
$EndComp
Connection ~ 9800 5900
Wire Wire Line
	9800 5900 9850 5900
Wire Wire Line
	1200 3600 1600 3600
Wire Wire Line
	1600 3200 1600 3250
Wire Wire Line
	1200 3200 1600 3200
Connection ~ 3800 4250
Wire Wire Line
	3800 4250 3950 4250
Wire Wire Line
	4250 4250 4550 4250
Wire Wire Line
	3800 4250 3800 4400
$Comp
L eload:GNDD #PWR?
U 1 1 5BB00CBC
P 2100 4650
AR Path="/5B5FBC03/5BB00CBC" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB00CBC" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 2100 4400 50  0001 C CNN
F 1 "GNDD" H 2105 4477 50  0000 C CNN
F 2 "" H 2100 4650 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2100 4650 50  0001 C CNN
	1    2100 4650
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5BB00F60
P 2100 5500
AR Path="/5B5FBC03/5BB00F60" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB00F60" Ref="#PWR0103"  Part="1" 
F 0 "#PWR0103" H 2100 5250 50  0001 C CNN
F 1 "GNDD" H 2105 5327 50  0000 C CNN
F 2 "" H 2100 5500 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2100 5500 50  0001 C CNN
	1    2100 5500
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5BB00FE1
P 2900 4700
AR Path="/5B5FBC03/5BB00FE1" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB00FE1" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 2900 4450 50  0001 C CNN
F 1 "GNDD" H 2905 4527 50  0000 C CNN
F 2 "" H 2900 4700 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2900 4700 50  0001 C CNN
	1    2900 4700
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5BB01062
P 3300 4750
AR Path="/5B5FBC03/5BB01062" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB01062" Ref="#PWR0105"  Part="1" 
F 0 "#PWR0105" H 3300 4500 50  0001 C CNN
F 1 "GNDD" H 3305 4577 50  0000 C CNN
F 2 "" H 3300 4750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3300 4750 50  0001 C CNN
	1    3300 4750
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5BB010E3
P 3800 4750
AR Path="/5B5FBC03/5BB010E3" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB010E3" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 3800 4500 50  0001 C CNN
F 1 "GNDD" H 3805 4577 50  0000 C CNN
F 2 "" H 3800 4750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3800 4750 50  0001 C CNN
	1    3800 4750
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5BB01164
P 4750 4750
AR Path="/5B5FBC03/5BB01164" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB01164" Ref="#PWR0107"  Part="1" 
F 0 "#PWR0107" H 4750 4500 50  0001 C CNN
F 1 "GNDD" H 4755 4577 50  0000 C CNN
F 2 "" H 4750 4750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4750 4750 50  0001 C CNN
	1    4750 4750
	1    0    0    -1  
$EndComp
$Comp
L eload:-0V232 #PWR0525
U 1 1 5BB02484
P 9850 6400
F 0 "#PWR0525" H 9850 6250 50  0001 C CNN
F 1 "-0V232" H 9855 6573 50  0000 C CNN
F 2 "" H 9850 6400 50  0001 C CNN
F 3 "" H 9850 6400 50  0001 C CNN
	1    9850 6400
	-1   0    0    1   
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5BB02505
P 9300 6400
AR Path="/5B6010A5/5BB02505" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BB02505" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5BB02505" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB02505" Ref="#PWR0524"  Part="1" 
F 0 "#PWR0524" H 9300 6150 50  0001 C CNN
F 1 "GNDA" H 9305 6227 50  0000 C CNN
F 2 "" H 9300 6400 50  0001 C CNN
F 3 "" H 9300 6400 50  0001 C CNN
	1    9300 6400
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R517
U 1 1 5BB0272A
P 9550 6350
F 0 "R517" V 9500 6100 50  0000 C CNN
F 1 "0R" V 9500 6550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9590 6340 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 9550 6350 50  0001 C CNN
F 4 "~" H 5700 3200 50  0001 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H 5700 3200 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H 5700 3200 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H 5700 3200 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H 5700 3200 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H 5700 3200 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H 5700 3200 50  0001 C CNN "Power"
	1    9550 6350
	0    1    1    0   
$EndComp
Wire Wire Line
	9400 6350 9300 6350
Wire Wire Line
	9300 6350 9300 6400
Wire Wire Line
	9700 6350 9850 6350
Wire Wire Line
	9850 6350 9850 6400
$Comp
L eload:GNDD #PWR?
U 1 1 5B9F611E
P 7500 2900
AR Path="/5B5FBC03/5B9F611E" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B9F611E" Ref="#PWR0512"  Part="1" 
F 0 "#PWR0512" H 7500 2650 50  0001 C CNN
F 1 "GNDD" H 7505 2727 50  0000 C CNN
F 2 "" H 7500 2900 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 7500 2900 50  0001 C CNN
	1    7500 2900
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B9F61A1
P 7900 2750
AR Path="/5B5FBC03/5B9F61A1" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B9F61A1" Ref="#PWR0511"  Part="1" 
F 0 "#PWR0511" H 7900 2500 50  0001 C CNN
F 1 "GNDD" H 7905 2577 50  0000 C CNN
F 2 "" H 7900 2750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 7900 2750 50  0001 C CNN
	1    7900 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2900 7500 2850
Wire Wire Line
	4200 3150 4200 3250
$Comp
L eload:GNDA #PWR?
U 1 1 5B8A4818
P 4000 3250
AR Path="/5B6010A5/5B8A4818" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8A4818" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B8A4818" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8A4818" Ref="#PWR0111"  Part="1" 
F 0 "#PWR0111" H 4000 3000 50  0001 C CNN
F 1 "GNDA" H 4005 3077 50  0000 C CNN
F 2 "" H 4000 3250 50  0001 C CNN
F 3 "" H 4000 3250 50  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 2950 4000 3250
$Comp
L eload:CP C?
U 1 1 5B8BD87E
P 5050 1400
AR Path="/5B5FBC06/5B8BD87E" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B8BD87E" Ref="C503"  Part="1" 
F 0 "C503" H 5168 1446 50  0000 L CNN
F 1 "10uF" H 5168 1355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 5088 1250 50  0001 C CNN
F 3 "~" H 5050 1400 50  0001 C CNN
	1    5050 1400
	1    0    0    -1  
$EndComp
$Comp
L eload:CP C?
U 1 1 5B8C8E73
P 6700 3800
AR Path="/5B5FBC06/5B8C8E73" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B8C8E73" Ref="C510"  Part="1" 
F 0 "C510" H 6818 3846 50  0000 L CNN
F 1 "10uF" H 6818 3755 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 6738 3650 50  0001 C CNN
F 3 "~" H 6700 3800 50  0001 C CNN
	1    6700 3800
	1    0    0    -1  
$EndComp
$Comp
L eload:CP C?
U 1 1 5B8C945E
P 9200 4300
AR Path="/5B5FBC06/5B8C945E" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B8C945E" Ref="C515"  Part="1" 
F 0 "C515" H 9318 4346 50  0000 L CNN
F 1 "10uF" H 9318 4255 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 9238 4150 50  0001 C CNN
F 3 "~" H 9200 4300 50  0001 C CNN
	1    9200 4300
	1    0    0    -1  
$EndComp
$Comp
L eload:CP C?
U 1 1 5B8CA32C
P 4750 4500
AR Path="/5B5FBC06/5B8CA32C" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B8CA32C" Ref="C513"  Part="1" 
F 0 "C513" H 4868 4546 50  0000 L CNN
F 1 "10uF" H 4868 4455 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 4788 4350 50  0001 C CNN
F 3 "~" H 4750 4500 50  0001 C CNN
	1    4750 4500
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B8CF78B
P 8950 1600
AR Path="/5B5FBC03/5B8CF78B" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B8CF78B" Ref="#PWR0526"  Part="1" 
F 0 "#PWR0526" H 8950 1350 50  0001 C CNN
F 1 "GNDD" H 8955 1427 50  0000 C CNN
F 2 "" H 8950 1600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 8950 1600 50  0001 C CNN
	1    8950 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 1550 8950 1600
$Comp
L eload:CP C?
U 1 1 5B8CF792
P 8950 1400
AR Path="/5B5FBC06/5B8CF792" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B8CF792" Ref="C525"  Part="1" 
F 0 "C525" H 9068 1446 50  0000 L CNN
F 1 "10uF" H 9068 1355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 8988 1250 50  0001 C CNN
F 3 "~" H 8950 1400 50  0001 C CNN
	1    8950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 1200 9050 1150
Wire Wire Line
	8600 1200 8950 1200
Wire Wire Line
	8950 1250 8950 1200
Connection ~ 8950 1200
Wire Wire Line
	8950 1200 9050 1200
Wire Wire Line
	2750 1150 3400 1150
Connection ~ 2750 1150
$Comp
L eload:GNDD #PWR?
U 1 1 5B85EF29
P 3400 1600
AR Path="/5B5FBC03/5B85EF29" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B85EF29" Ref="#PWR0527"  Part="1" 
F 0 "#PWR0527" H 3400 1350 50  0001 C CNN
F 1 "GNDD" H 3405 1427 50  0000 C CNN
F 2 "" H 3400 1600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3400 1600 50  0001 C CNN
	1    3400 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1550 3400 1600
$Comp
L eload:CP C?
U 1 1 5B85EF30
P 3400 1400
AR Path="/5B5FBC06/5B85EF30" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B85EF30" Ref="C526"  Part="1" 
F 0 "C526" H 3518 1446 50  0000 L CNN
F 1 "10uF" H 3518 1355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 3438 1250 50  0001 C CNN
F 3 "~" H 3400 1400 50  0001 C CNN
	1    3400 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 1250 3400 1150
Connection ~ 3400 1150
Wire Wire Line
	3400 1150 3850 1150
$Comp
L eload:+5V0_FILT #PWR0528
U 1 1 5B885855
P 1250 4650
F 0 "#PWR0528" H 1250 4500 50  0001 C CNN
F 1 "+5V0_FILT" H 1255 4823 50  0000 C CNN
F 2 "" H 1250 4650 50  0001 C CNN
F 3 "" H 1250 4650 50  0001 C CNN
	1    1250 4650
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B8910BD
P 1550 4700
AR Path="/5B8910BD" Ref="R?"  Part="1" 
AR Path="/5B6010A5/5B8910BD" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B8910BD" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B8910BD" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5B8910BD" Ref="R518"  Part="1" 
F 0 "R518" V 1663 4700 50  0000 C CNN
F 1 "10k" V 1754 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 1590 4690 50  0001 L CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1550 4700 50  0001 L CNN
F 4 "±1%" H 1600 4600 50  0001 L CNN "Tolerance"
F 5 "±100ppm/°C" H 1550 4700 50  0001 L CNN "Temperature Coefficient"
F 6 "CRCW080510K0FKEAC" H 1550 4700 50  0001 L CNN "Manufacturer Part Number"
F 7 "Vishay Dale" H 1550 4700 50  0001 L CNN "Manufacturer"
F 8 " 541-3976-1-ND " H 1550 4700 50  0001 L CNN "Digi-Key Part Number"
F 9 "-55°C ~ 155°C" H 250 2150 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W " H 250 2150 50  0001 C CNN "Power"
F 11 "~" H -300 1550 50  0001 C CNN "Stuff"
F 12 "GWCR0805-10KFT5 " H -300 1550 50  0001 C CNN "Mouser Part Number"
	1    1550 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 4650 1250 4700
Wire Wire Line
	1250 4700 1400 4700
Wire Wire Line
	1650 5050 1850 5050
Wire Wire Line
	1700 4700 1850 4700
Wire Wire Line
	1850 4700 1850 5050
Connection ~ 1850 5050
Wire Wire Line
	1850 5050 2100 5050
$Comp
L eload:R_US R519
U 1 1 5B87E8E3
P 2550 2950
F 0 "R519" V 2663 2950 50  0000 C CNN
F 1 "0R" V 2754 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2590 2940 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 2550 2950 50  0001 C CNN
F 4 "DNS" V 2845 2950 50  0000 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H -1300 -200 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H -1300 -200 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H -1300 -200 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H -1300 -200 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H -1300 -200 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H -1300 -200 50  0001 C CNN "Power"
	1    2550 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 2950 2700 2950
Connection ~ 2800 2950
Wire Wire Line
	1750 2800 3050 2800
Wire Wire Line
	2100 2650 2150 2650
Wire Wire Line
	2400 2950 2150 2950
Wire Wire Line
	2150 2950 2150 2650
Connection ~ 2150 2650
Wire Wire Line
	2150 2650 3050 2650
$Comp
L eload:Conn_01x02 J502
U 1 1 5B91A880
P 800 1250
F 0 "J502" H 800 1000 50  0000 C CNN
F 1 "Conn_01x02" H 720 1016 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 800 1250 50  0001 C CNN
F 3 "~" H 800 1250 50  0001 C CNN
	1    800  1250
	-1   0    0    1   
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B91AC4B
P 1050 1300
AR Path="/5B5FBC03/5B91AC4B" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5B91AC4B" Ref="#PWR0533"  Part="1" 
F 0 "#PWR0533" H 1050 1050 50  0001 C CNN
F 1 "GNDD" H 1055 1127 50  0000 C CNN
F 2 "" H 1050 1300 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1050 1300 50  0001 C CNN
	1    1050 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1250 1050 1250
Wire Wire Line
	1050 1250 1050 1300
Text Label 1100 1150 0    50   ~ 0
VBUS
Wire Wire Line
	1000 1150 1100 1150
Wire Wire Line
	5150 2700 5150 2800
Wire Wire Line
	3800 2650 5000 2650
Connection ~ 5150 2800
Wire Wire Line
	5150 2800 5250 2800
$Comp
L eload:L L502
U 1 1 5B885C0C
P 7500 3600
F 0 "L502" V 7690 3600 50  0000 C CNN
F 1 "10u" V 7599 3600 50  0000 C CNN
F 2 "Inductor_SMD:L_1210_3225Metric" H 7500 3600 50  0001 C CNN
F 3 "~" H 7500 3600 50  0001 C CNN
	1    7500 3600
	0    -1   -1   0   
$EndComp
NoConn ~ 2150 1550
$EndSCHEMATC

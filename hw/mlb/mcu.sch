EESchema Schematic File Version 4
LIBS:eLoad-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 6
Title ""
Date "2018-08-10"
Rev "0.1"
Comp "Peter Yin"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eload:STM32F301C8Tx U301
U 1 1 5BB9642C
P 3950 900
F 0 "U301" H 4000 1050 50  0000 C CNN
F 1 "STM32F301C8Tx" H 4250 950 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 3350 -500 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00093332.pdf" H 3950 900 50  0001 C CNN
F 4 "497-17409-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "STM32F301C8T6" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 85°C (TA)" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "~" H 0   0   50  0001 C CNN "Stuff"
F 9 "511-STM32F301C8T6" H 3950 900 50  0001 C CNN "Mouser Part Number"
	1    3950 900 
	1    0    0    -1  
$EndComp
$Comp
L eload:Crystal Y302
U 1 1 5BB9E248
P 2650 2400
F 0 "Y302" V 2604 2531 50  0000 L CNN
F 1 "8MHZ" V 2695 2531 50  0000 L CNN
F 2 "eload_fp:Crystal_SMD_5032-2Pin_5.0x3.2mm" H 2650 2400 50  0001 C CNN
F 3 "https://abracon.com/Resonators/abm3b.pdf" H 2650 2400 50  0001 C CNN
F 4 "535-10630-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Abracon LLC" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ABM3-8.000MHZ-D2Y-T" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-20°C ~ 70°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "±20ppm " H 0   0   50  0001 C CNN "Tolerance"
F 9 "~" H 0   0   50  0001 C CNN "Stuff"
F 10 "717-7A-8.000MAAJ-T " H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2650 2400
	0    1    1    0   
$EndComp
$Comp
L eload:C C304
U 1 1 5BB9E89F
P 2350 2200
F 0 "C304" V 2300 2000 50  0000 C CNN
F 1 "33pF" V 2300 2350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2388 2050 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21C330JBANNNC.jsp" H 2350 2200 50  0001 C CNN
F 4 "1276-1130-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL21C300JBANNNC" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 125°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "C0G, NP0 " H 0   0   50  0001 C CNN "Temperature Coefficient"
F 9 "±5%" H 0   0   50  0001 C CNN "Tolerance"
F 10 "50V" H 0   0   50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "80-C0805C300M5HACTU" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2350 2200
	0    1    1    0   
$EndComp
$Comp
L eload:C C305
U 1 1 5BB9E9E8
P 2350 2600
F 0 "C305" V 2400 2400 50  0000 C CNN
F 1 "33pF" V 2400 2750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2388 2450 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21C330JBANNNC.jsp" H 2350 2600 50  0001 C CNN
F 4 "1276-1130-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL21C300JBANNNC" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 125°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "C0G, NP0 " H 0   0   50  0001 C CNN "Temperature Coefficient"
F 9 "±5%" H 0   0   50  0001 C CNN "Tolerance"
F 10 "50V" H 0   0   50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "80-C0805C300M5HACTU" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2350 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2550 2650 2600
Wire Wire Line
	2650 2600 2500 2600
Wire Wire Line
	2500 2200 2650 2200
Wire Wire Line
	2650 2200 2650 2250
Wire Wire Line
	2200 2200 2150 2200
Wire Wire Line
	2150 2200 2150 2400
Wire Wire Line
	2150 2600 2200 2600
$Comp
L eload:GNDD #PWR0308
U 1 1 5BBA33DA
P 1850 2450
F 0 "#PWR0308" H 1850 2200 50  0001 C CNN
F 1 "GNDD" H 1855 2277 50  0000 C CNN
F 2 "" H 1850 2450 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1850 2450 50  0001 C CNN
	1    1850 2450
	1    0    0    -1  
$EndComp
Connection ~ 2150 2400
Wire Wire Line
	2150 2400 2150 2600
$Comp
L eload:C C302
U 1 1 5BBA50EB
P 3000 1500
F 0 "C302" V 2950 1300 50  0000 C CNN
F 1 "20pF" V 2950 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3038 1350 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21C200JBANNNC.jsp" H 3000 1500 50  0001 C CNN
F 4 "1276-1829-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL21C200JBANNNC" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 125°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "C0G, NP0 " H 0   0   50  0001 C CNN "Temperature Coefficient"
F 9 "±5%" H 0   0   50  0001 C CNN "Tolerance"
F 10 "50V" H 0   0   50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "603-CC805JRNPO9BN200" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    3000 1500
	0    1    1    0   
$EndComp
$Comp
L eload:C C303
U 1 1 5BBA50F2
P 3000 1900
F 0 "C303" V 3050 1700 50  0000 C CNN
F 1 "20pF" V 3050 2100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3038 1750 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21C200JBANNNC.jsp" H 3000 1900 50  0001 C CNN
F 4 "1276-1829-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL21C200JBANNNC" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 125°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "C0G, NP0 " H 0   0   50  0001 C CNN "Temperature Coefficient"
F 9 "±5%" H 0   0   50  0001 C CNN "Tolerance"
F 10 "50V" H 0   0   50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "603-CC805JRNPO9BN200" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    3000 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	3300 1850 3300 1900
Wire Wire Line
	3300 1900 3150 1900
Wire Wire Line
	3150 1500 3300 1500
Wire Wire Line
	3300 1500 3300 1550
Wire Wire Line
	2850 1500 2800 1500
Wire Wire Line
	2800 1500 2800 1700
Wire Wire Line
	2800 1900 2850 1900
$Comp
L eload:GNDD #PWR0306
U 1 1 5BBA5100
P 2500 1750
F 0 "#PWR0306" H 2500 1500 50  0001 C CNN
F 1 "GNDD" H 2505 1577 50  0000 C CNN
F 2 "" H 2500 1750 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2500 1750 50  0001 C CNN
	1    2500 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 1750 2500 1700
Connection ~ 2800 1700
Wire Wire Line
	2800 1700 2800 1900
Wire Wire Line
	3850 1500 3300 1500
Connection ~ 3300 1500
Wire Wire Line
	3850 1900 3300 1900
Connection ~ 3300 1900
Wire Wire Line
	3850 2200 2650 2200
Connection ~ 2650 2200
Wire Wire Line
	3850 2600 2650 2600
Connection ~ 2650 2600
$Comp
L eload:Battery_Cell BT301
U 1 1 5BBFA38A
P 1200 7000
F 0 "BT301" H 1318 7096 50  0000 L CNN
F 1 "Battery_Cell" H 1318 7005 50  0000 L CNN
F 2 "eload_fp:BatteryHolder_Keystone_1060_1x2032" V 1200 7060 50  0001 C CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=726" V 1200 7060 50  0001 C CNN
F 4 "DNS" H -150 50  50  0001 C CNN "Stuff"
F 5 "36-1060-ND" H -150 50  50  0001 C CNN "Digi-Key Part Number"
F 6 "Keystone Electronics" H -150 50  50  0001 C CNN "Manufacturer"
F 7 "1060" H -150 50  50  0001 C CNN "Manufacturer Part Number"
F 8 "534-1060" H -150 50  50  0001 C CNN "Mouser Part Number"
F 9 "-50°C ~ 145°C" H -150 50  50  0001 C CNN "Operating Temperature"
	1    1200 7000
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R302
U 1 1 5BBFEB31
P 3000 1050
F 0 "R302" V 2795 1050 50  0000 C CNN
F 1 "10k" V 2886 1050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 1040 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 3000 1050 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 0   0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 0   0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 0   0   50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H 0   0   50  0001 C CNN "Mouser Part Number"
	1    3000 1050
	0    1    1    0   
$EndComp
$Comp
L eload:GNDD #PWR0303
U 1 1 5BC0101E
P 2750 1100
F 0 "#PWR0303" H 2750 850 50  0001 C CNN
F 1 "GNDD" H 2755 927 50  0000 C CNN
F 2 "" H 2750 1100 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2750 1100 50  0001 C CNN
	1    2750 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1050 2750 1050
Wire Wire Line
	2750 1050 2750 1100
$Comp
L eload:R_US R?
U 1 1 5BC0D6E8
P 2600 3000
AR Path="/5B6010A5/5BC0D6E8" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BC0D6E8" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BC0D6E8" Ref="R306"  Part="1" 
F 0 "R306" V 2550 2800 50  0000 C CNN
F 1 "100R" V 2550 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 2990 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2600 3000 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2600 3000 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2600 3000 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2600 3000 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2600 3000 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2600 3000 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2600 3000 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2600 3000 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2600 3000
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BC0D850
P 2600 3100
AR Path="/5B6010A5/5BC0D850" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BC0D850" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BC0D850" Ref="R307"  Part="1" 
F 0 "R307" V 2550 2900 50  0000 C CNN
F 1 "100R" V 2550 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 3090 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2600 3100 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2600 3100 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2600 3100 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2600 3100 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2600 3100 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2600 3100 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2600 3100 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2600 3100 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2600 3100
	0    1    1    0   
$EndComp
Text HLabel 1200 3000 0    50   Output ~ 0
MCU_FTDI_TX
Text HLabel 1200 3100 0    50   Input ~ 0
MCU_FTDI_RX
Text HLabel 7650 4350 2    50   Output ~ 0
RS_SW
Text HLabel 8000 4650 2    50   Output ~ 0
I_SET
Text HLabel 8000 4950 2    50   Input ~ 0
I_SENSE
Text HLabel 8000 5450 2    50   Input ~ 0
V_SENSE
Text Label 3450 1050 0    50   ~ 0
BOOT0_R
Wire Wire Line
	3150 1050 3850 1050
Text Label 3050 3000 0    50   ~ 0
MCU_FTDI_TX_R
Text Label 3050 3100 0    50   ~ 0
MCU_FTDI_RX_R
Wire Wire Line
	2750 3000 3850 3000
Wire Wire Line
	2750 3100 3850 3100
Text Label 3400 1500 0    50   ~ 0
XTAL32_C1
Text Label 3400 1900 0    50   ~ 0
XTAL32_C2
Text Label 3400 2200 0    50   ~ 0
XTAL8_C+
Text Label 3400 2600 0    50   ~ 0
XTAL8_C-
$Comp
L eload:R_US R?
U 1 1 5BC759FB
P 7250 4650
AR Path="/5B6010A5/5BC759FB" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BC759FB" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BC759FB" Ref="R323"  Part="1" 
F 0 "R323" V 7200 4450 50  0000 C CNN
F 1 "100R" V 7200 4850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7290 4640 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7250 4650 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7250 4650 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7250 4650 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7250 4650 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7250 4650 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7250 4650 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7250 4650 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7250 4650 50  0001 C CNN "Operating Temperature"
F 11 "~" H 450 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 450 0   50  0001 C CNN "Mouser Part Number"
	1    7250 4650
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BC75A71
P 7250 4950
AR Path="/5B6010A5/5BC75A71" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BC75A71" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BC75A71" Ref="R324"  Part="1" 
F 0 "R324" V 7200 4750 50  0000 C CNN
F 1 "100R" V 7200 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7290 4940 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7250 4950 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7250 4950 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7250 4950 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7250 4950 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7250 4950 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7250 4950 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7250 4950 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7250 4950 50  0001 C CNN "Operating Temperature"
F 11 "~" H 450 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 450 0   50  0001 C CNN "Mouser Part Number"
	1    7250 4950
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BC75AEB
P 7250 5450
AR Path="/5B6010A5/5BC75AEB" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BC75AEB" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BC75AEB" Ref="R329"  Part="1" 
F 0 "R329" V 7200 5250 50  0000 C CNN
F 1 "100R" V 7200 5650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7290 5440 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7250 5450 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7250 5450 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7250 5450 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7250 5450 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7250 5450 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7250 5450 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7250 5450 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7250 5450 50  0001 C CNN "Operating Temperature"
F 11 "~" H 450 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 450 0   50  0001 C CNN "Mouser Part Number"
	1    7250 5450
	0    1    1    0   
$EndComp
$Comp
L eload:STM32F301C8Tx U301
U 2 1 5BCE54A2
P 4350 6150
F 0 "U301" H 4350 6300 50  0000 L CNN
F 1 "STM32F301C8Tx" H 4350 6200 50  0000 L CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 6650 6200 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00093332.pdf" H 4950 4750 50  0001 C CNN
F 4 "497-17409-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "STMicroelectronics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "STM32F301C8T6" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 85°C (TA)" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "~" H 0   0   50  0001 C CNN "Stuff"
F 9 "511-STM32F301C8T6" H 0   0   50  0001 C CNN "Mouser Part Number"
	2    4350 6150
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B86CE66
P 2600 3450
AR Path="/5B6010A5/5B86CE66" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B86CE66" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B86CE66" Ref="R308"  Part="1" 
F 0 "R308" V 2550 3250 50  0000 C CNN
F 1 "100R" V 2550 3650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 3440 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2600 3450 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2600 3450 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2600 3450 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2600 3450 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2600 3450 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2600 3450 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2600 3450 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2600 3450 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2600 3450
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B86CE74
P 2600 3550
AR Path="/5B6010A5/5B86CE74" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B86CE74" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B86CE74" Ref="R310"  Part="1" 
F 0 "R310" V 2550 3350 50  0000 C CNN
F 1 "100R" V 2550 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 3540 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2600 3550 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2600 3550 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2600 3550 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2600 3550 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2600 3550 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2600 3550 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2600 3550 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2600 3550 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2600 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 3450 3850 3450
Wire Wire Line
	2750 3550 3850 3550
Text Label 3050 3450 0    50   ~ 0
MCU_ESP_TX_R
Text Label 3050 3550 0    50   ~ 0
MCU_ESP_RX_R
Text HLabel 3550 4200 0    50   Output ~ 0
MCU_SD_SCK
Text HLabel 3550 4300 0    50   Input ~ 0
MCU_SD_MISO
Text HLabel 3550 4400 0    50   Output ~ 0
MCU_SD_MOSI
Text HLabel 3550 4500 0    50   Output ~ 0
MCU_SD_CS
Text HLabel 3550 4600 0    50   Input ~ 0
MCU_SD_DET
Text Label 10000 950  2    50   ~ 0
SWD_TCK
Text Label 10000 1150 2    50   ~ 0
SWD_TMS
Text Label 10050 1250 2    50   ~ 0
SWD_NRST
$Comp
L eload:+3V3 #PWR0302
U 1 1 5B931B96
P 10100 750
F 0 "#PWR0302" H 10100 600 50  0001 C CNN
F 1 "+3V3" H 10105 923 50  0000 C CNN
F 2 "" H 10100 750 50  0001 C CNN
F 3 "" H 10100 750 50  0001 C CNN
	1    10100 750 
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR0304
U 1 1 5B95B4F0
P 10100 1350
F 0 "#PWR0304" H 10100 1100 50  0001 C CNN
F 1 "GNDD" H 10105 1177 50  0000 C CNN
F 2 "" H 10100 1350 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 10100 1350 50  0001 C CNN
	1    10100 1350
	1    0    0    -1  
$EndComp
Text Label 6600 1250 0    50   ~ 0
SWD_TCK_R
Text Label 6600 1350 0    50   ~ 0
SWD_TMS_R
Wire Wire Line
	6600 1250 6400 1250
Wire Wire Line
	6600 1350 6400 1350
Wire Wire Line
	7800 3350 7500 3350
$Comp
L eload:R_US R?
U 1 1 5BA9910A
P 7350 3850
AR Path="/5B6010A5/5BA9910A" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BA9910A" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BA9910A" Ref="R313"  Part="1" 
F 0 "R313" V 7300 3650 50  0000 C CNN
F 1 "100R" V 7300 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7390 3840 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7350 3850 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7350 3850 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7350 3850 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7350 3850 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7350 3850 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7350 3850 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7350 3850 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7350 3850 50  0001 C CNN "Operating Temperature"
F 11 "~" H 550 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 550 0   50  0001 C CNN "Mouser Part Number"
	1    7350 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	7500 3850 7750 3850
Wire Wire Line
	6400 4650 7100 4650
Wire Wire Line
	7100 4950 6400 4950
Wire Wire Line
	6400 5450 7100 5450
Text HLabel 7800 3200 2    50   Output ~ 0
FAN_SW
Text HLabel 7800 3350 2    50   Input ~ 0
FAN_TACH
Text HLabel 7750 3850 2    50   Input ~ 0
TEMP_SENSE
$Comp
L eload:R_US R?
U 1 1 5BB121A0
P 9450 2050
AR Path="/5B6010A5/5BB121A0" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BB121A0" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BB121A0" Ref="R305"  Part="1" 
F 0 "R305" V 9400 1850 50  0000 C CNN
F 1 "100R" V 9400 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9490 2040 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9450 2050 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9450 2050 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9450 2050 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9450 2050 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9450 2050 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9450 2050 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9450 2050 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9450 2050 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2700 100 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2700 100 50  0001 C CNN "Mouser Part Number"
	1    9450 2050
	0    1    1    0   
$EndComp
$Comp
L eload:C C309
U 1 1 5BB391DA
P 2650 6450
F 0 "C309" H 2535 6404 50  0000 R CNN
F 1 "0.1uF" H 2535 6495 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2688 6300 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 2650 6450 50  0001 C CNN
F 4 "399-1177-1-ND" H -250 150 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H -250 150 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H -250 150 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H -250 150 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H -250 150 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H -250 150 50  0001 C CNN "Tolerance"
F 10 "50V" H -250 150 50  0001 C CNN "Voltage"
F 11 "~" H -250 150 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -250 150 50  0001 C CNN "Mouser Part Number"
	1    2650 6450
	-1   0    0    1   
$EndComp
$Comp
L eload:GNDD #PWR0323
U 1 1 5BB4433B
P 5950 6850
F 0 "#PWR0323" H 5950 6600 50  0001 C CNN
F 1 "GNDD" H 5955 6677 50  0000 C CNN
F 2 "" H 5950 6850 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 5950 6850 50  0001 C CNN
	1    5950 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 6550 5950 6550
Wire Wire Line
	5950 6550 5950 6650
Wire Wire Line
	5700 6750 5950 6750
Connection ~ 5950 6750
Wire Wire Line
	5950 6750 5950 6850
Wire Wire Line
	5700 6650 5950 6650
Connection ~ 5950 6650
Wire Wire Line
	5950 6650 5950 6750
$Comp
L eload:+3V3 #PWR0319
U 1 1 5BB5334A
P 3550 6500
F 0 "#PWR0319" H 3550 6350 50  0001 C CNN
F 1 "+3V3" H 3555 6673 50  0000 C CNN
F 2 "" H 3550 6500 50  0001 C CNN
F 3 "" H 3550 6500 50  0001 C CNN
	1    3550 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 6650 3800 6550
Wire Wire Line
	3800 6750 3800 6650
Connection ~ 3800 6650
$Comp
L eload:C C310
U 1 1 5BB6ABCB
P 3100 6450
F 0 "C310" H 2985 6404 50  0000 R CNN
F 1 "0.1uF" H 2985 6495 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3138 6300 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 3100 6450 50  0001 C CNN
F 4 "399-1177-1-ND" H -200 150 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H -200 150 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H -200 150 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H -200 150 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H -200 150 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H -200 150 50  0001 C CNN "Tolerance"
F 10 "50V" H -200 150 50  0001 C CNN "Voltage"
F 11 "~" H -200 150 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -250 150 50  0001 C CNN "Mouser Part Number"
	1    3100 6450
	-1   0    0    1   
$EndComp
$Comp
L eload:C C308
U 1 1 5BB6B062
P 2200 6450
F 0 "C308" H 2085 6404 50  0000 R CNN
F 1 "0.1uF" H 2085 6495 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2238 6300 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 2200 6450 50  0001 C CNN
F 4 "399-1177-1-ND" H -250 150 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H -250 150 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H -250 150 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H -250 150 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H -250 150 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H -250 150 50  0001 C CNN "Tolerance"
F 10 "50V" H -250 150 50  0001 C CNN "Voltage"
F 11 "~" H -250 150 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -250 150 50  0001 C CNN "Mouser Part Number"
	1    2200 6450
	-1   0    0    1   
$EndComp
$Comp
L eload:+3V3 #PWR0316
U 1 1 5BB735EE
P 2100 6250
F 0 "#PWR0316" H 2100 6100 50  0001 C CNN
F 1 "+3V3" H 2105 6423 50  0000 C CNN
F 2 "" H 2100 6250 50  0001 C CNN
F 3 "" H 2100 6250 50  0001 C CNN
	1    2100 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 6250 2200 6250
Wire Wire Line
	2650 6250 2650 6300
Connection ~ 2650 6250
Wire Wire Line
	2200 6250 2200 6300
Connection ~ 2200 6250
Wire Wire Line
	2200 6250 2650 6250
$Comp
L eload:GNDD #PWR0322
U 1 1 5BB84E4B
P 2100 6650
F 0 "#PWR0322" H 2100 6400 50  0001 C CNN
F 1 "GNDD" H 2105 6477 50  0000 C CNN
F 2 "" H 2100 6650 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2100 6650 50  0001 C CNN
	1    2100 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 6650 2200 6650
Wire Wire Line
	3100 6650 3100 6600
Wire Wire Line
	2650 6650 2650 6600
Connection ~ 2650 6650
Wire Wire Line
	2200 6650 2200 6600
Connection ~ 2200 6650
Wire Wire Line
	2200 6650 2650 6650
$Comp
L eload:GNDD #PWR0325
U 1 1 5BBA30A6
P 1200 7150
F 0 "#PWR0325" H 1200 6900 50  0001 C CNN
F 1 "GNDD" H 1205 6977 50  0000 C CNN
F 2 "" H 1200 7150 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1200 7150 50  0001 C CNN
	1    1200 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 7150 1200 7100
$Comp
L eload:C C312
U 1 1 5BBB3BC8
P 2700 7400
F 0 "C312" H 2585 7354 50  0000 R CNN
F 1 "0.1uF" H 2585 7445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2738 7250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 2700 7400 50  0001 C CNN
F 4 "399-1177-1-ND" H -150 50  50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H -150 50  50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H -150 50  50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H -150 50  50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H -150 50  50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H -150 50  50  0001 C CNN "Tolerance"
F 10 "50V" H -150 50  50  0001 C CNN "Voltage"
F 11 "~" H -150 50  50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -200 50  50  0001 C CNN "Mouser Part Number"
	1    2700 7400
	-1   0    0    1   
$EndComp
$Comp
L eload:+3V3_A #PWR0326
U 1 1 5BBBF1E0
P 2150 7200
AR Path="/5B5FBC06/5BBBF1E0" Ref="#PWR0326"  Part="1" 
AR Path="/5B6C0220/5BBBF1E0" Ref="#PWR?"  Part="1" 
F 0 "#PWR0326" H 2150 7050 50  0001 C CNN
F 1 "+3V3_A" H 2155 7373 50  0000 C CNN
F 2 "" H 2150 7200 50  0001 C CNN
F 3 "" H 2150 7200 50  0001 C CNN
	1    2150 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 7200 2250 7250
Wire Wire Line
	2250 7600 2250 7550
Text Label 6600 1150 0    50   ~ 0
NRST
$Comp
L eload:VBAT #PWR0321
U 1 1 5BC6D3AB
P 1200 6600
F 0 "#PWR0321" H 1200 6450 50  0001 C CNN
F 1 "VBAT" H 1205 6773 50  0000 C CNN
F 2 "" H 1200 6600 50  0001 C CNN
F 3 "" H 1200 6600 50  0001 C CNN
	1    1200 6600
	1    0    0    -1  
$EndComp
$Comp
L eload:VBAT #PWR0324
U 1 1 5BC6D447
P 4100 7000
F 0 "#PWR0324" H 4100 6850 50  0001 C CNN
F 1 "VBAT" H 4105 7173 50  0000 C CNN
F 2 "" H 4100 7000 50  0001 C CNN
F 3 "" H 4100 7000 50  0001 C CNN
	1    4100 7000
	1    0    0    -1  
$EndComp
$Comp
L eload:+3V3_A #PWR0327
U 1 1 5BC72F03
P 3300 7200
AR Path="/5B5FBC06/5BC72F03" Ref="#PWR0327"  Part="1" 
AR Path="/5B6C0220/5BC72F03" Ref="#PWR?"  Part="1" 
F 0 "#PWR0327" H 3300 7050 50  0001 C CNN
F 1 "+3V3_A" H 3305 7373 50  0000 C CNN
F 2 "" H 3300 7200 50  0001 C CNN
F 3 "" H 3300 7200 50  0001 C CNN
	1    3300 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 7200 3300 7250
Connection ~ 2250 7200
$Comp
L eload:GNDA #PWR?
U 1 1 5BC90BB8
P 2150 7600
AR Path="/5B6010A5/5BC90BB8" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BC90BB8" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5BC90BB8" Ref="#PWR0329"  Part="1" 
AR Path="/5B6C0220/5BC90BB8" Ref="#PWR?"  Part="1" 
F 0 "#PWR0329" H 2150 7350 50  0001 C CNN
F 1 "GNDA" H 2155 7427 50  0000 C CNN
F 2 "" H 2150 7600 50  0001 C CNN
F 3 "" H 2150 7600 50  0001 C CNN
	1    2150 7600
	1    0    0    -1  
$EndComp
Connection ~ 2250 7600
$Comp
L eload:GNDA #PWR?
U 1 1 5BCA34C6
P 5750 7300
AR Path="/5B6010A5/5BCA34C6" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BCA34C6" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5BCA34C6" Ref="#PWR0328"  Part="1" 
AR Path="/5B6C0220/5BCA34C6" Ref="#PWR?"  Part="1" 
F 0 "#PWR0328" H 5750 7050 50  0001 C CNN
F 1 "GNDA" H 5755 7127 50  0000 C CNN
F 2 "" H 5750 7300 50  0001 C CNN
F 3 "" H 5750 7300 50  0001 C CNN
	1    5750 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 7250 5750 7250
Wire Wire Line
	5750 7250 5750 7300
Wire Wire Line
	2150 7200 2250 7200
Wire Wire Line
	2150 7600 2250 7600
Text HLabel 1200 5500 0    50   BiDi ~ 0
I2C_SCL
Text HLabel 1200 5600 0    50   BiDi ~ 0
I2C_SDA
Wire Wire Line
	2550 5000 2250 5000
Wire Wire Line
	2250 5000 2250 5500
Wire Wire Line
	2350 5100 2350 5600
Wire Wire Line
	2350 5100 2550 5100
$Comp
L eload:R_US R331
U 1 1 5C09AA87
P 9600 6050
F 0 "R331" V 9805 6050 50  0000 C CNN
F 1 "100R" V 9714 6050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9640 6040 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9600 6050 50  0001 C CNN
F 4 "~" H 100 1500 50  0001 C CNN "Stuff"
F 5 "1276-5224-1-ND" H 100 1500 50  0001 C CNN "Digi-Key Part Number"
F 6 "Samsung Electro-Mechanics" H 100 1500 50  0001 C CNN "Manufacturer"
F 7 "RC2012F101CS" H 100 1500 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 100 1500 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 100 1500 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 100 1500 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 100 1500 50  0001 C CNN "Tolerance"
F 12 "756-GWCR0805-100RFT5" H 100 1500 50  0001 C CNN "Mouser Part Number"
	1    9600 6050
	0    -1   -1   0   
$EndComp
$Comp
L eload:C C307
U 1 1 5C09AA8E
P 9400 6250
F 0 "C307" H 9515 6296 50  0000 L CNN
F 1 "0.1uF" H 9515 6205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9438 6100 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 9400 6250 50  0001 C CNN
F 4 "399-1177-1-ND" H 100 1500 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 100 1500 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 100 1500 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 100 1500 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 100 1500 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 100 1500 50  0001 C CNN "Tolerance"
F 10 "50V" H 100 1500 50  0001 C CNN "Voltage"
F 11 "~" H 100 1500 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 100 1500 50  0001 C CNN "Mouser Part Number"
	1    9400 6250
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R330
U 1 1 5C09AA95
P 10000 5850
F 0 "R330" H 9932 5804 50  0000 R CNN
F 1 "10k" H 9932 5895 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10040 5840 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 10000 5850 50  0001 C CNN
F 4 "~" H 100 1500 50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H 100 1500 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 100 1500 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H 100 1500 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 100 1500 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 100 1500 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 100 1500 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 100 1500 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H 100 1500 50  0001 C CNN "Mouser Part Number"
	1    10000 5850
	-1   0    0    1   
$EndComp
$Comp
L eload:+3V3 #PWR0315
U 1 1 5C09AA9C
P 10000 5650
F 0 "#PWR0315" H 10000 5500 50  0001 C CNN
F 1 "+3V3" H 10005 5823 50  0000 C CNN
F 2 "" H 10000 5650 50  0001 C CNN
F 3 "" H 10000 5650 50  0001 C CNN
	1    10000 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 6050 10000 6100
Wire Wire Line
	10000 6050 10000 6000
Connection ~ 10000 6050
Wire Wire Line
	10000 5700 10000 5650
Wire Wire Line
	9450 6050 9400 6050
Wire Wire Line
	9400 6050 9400 6100
Wire Wire Line
	9400 6450 9400 6400
$Comp
L eload:R_US R?
U 1 1 5C0A5943
P 10100 3350
AR Path="/5B6010A5/5C0A5943" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5C0A5943" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5C0A5943" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5C0A5943" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5C0A5943" Ref="R320"  Part="1" 
F 0 "R320" H 10032 3259 50  0000 R CNN
F 1 "1k" H 10032 3350 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10140 3340 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 10100 3350 50  0001 C CNN
F 4 "Yageo" V 10100 3350 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 10100 3350 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 10100 3350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 10032 3441 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 10100 3350 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 10100 3350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 10100 3350 50  0001 C CNN "Operating Temperature"
F 11 "~" H 250 -2600 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H 250 -2600 50  0001 C CNN "Mouser Part Number"
	1    10100 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	10100 3550 10100 3500
Wire Wire Line
	10100 3100 10100 3200
$Comp
L eload:+3V3 #PWR?
U 1 1 5C0A5953
P 10100 3100
AR Path="/5B5FBC03/5C0A5953" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5C0A5953" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5C0A5953" Ref="#PWR0313"  Part="1" 
F 0 "#PWR0313" H 10100 2950 50  0001 C CNN
F 1 "+3V3" H 10105 3273 50  0000 C CNN
F 2 "" H 10100 3100 50  0001 C CNN
F 3 "" H 10100 3100 50  0001 C CNN
	1    10100 3100
	1    0    0    -1  
$EndComp
Text Label 6600 2850 0    50   ~ 0
RTC_SW
Text Label 6600 2150 0    50   ~ 0
GEN_LED1
Wire Wire Line
	6600 2150 6400 2150
Text Label 9300 6050 2    50   ~ 0
RTC_SW
Text Label 9900 3950 2    50   ~ 0
GEN_LED2
Wire Wire Line
	9400 6050 9300 6050
Connection ~ 9400 6050
Wire Wire Line
	10100 3950 9900 3950
Wire Wire Line
	10100 3850 10100 3950
Wire Wire Line
	7400 4650 8000 4650
Wire Wire Line
	7400 4950 8000 4950
Wire Wire Line
	7400 5450 8000 5450
Text Label 7600 4650 0    50   ~ 0
I_SET
Text Label 7600 4950 0    50   ~ 0
I_SENSE
Text Label 7600 5450 0    50   ~ 0
V_SENSE
$Comp
L eload:R_US R?
U 1 1 5B8EF062
P 2600 3650
AR Path="/5B6010A5/5B8EF062" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B8EF062" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B8EF062" Ref="R311"  Part="1" 
F 0 "R311" V 2550 3450 50  0000 C CNN
F 1 "100R" V 2550 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 3640 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2600 3650 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2600 3650 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2600 3650 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2600 3650 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2600 3650 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2600 3650 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2600 3650 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2600 3650 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2600 3650
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 3650 3850 3650
$Comp
L eload:R_US R?
U 1 1 5B91126D
P 2600 3750
AR Path="/5B6010A5/5B91126D" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B91126D" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B91126D" Ref="R312"  Part="1" 
F 0 "R312" V 2550 3550 50  0000 C CNN
F 1 "100R" V 2550 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2640 3740 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2600 3750 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2600 3750 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2600 3750 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2600 3750 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2600 3750 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2600 3750 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2600 3750 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2600 3750 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2600 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 3750 3850 3750
Text Label 3050 3650 0    50   ~ 0
MCU_ESP_EN_R
Text Label 3050 3750 0    50   ~ 0
MCU_ESP_EX_R
$Comp
L eload:PWR_FLAG #FLG0301
U 1 1 5BBF4605
P 1150 6700
F 0 "#FLG0301" H 1150 6775 50  0001 C CNN
F 1 "PWR_FLAG" V 1150 6828 50  0000 L CNN
F 2 "" H 1150 6700 50  0001 C CNN
F 3 "~" H 1150 6700 50  0001 C CNN
	1    1150 6700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1200 6600 1200 6700
Wire Wire Line
	1150 6700 1200 6700
Connection ~ 1200 6700
$Comp
L eload:LED D?
U 1 1 5BCA856B
P 10100 3700
AR Path="/5B5FBC03/5BCA856B" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5BCA856B" Ref="D?"  Part="1" 
AR Path="/5B5FBC06/5BCA856B" Ref="D302"  Part="1" 
F 0 "D302" V 10138 3583 50  0000 R CNN
F 1 "RED" V 10047 3583 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 10100 3700 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120RS75000.pdf" H 10100 3700 50  0001 C CNN
F 4 "20mA" H 6750 1200 50  0001 C CNN "Current"
F 5 "732-4991-1-ND" H 6750 1200 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H 6750 1200 50  0001 C CNN "Manufacturer"
F 7 "150120RS75000" H 6750 1200 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H 6750 1200 50  0001 C CNN "Voltage"
F 9 "~" H 250 -2600 50  0001 C CNN "Stuff"
F 10 "710-156120RS75000" H 250 -2600 50  0001 C CNN "Mouser Part Number"
	1    10100 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2500 1700 2800 1700
Wire Wire Line
	1850 2400 1850 2450
Wire Wire Line
	1850 2400 2150 2400
$Comp
L eload:C C?
U 1 1 5B78AF3B
P 7150 5150
AR Path="/5B6010A5/5B78AF3B" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B78AF3B" Ref="C?"  Part="1" 
AR Path="/5B5FBC06/5B78AF3B" Ref="C306"  Part="1" 
F 0 "C306" V 7200 5350 50  0000 C CNN
F 1 "10pF" V 7200 4950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7188 5000 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 7150 5150 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 7150 5150 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 7150 5150 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 7150 5150 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 7150 5150 50  0001 C CNN "Tolerance"
F 8 "100V" V 7150 5150 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 7150 5150 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 7150 5150 50  0001 C CNN "Operating Temperature"
F 11 "~" H 200 0   50  0001 C CNN "Stuff"
F 12 "710-885012007051" H 200 0   50  0001 C CNN "Mouser Part Number"
	1    7150 5150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 5150 7000 5150
Wire Wire Line
	7300 5150 7350 5150
Wire Wire Line
	7350 5150 7350 5350
Wire Wire Line
	2700 7200 2700 7250
Wire Wire Line
	2250 7200 2700 7200
Wire Wire Line
	2700 7550 2700 7600
Wire Wire Line
	2250 7600 2700 7600
Wire Wire Line
	3100 6250 3100 6300
Wire Wire Line
	2650 6250 3100 6250
Wire Wire Line
	2650 6650 3100 6650
$Comp
L eload:SW_Push SW302
U 1 1 5B7CEB5C
P 10000 6300
F 0 "SW302" V 9954 6448 50  0000 L CNN
F 1 "SW_Push" V 10045 6448 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H5mm" H 10000 6500 50  0001 C CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 10000 6500 50  0001 C CNN
F 4 "506-FSM4JH" H 100 1500 50  0001 C CNN "Mouser Part Number"
F 5 "450-1650-ND" H 100 1500 50  0001 C CNN "Digi-Key Part Number"
F 6 "TE Connectivity ALCOSWITCH Switches" H 100 1500 50  0001 C CNN "Manufacturer"
F 7 "1825910-6" H 100 1500 50  0001 C CNN "Manufacturer Part Number"
	1    10000 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 6050 10000 6050
Wire Wire Line
	10000 6500 10000 6550
Text Label 6500 3200 0    50   ~ 0
FAN_SW
Text Label 6500 3350 0    50   ~ 0
FAN_TACH_R
Wire Wire Line
	6400 3350 7200 3350
$Comp
L eload:R_US R?
U 1 1 5B910E97
P 2700 5000
AR Path="/5B6010A5/5B910E97" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B910E97" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B910E97" Ref="R325"  Part="1" 
F 0 "R325" V 2650 4800 50  0000 C CNN
F 1 "100R" V 2650 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2740 4990 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2700 5000 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2700 5000 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2700 5000 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2700 5000 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2700 5000 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2700 5000 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2700 5000 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2700 5000 50  0001 C CNN "Operating Temperature"
F 11 "~" H 100 800 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 100 800 50  0001 C CNN "Mouser Part Number"
	1    2700 5000
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B910F8B
P 2700 5100
AR Path="/5B6010A5/5B910F8B" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B910F8B" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B910F8B" Ref="R326"  Part="1" 
F 0 "R326" V 2650 4900 50  0000 C CNN
F 1 "100R" V 2650 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2740 5090 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 2700 5100 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 2700 5100 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 2700 5100 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 2700 5100 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 2700 5100 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 2700 5100 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 2700 5100 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 2700 5100 50  0001 C CNN "Operating Temperature"
F 11 "~" H 100 900 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 100 900 50  0001 C CNN "Mouser Part Number"
	1    2700 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 5000 3850 5000
Wire Wire Line
	3850 5100 2850 5100
Text Label 3050 5000 0    50   ~ 0
MCU_I2C_SCL_R
Text Label 3050 5100 0    50   ~ 0
MCU_I2C_SDA_R
Wire Wire Line
	6400 3200 7800 3200
Wire Wire Line
	6400 3850 7200 3850
Text Label 6450 3850 0    50   ~ 0
TEMP_SENSE_R
Text Label 6550 4350 0    50   ~ 0
RS_SW
Text Label 6450 4650 0    50   ~ 0
I_SET_R
Text Label 6450 4950 0    50   ~ 0
I_SENSE_R
Text Label 6450 5450 0    50   ~ 0
V_SENSE_R
Text Label 6450 5250 0    50   ~ 0
OP_IIN
Wire Wire Line
	6800 5250 6800 5150
Wire Wire Line
	6400 5250 6800 5250
Text Label 6450 5350 0    50   ~ 0
OP_OUT
Wire Wire Line
	6400 5350 7350 5350
$Comp
L eload:Conn_01x06 J303
U 1 1 5BB3DFE4
P 850 3750
F 0 "J303" H 850 3300 50  0000 C CNN
F 1 "Conn_01x06" H 1150 3950 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 850 3750 50  0001 C CNN
F 3 "~" H 850 3750 50  0001 C CNN
	1    850  3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 3450 2450 3450
Wire Wire Line
	1050 3550 2450 3550
Wire Wire Line
	1050 3650 2450 3650
Wire Wire Line
	1050 3750 2450 3750
Wire Wire Line
	1050 3950 1200 3950
Wire Wire Line
	1200 3950 1200 4000
Wire Wire Line
	1200 3350 1200 3850
Text Label 1600 3450 0    50   ~ 0
MCU_ESP_TX
Text Label 1600 3550 0    50   ~ 0
MCU_ESP_RX
Text Label 1600 3650 0    50   ~ 0
MCU_ESP_EN
Text Label 1600 3750 0    50   ~ 0
MCU_ESP_EX
Wire Wire Line
	1200 3000 2450 3000
Wire Wire Line
	1200 3100 2450 3100
Text Label 1600 3000 0    50   ~ 0
MCU_FTDI_TX
Text Label 1600 3100 0    50   ~ 0
MCU_FTDI_RX
Wire Wire Line
	1200 6700 1200 6800
Wire Wire Line
	4100 7000 4250 7000
Wire Wire Line
	3800 6550 4250 6550
Wire Wire Line
	3800 6650 4250 6650
Wire Wire Line
	3800 6750 4250 6750
Wire Wire Line
	3550 6500 3550 6550
Wire Wire Line
	3550 6550 3800 6550
Connection ~ 3800 6550
$Comp
L eload:+5V0 #PWR?
U 1 1 5B91E9A1
P 1200 3350
AR Path="/5B6C0220/5B91E9A1" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5B91E9A1" Ref="#PWR0310"  Part="1" 
F 0 "#PWR0310" H 1200 3200 50  0001 C CNN
F 1 "+5V0" H 1205 3523 50  0000 C CNN
F 2 "" H 1200 3350 50  0001 C CNN
F 3 "" H 1200 3350 50  0001 C CNN
	1    1200 3350
	1    0    0    -1  
$EndComp
Text Notes 1600 3300 0    50   ~ 0
*NOTE* ESP Must provide own +3.3V
Text Label 9850 2050 0    50   ~ 0
PA12
$Comp
L eload:GNDD #PWR0309
U 1 1 5B980B01
P 10100 2600
F 0 "#PWR0309" H 10100 2350 50  0001 C CNN
F 1 "GNDD" H 10105 2427 50  0000 C CNN
F 2 "" H 10100 2600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 10100 2600 50  0001 C CNN
	1    10100 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 2550 10100 2550
Wire Wire Line
	10100 2550 10100 2600
$Comp
L eload:+3V3 #PWR0307
U 1 1 5B98E8DC
P 10100 1850
F 0 "#PWR0307" H 10100 1700 50  0001 C CNN
F 1 "+3V3" H 10105 2023 50  0000 C CNN
F 2 "" H 10100 1850 50  0001 C CNN
F 3 "" H 10100 1850 50  0001 C CNN
	1    10100 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 7250 4250 7250
Wire Wire Line
	1200 3850 1050 3850
Text HLabel 6700 1750 2    50   Output ~ 0
+5V0_EN
Wire Wire Line
	6400 1750 6700 1750
Text Label 6600 2250 0    50   ~ 0
GEN_LED2
Wire Wire Line
	6600 2250 6400 2250
Wire Wire Line
	6400 2500 6650 2500
Text Label 6650 2500 0    50   ~ 0
PB1_R
$Comp
L eload:R_US R?
U 1 1 5BE57151
P 9350 3350
AR Path="/5B6010A5/5BE57151" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BE57151" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5BE57151" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5BE57151" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BE57151" Ref="R319"  Part="1" 
F 0 "R319" H 9282 3259 50  0000 R CNN
F 1 "1k" H 9282 3350 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9390 3340 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 9350 3350 50  0001 C CNN
F 4 "Yageo" V 9350 3350 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 9350 3350 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 9350 3350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 9282 3441 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9350 3350 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9350 3350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9350 3350 50  0001 C CNN "Operating Temperature"
F 11 "~" H -500 -2600 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -500 -2600 50  0001 C CNN "Mouser Part Number"
	1    9350 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	9350 3550 9350 3500
Wire Wire Line
	9350 3100 9350 3200
$Comp
L eload:+3V3 #PWR?
U 1 1 5BE5715A
P 9350 3100
AR Path="/5B5FBC03/5BE5715A" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BE5715A" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBC06/5BE5715A" Ref="#PWR0312"  Part="1" 
F 0 "#PWR0312" H 9350 2950 50  0001 C CNN
F 1 "+3V3" H 9355 3273 50  0000 C CNN
F 2 "" H 9350 3100 50  0001 C CNN
F 3 "" H 9350 3100 50  0001 C CNN
	1    9350 3100
	1    0    0    -1  
$EndComp
Text Label 9150 3950 2    50   ~ 0
GEN_LED1
Wire Wire Line
	9350 3950 9150 3950
Wire Wire Line
	9350 3850 9350 3950
$Comp
L eload:LED D?
U 1 1 5BE5716A
P 9350 3700
AR Path="/5B5FBC03/5BE5716A" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5BE5716A" Ref="D?"  Part="1" 
AR Path="/5B5FBC06/5BE5716A" Ref="D301"  Part="1" 
F 0 "D301" V 9388 3583 50  0000 R CNN
F 1 "RED" V 9297 3583 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 9350 3700 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120RS75000.pdf" H 9350 3700 50  0001 C CNN
F 4 "20mA" H 6000 1200 50  0001 C CNN "Current"
F 5 "732-4991-1-ND" H 6000 1200 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H 6000 1200 50  0001 C CNN "Manufacturer"
F 7 "150120RS75000" H 6000 1200 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H 6000 1200 50  0001 C CNN "Voltage"
F 9 "~" H -500 -2600 50  0001 C CNN "Stuff"
F 10 "710-156120RS75000" H -500 -2600 50  0001 C CNN "Mouser Part Number"
	1    9350 3700
	0    -1   -1   0   
$EndComp
Text Label 6650 2700 0    50   ~ 0
PB7_R
Text Label 6600 2350 0    50   ~ 0
PA12_R
Wire Wire Line
	6600 2350 6400 2350
Text Label 9050 2050 2    50   ~ 0
PA12_R
Wire Wire Line
	9300 2050 9050 2050
Wire Wire Line
	9600 2050 10150 2050
$Comp
L eload:Conn_01x05 J301
U 1 1 5BF787FC
P 10350 1050
F 0 "J301" H 10250 1350 50  0000 L CNN
F 1 "Conn_01x05" H 10430 1001 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 10350 1050 50  0001 C CNN
F 3 "~" H 10350 1050 50  0001 C CNN
	1    10350 1050
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B9CE62F
P 7350 3350
AR Path="/5B6010A5/5B9CE62F" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B9CE62F" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B9CE62F" Ref="R309"  Part="1" 
F 0 "R309" V 7300 3150 50  0000 C CNN
F 1 "100R" V 7300 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7390 3340 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7350 3350 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7350 3350 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7350 3350 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7350 3350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7350 3350 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7350 3350 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7350 3350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7350 3350 50  0001 C CNN "Operating Temperature"
F 11 "~" H 350 -150 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 350 -150 50  0001 C CNN "Mouser Part Number"
	1    7350 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 4350 7650 4350
Wire Wire Line
	3850 4200 3550 4200
Wire Wire Line
	3850 4300 3550 4300
Wire Wire Line
	3850 4400 3550 4400
Wire Wire Line
	3850 4500 3550 4500
Wire Wire Line
	3850 4600 3550 4600
$Comp
L eload:GNDD #PWR0320
U 1 1 5BA051A1
P 10000 6550
F 0 "#PWR0320" H 10000 6300 50  0001 C CNN
F 1 "GNDD" H 10005 6377 50  0000 C CNN
F 2 "" H 10000 6550 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 10000 6550 50  0001 C CNN
	1    10000 6550
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR0318
U 1 1 5BA0549C
P 9400 6450
F 0 "#PWR0318" H 9400 6200 50  0001 C CNN
F 1 "GNDD" H 9405 6277 50  0000 C CNN
F 2 "" H 9400 6450 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 9400 6450 50  0001 C CNN
	1    9400 6450
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B80357C
P 9300 950
AR Path="/5B6010A5/5B80357C" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B80357C" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B80357C" Ref="R314"  Part="1" 
F 0 "R314" V 9250 750 50  0000 C CNN
F 1 "100R" V 9250 1150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9340 940 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9300 950 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9300 950 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9300 950 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9300 950 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9300 950 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9300 950 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9300 950 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9300 950 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2550 -800 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2550 -800 50  0001 C CNN "Mouser Part Number"
	1    9300 950 
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B80358C
P 9300 1150
AR Path="/5B6010A5/5B80358C" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B80358C" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B80358C" Ref="R315"  Part="1" 
F 0 "R315" V 9250 950 50  0000 C CNN
F 1 "100R" V 9250 1350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9340 1140 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9300 1150 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9300 1150 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9300 1150 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9300 1150 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9300 1150 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9300 1150 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9300 1150 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9300 1150 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2550 -700 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2550 -700 50  0001 C CNN "Mouser Part Number"
	1    9300 1150
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B80359C
P 9300 1250
AR Path="/5B6010A5/5B80359C" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B80359C" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B80359C" Ref="R316"  Part="1" 
F 0 "R316" V 9250 1050 50  0000 C CNN
F 1 "100R" V 9250 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9340 1240 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9300 1250 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9300 1250 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9300 1250 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9300 1250 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9300 1250 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9300 1250 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9300 1250 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9300 1250 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2550 -700 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2550 -700 50  0001 C CNN "Mouser Part Number"
	1    9300 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	10150 1050 10100 1050
Wire Wire Line
	10100 1050 10100 1350
Wire Wire Line
	10150 850  10100 850 
Wire Wire Line
	10100 850  10100 750 
Wire Wire Line
	9450 1150 10150 1150
Wire Wire Line
	9450 1250 10150 1250
Wire Wire Line
	9450 950  10150 950 
Text Label 9000 950  2    50   ~ 0
SWD_TCK_R
Text Label 9000 1150 2    50   ~ 0
SWD_TMS_R
Wire Wire Line
	9000 950  9150 950 
Wire Wire Line
	9150 1150 9000 1150
Text Label 9000 1250 2    50   ~ 0
NRST
Wire Wire Line
	9000 1250 9150 1250
$Comp
L eload:CP C311
U 1 1 5B8BBFA4
P 2250 7400
F 0 "C311" H 2368 7446 50  0000 L CNN
F 1 "10uF" H 2368 7355 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 2288 7250 50  0001 C CNN
F 3 "~" H 2250 7400 50  0001 C CNN
	1    2250 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2850 6400 2850
$Comp
L eload:GNDD #PWR0113
U 1 1 5B8330A1
P 1200 4000
F 0 "#PWR0113" H 1200 3750 50  0001 C CNN
F 1 "GNDD" H 1205 3827 50  0000 C CNN
F 2 "" H 1200 4000 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1200 4000 50  0001 C CNN
	1    1200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 5500 2250 5500
Wire Wire Line
	1200 5500 1850 5500
Connection ~ 1850 5500
Wire Wire Line
	1850 5500 1850 5450
Wire Wire Line
	1450 5600 2350 5600
Wire Wire Line
	1200 5600 1450 5600
Connection ~ 1450 5600
Wire Wire Line
	1450 5600 1450 5450
Wire Wire Line
	1350 5050 1350 5100
Wire Wire Line
	1850 5100 1850 5150
Wire Wire Line
	1350 5100 1450 5100
Wire Wire Line
	1450 5100 1850 5100
Connection ~ 1450 5100
Wire Wire Line
	1450 5150 1450 5100
$Comp
L eload:+3V3 #PWR0314
U 1 1 5BD2188C
P 1350 5050
F 0 "#PWR0314" H 1350 4900 50  0001 C CNN
F 1 "+3V3" H 1355 5223 50  0000 C CNN
F 2 "" H 1350 5050 50  0001 C CNN
F 3 "" H 1350 5050 50  0001 C CNN
	1    1350 5050
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R327
U 1 1 5BCEA564
P 1450 5300
F 0 "R327" H 1382 5254 50  0000 R CNN
F 1 "10k" H 1382 5345 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1490 5290 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1450 5300 50  0001 C CNN
F 4 "~" H -1350 0   50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H -1350 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H -1350 0   50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H -1350 0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -1350 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H -1350 0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H -1350 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H -1350 0   50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H -1350 0   50  0001 C CNN "Mouser Part Number"
	1    1450 5300
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R328
U 1 1 5BCEA3D6
P 1850 5300
F 0 "R328" H 1782 5254 50  0000 R CNN
F 1 "10k" H 1782 5345 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1890 5290 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1850 5300 50  0001 C CNN
F 4 "~" H -1350 0   50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H -1350 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H -1350 0   50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H -1350 0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -1350 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H -1350 0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H -1350 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H -1350 0   50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H -1350 0   50  0001 C CNN "Mouser Part Number"
	1    1850 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6400 2600 6650 2600
Text Label 6650 2600 0    50   ~ 0
PB2_R
$Comp
L eload:R_US R?
U 1 1 5B94901D
P 9450 2150
AR Path="/5B6010A5/5B94901D" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B94901D" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B94901D" Ref="R317"  Part="1" 
F 0 "R317" V 9400 1950 50  0000 C CNN
F 1 "100R" V 9400 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9490 2140 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9450 2150 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9450 2150 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9450 2150 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9450 2150 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9450 2150 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9450 2150 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9450 2150 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9450 2150 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2700 300 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2700 300 50  0001 C CNN "Mouser Part Number"
	1    9450 2150
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B94902D
P 9450 2250
AR Path="/5B6010A5/5B94902D" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B94902D" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B94902D" Ref="R318"  Part="1" 
F 0 "R318" V 9400 2050 50  0000 C CNN
F 1 "100R" V 9400 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9490 2240 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9450 2250 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9450 2250 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9450 2250 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9450 2250 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9450 2250 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9450 2250 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9450 2250 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9450 2250 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2700 300 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2700 300 50  0001 C CNN "Mouser Part Number"
	1    9450 2250
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 2250 10150 2250
Wire Wire Line
	9600 2150 10150 2150
Text Label 9850 2150 0    50   ~ 0
PB1
Wire Wire Line
	9300 2150 9050 2150
Wire Wire Line
	9300 2250 9050 2250
Text Label 9050 2150 2    50   ~ 0
PB1_R
Text Label 9050 2250 2    50   ~ 0
PB2_R
Text Label 9850 2250 0    50   ~ 0
PB2
Wire Wire Line
	10150 2450 10100 2450
Wire Wire Line
	10100 2450 10100 1850
Wire Wire Line
	6400 2700 6650 2700
$Comp
L eload:R_US R?
U 1 1 5BA18361
P 9450 2350
AR Path="/5B6010A5/5BA18361" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BA18361" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BA18361" Ref="R303"  Part="1" 
F 0 "R303" V 9400 2150 50  0000 C CNN
F 1 "100R" V 9400 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9490 2340 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9450 2350 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9450 2350 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9450 2350 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9450 2350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9450 2350 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9450 2350 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9450 2350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9450 2350 50  0001 C CNN "Operating Temperature"
F 11 "~" H 2700 400 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 2700 400 50  0001 C CNN "Mouser Part Number"
	1    9450 2350
	0    1    1    0   
$EndComp
Wire Wire Line
	9600 2350 10150 2350
Wire Wire Line
	9300 2350 9050 2350
Text Label 9050 2350 2    50   ~ 0
PB7_R
Text Label 9850 2350 0    50   ~ 0
PB7
$Comp
L eload:Crystal Y301
U 1 1 5BA7B8F3
P 3300 1700
F 0 "Y301" V 3254 1831 50  0000 L CNN
F 1 "32.768KHZ" V 3345 1831 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_3215-2Pin_3.2x1.5mm" H 3300 1700 50  0001 C CNN
F 3 "~" H 3300 1700 50  0001 C CNN
	1    3300 1700
	0    1    1    0   
$EndComp
$Comp
L eload:Conn_01x06 J302
U 1 1 5BA95484
P 10350 2250
F 0 "J302" H 10300 2550 50  0000 L CNN
F 1 "Conn_01x06" H 10430 2151 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 10350 2250 50  0001 C CNN
F 3 "~" H 10350 2250 50  0001 C CNN
	1    10350 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3500 7800 3500
Text HLabel 7800 3500 2    50   Output ~ 0
FAN_PWM
Wire Wire Line
	6400 1150 6600 1150
$Comp
L eload:R_US R304
U 1 1 5B9B783A
P 9600 4650
F 0 "R304" V 9805 4650 50  0000 C CNN
F 1 "100R" V 9714 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9640 4640 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9600 4650 50  0001 C CNN
F 4 "~" H 100 100 50  0001 C CNN "Stuff"
F 5 "1276-5224-1-ND" H 100 100 50  0001 C CNN "Digi-Key Part Number"
F 6 "Samsung Electro-Mechanics" H 100 100 50  0001 C CNN "Manufacturer"
F 7 "RC2012F101CS" H 100 100 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 100 100 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 100 100 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 100 100 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 100 100 50  0001 C CNN "Tolerance"
F 12 "756-GWCR0805-100RFT5" H 100 100 50  0001 C CNN "Mouser Part Number"
	1    9600 4650
	0    -1   -1   0   
$EndComp
$Comp
L eload:C C301
U 1 1 5B9B784A
P 9400 4850
F 0 "C301" H 9515 4896 50  0000 L CNN
F 1 "0.1uF" H 9515 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9438 4700 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 9400 4850 50  0001 C CNN
F 4 "399-1177-1-ND" H 100 100 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 100 100 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 100 100 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 100 100 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 100 100 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 100 100 50  0001 C CNN "Tolerance"
F 10 "50V" H 100 100 50  0001 C CNN "Voltage"
F 11 "~" H 100 100 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 100 100 50  0001 C CNN "Mouser Part Number"
	1    9400 4850
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R301
U 1 1 5B9B785A
P 10000 4450
F 0 "R301" H 9932 4404 50  0000 R CNN
F 1 "10k" H 9932 4495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10040 4440 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 10000 4450 50  0001 C CNN
F 4 "~" H 100 100 50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H 100 100 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 100 100 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H 100 100 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 100 100 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 100 100 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 100 100 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 100 100 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H 100 100 50  0001 C CNN "Mouser Part Number"
	1    10000 4450
	-1   0    0    1   
$EndComp
$Comp
L eload:+3V3 #PWR0301
U 1 1 5B9B7861
P 10000 4250
F 0 "#PWR0301" H 10000 4100 50  0001 C CNN
F 1 "+3V3" H 10005 4423 50  0000 C CNN
F 2 "" H 10000 4250 50  0001 C CNN
F 3 "" H 10000 4250 50  0001 C CNN
	1    10000 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4650 10000 4700
Wire Wire Line
	10000 4650 10000 4600
Connection ~ 10000 4650
Wire Wire Line
	10000 4300 10000 4250
Wire Wire Line
	9450 4650 9400 4650
Wire Wire Line
	9400 4650 9400 4700
Wire Wire Line
	9400 5050 9400 5000
Wire Wire Line
	9400 4650 9300 4650
Connection ~ 9400 4650
$Comp
L eload:SW_Push SW301
U 1 1 5B9B7875
P 10000 4900
F 0 "SW301" V 9954 5048 50  0000 L CNN
F 1 "SW_Push" V 10045 5048 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H5mm" H 10000 5100 50  0001 C CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1825910&DocType=Customer+Drawing&DocLang=English" H 10000 5100 50  0001 C CNN
F 4 "506-FSM4JH" H 100 100 50  0001 C CNN "Mouser Part Number"
F 5 "450-1650-ND" H 100 100 50  0001 C CNN "Digi-Key Part Number"
F 6 "TE Connectivity ALCOSWITCH Switches" H 100 100 50  0001 C CNN "Manufacturer"
F 7 "1825910-6" H 100 100 50  0001 C CNN "Manufacturer Part Number"
	1    10000 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	9750 4650 10000 4650
Wire Wire Line
	10000 5100 10000 5150
$Comp
L eload:GNDD #PWR0311
U 1 1 5B9B787E
P 10000 5150
F 0 "#PWR0311" H 10000 4900 50  0001 C CNN
F 1 "GNDD" H 10005 4977 50  0000 C CNN
F 2 "" H 10000 5150 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 10000 5150 50  0001 C CNN
	1    10000 5150
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR0305
U 1 1 5B9B7884
P 9400 5050
F 0 "#PWR0305" H 9400 4800 50  0001 C CNN
F 1 "GNDD" H 9405 4877 50  0000 C CNN
F 2 "" H 9400 5050 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 9400 5050 50  0001 C CNN
	1    9400 5050
	1    0    0    -1  
$EndComp
Text Label 9300 4650 2    50   ~ 0
PA12_R
$EndSCHEMATC

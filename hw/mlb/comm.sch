EESchema Schematic File Version 4
LIBS:eLoad-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 6
Title ""
Date "2018-08-10"
Rev "0.1"
Comp "Peter Yin"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eload:USB_B_Micro J201
U 1 1 5B65A460
P 850 3050
F 0 "J201" H 905 3517 50  0000 C CNN
F 1 "USB_B_Micro" H 905 3426 50  0000 C CNN
F 2 "eload_fp:Amphenol_10118193-0001LF" H 1000 3000 50  0001 C CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/10118193.pdf" H 1000 3000 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "1.8A" H 0   0   50  0001 C CNN "Current"
F 6 "609-4616-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 7 "Amphenol FCI" H 0   0   50  0001 C CNN "Manufacturer"
F 8 "10118193-0001LF" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 9 "-30°C ~ 80°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 10 "649-10118193-0001LF " H 0   0   50  0001 C CNN "Mouser Part Number"
	1    850  3050
	1    0    0    -1  
$EndComp
$Comp
L eload:FT230XS U202
U 1 1 5B65A4EC
P 4150 3150
F 0 "U202" H 3650 3850 50  0000 C CNN
F 1 "FT230XS" H 3750 3750 50  0000 C CNN
F 2 "Package_SO:SSOP-16_3.9x4.9mm_P0.635mm" H 4600 2500 50  0001 C CNN
F 3 "http://www.ftdichip.com/Products/ICs/FT230X.html" H 4150 3150 50  0001 C CNN
F 4 "768-1135-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "FTDI, Future Technology Devices International Ltd" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "FT230XS-R" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 85°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "~" H 0   0   50  0001 C CNN "Stuff"
F 9 "895-FT230XS-R" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    4150 3150
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R203
U 1 1 5B65A87D
P 2750 3050
F 0 "R203" V 2700 2800 50  0000 C CNN
F 1 "27R" V 2700 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2790 3040 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 2750 3050 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "311-27.0CRCT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "RC0805FR-0727RL" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H 0   0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 0   0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 0   0   50  0001 C CNN "Tolerance"
F 12 "652-CR0805FX-27R0ELF" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2750 3050
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R204
U 1 1 5B65A8EC
P 2750 3150
F 0 "R204" V 2700 2900 50  0000 C CNN
F 1 "27R" V 2700 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2790 3140 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 2750 3150 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "311-27.0CRCT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "RC0805FR-0727RL" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H 0   0   50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 0   0   50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 0   0   50  0001 C CNN "Tolerance"
F 12 "652-CR0805FX-27R0ELF" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2750 3150
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 3050 2900 3050
Wire Wire Line
	3450 3150 2900 3150
$Comp
L eload:C C209
U 1 1 5B65B0E2
P 2400 3400
F 0 "C209" H 2515 3446 50  0000 L CNN
F 1 "47pF" H 2515 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2438 3250 50  0001 C CNN
F 3 "~" H 2400 3400 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "710-885012007014" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2400 3400
	1    0    0    -1  
$EndComp
$Comp
L eload:C C208
U 1 1 5B65B1C2
P 2000 3400
F 0 "C208" H 2115 3446 50  0000 L CNN
F 1 "47pF" H 2115 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2038 3250 50  0001 C CNN
F 3 "~" H 2000 3400 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "710-885012007014" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    2000 3400
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDISO #PWR0215
U 1 1 5B65BAFA
P 2000 3600
F 0 "#PWR0215" H 2000 3350 50  0001 C CNN
F 1 "GNDISO" H 2005 3427 50  0000 C CNN
F 2 "" H 2000 3600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 2000 3600 50  0001 C CNN
	1    2000 3600
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDISO #PWR0219
U 1 1 5B65BF43
P 4050 3950
F 0 "#PWR0219" H 4050 3700 50  0001 C CNN
F 1 "GNDISO" H 4055 3777 50  0000 C CNN
F 2 "" H 4050 3950 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4050 3950 50  0001 C CNN
	1    4050 3950
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDISO #PWR0209
U 1 1 5B65C2B1
P 3050 2650
F 0 "#PWR0209" H 3050 2400 50  0001 C CNN
F 1 "GNDISO" H 3055 2477 50  0000 C CNN
F 2 "" H 3050 2650 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3050 2650 50  0001 C CNN
	1    3050 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2750 3400 2750
Wire Wire Line
	3050 2250 3050 2300
Wire Wire Line
	2400 3250 2400 3150
Wire Wire Line
	2000 3250 2000 3050
Wire Wire Line
	4050 3850 4050 3900
Wire Wire Line
	4050 3900 4250 3900
Wire Wire Line
	4250 3900 4250 3850
Wire Wire Line
	4050 3950 4050 3900
Connection ~ 4050 3900
$Comp
L eload:LED D201
U 1 1 5B66306F
P 5750 3100
F 0 "D201" V 5788 2983 50  0000 R CNN
F 1 "YELLOW" V 5697 2983 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 5750 3100 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120YS75000.pdf" H 5750 3100 50  0001 C CNN
F 4 "20mA" H -300 -1800 50  0001 C CNN "Current"
F 5 "732-4994-1-ND" H -300 -1800 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -300 -1800 50  0001 C CNN "Manufacturer"
F 7 "150120YS75000" H -300 -1800 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H -300 -1800 50  0001 C CNN "Voltage"
F 9 "~" H 0   0   50  0001 C CNN "Stuff"
F 10 "710-156120YS75000" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    5750 3100
	0    -1   -1   0   
$EndComp
$Comp
L eload:LED D202
U 1 1 5B66326B
P 6300 3100
F 0 "D202" V 6338 2983 50  0000 R CNN
F 1 "YELLOW" V 6247 2983 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 6300 3100 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120YS75000.pdf" H 6300 3100 50  0001 C CNN
F 4 "20mA" H -300 -1800 50  0001 C CNN "Current"
F 5 "732-4994-1-ND" H -300 -1800 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -300 -1800 50  0001 C CNN "Manufacturer"
F 7 "150120YS75000" H -300 -1800 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H -300 -1800 50  0001 C CNN "Voltage"
F 9 "~" H 0   0   50  0001 C CNN "Stuff"
F 10 "710-156120YS75000" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    6300 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4850 3350 5750 3350
Wire Wire Line
	5750 3350 5750 3250
Wire Wire Line
	4850 3450 6300 3450
Wire Wire Line
	6300 3450 6300 3250
Wire Wire Line
	6300 2950 6300 2850
Wire Wire Line
	5750 2950 5750 2850
$Comp
L eload:C C201
U 1 1 5B669069
P 4500 2150
F 0 "C201" H 4615 2196 50  0000 L CNN
F 1 "0.1uF" H 4615 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4538 2000 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 4500 2150 50  0001 C CNN
F 4 "399-1177-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 0   0   50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 0   0   50  0001 C CNN "Tolerance"
F 10 "50V" H 0   0   50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    4500 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1950 4500 1950
Wire Wire Line
	4950 1950 4950 2000
Wire Wire Line
	4500 2000 4500 1950
Connection ~ 4500 1950
Wire Wire Line
	4500 1950 4700 1950
$Comp
L eload:GNDISO #PWR0205
U 1 1 5B66AD9B
P 4500 2350
F 0 "#PWR0205" H 4500 2100 50  0001 C CNN
F 1 "GNDISO" H 4505 2177 50  0000 C CNN
F 2 "" H 4500 2350 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4500 2350 50  0001 C CNN
	1    4500 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1950 4250 2450
Wire Wire Line
	4500 2300 4500 2350
Wire Wire Line
	4950 2300 4950 2350
Wire Wire Line
	4950 2350 4500 2350
Wire Wire Line
	2000 3550 2400 3550
Wire Wire Line
	2000 3600 2000 3550
Connection ~ 2000 3550
$Comp
L eload:Ferrite_Bead_Small L201
U 1 1 5B67179B
P 1700 2850
F 0 "L201" V 1463 2850 50  0000 C CNN
F 1 "600R @100MHZ" V 1554 2850 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" V 1630 2850 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/bead/CIC21J601NE.jsp" H 1700 2850 50  0001 C CNN
F 4 "1A" H -1450 -1800 50  0001 C CNN "Current"
F 5 "1276-6371-1-ND" H -1450 -1800 50  0001 C CNN "Digi-Key Part Number"
F 6 "Samsung Electro-Mechanics" H -1450 -1800 50  0001 C CNN "Manufacturer"
F 7 "CIC21J601NE" H -1450 -1800 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 125°C" H -1450 -1800 50  0001 C CNN "Operating Temperature"
F 9 "~" H 0   0   50  0001 C CNN "Stuff"
F 10 "875-MI0805K601R-10" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    1700 2850
	0    1    1    0   
$EndComp
$Comp
L eload:C C207
U 1 1 5B6723A4
P 3050 2450
F 0 "C207" H 3165 2496 50  0000 L CNN
F 1 "0.1uF" H 3165 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3088 2300 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 3050 2450 50  0001 C CNN
F 4 "399-1177-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 0   0   50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 0   0   50  0001 C CNN "Tolerance"
F 10 "50V" H 0   0   50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    3050 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2250 3400 2750
Wire Wire Line
	3400 2250 3650 2250
Wire Wire Line
	4050 2250 4050 2450
Connection ~ 3400 2250
Text Label 3100 3350 0    50   ~ 0
3V3_FTDI
Wire Wire Line
	3450 3350 3100 3350
Wire Wire Line
	3050 2650 3050 2600
$Comp
L eload:C C210
U 1 1 5B67AA6A
P 1500 3500
F 0 "C210" H 1615 3546 50  0000 L CNN
F 1 "10nF" H 1615 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1538 3350 50  0001 C CNN
F 3 "~" H 1500 3500 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "77-VJ0805Y103MXXCBC" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    1500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3350 1500 2850
Wire Wire Line
	3050 2250 3400 2250
$Comp
L eload:GNDISO #PWR0217
U 1 1 5B67D5E5
P 1500 3700
F 0 "#PWR0217" H 1500 3450 50  0001 C CNN
F 1 "GNDISO" H 1505 3527 50  0000 C CNN
F 2 "" H 1500 3700 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1500 3700 50  0001 C CNN
	1    1500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 3650 1500 3700
$Comp
L eload:GNDISO #PWR0216
U 1 1 5B68417A
P 750 3650
F 0 "#PWR0216" H 750 3400 50  0001 C CNN
F 1 "GNDISO" H 755 3477 50  0000 C CNN
F 2 "" H 750 3650 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 750 3650 50  0001 C CNN
	1    750  3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2450 5750 2550
Wire Wire Line
	6300 2450 6300 2550
$Comp
L eload:+3V3_FTDI #PWR0204
U 1 1 5B6EF379
P 3650 2250
F 0 "#PWR0204" H 3650 2100 50  0001 C CNN
F 1 "+3V3_FTDI" H 3655 2423 50  0000 C CNN
F 2 "" H 3650 2250 50  0001 C CNN
F 3 "" H 3650 2250 50  0001 C CNN
	1    3650 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2850 2700 2800
Wire Wire Line
	4700 1900 4700 1950
Connection ~ 4700 1950
Wire Wire Line
	4700 1950 4950 1950
Connection ~ 3650 2250
Wire Wire Line
	3650 2250 4050 2250
Text Label 5000 2750 0    50   ~ 0
FTDI_TXD_ISO
Text Label 5000 2850 0    50   ~ 0
FTDI_RXD_ISO
Wire Wire Line
	4850 2750 5000 2750
Wire Wire Line
	5000 2850 4850 2850
Connection ~ 2000 3050
Wire Wire Line
	2000 3050 2600 3050
Connection ~ 2400 3150
Wire Wire Line
	2400 3150 2600 3150
Wire Wire Line
	1150 3050 2000 3050
Wire Wire Line
	1150 3150 2400 3150
Connection ~ 1500 2850
Wire Wire Line
	1500 2850 1600 2850
Wire Wire Line
	1150 2850 1500 2850
Wire Wire Line
	850  3450 850  3500
Wire Wire Line
	850  3500 750  3500
Wire Wire Line
	750  3500 750  3450
Connection ~ 750  3500
Wire Notes Line width 50
	8750 6750 8750 3450
Wire Notes Line width 50
	8750 2450 8750 750 
Wire Notes Line width 50
	8750 750  500  750 
Text Notes 1700 5850 0    750  ~ 0
ISOLATED
Text HLabel 9450 2950 2    50   Input ~ 0
FTDI_MCU_RXD
Text HLabel 9450 3150 2    50   Output ~ 0
FTDI_MCU_TXD
Text Label 3100 2250 0    50   ~ 0
3V3_FTDI
$Comp
L eload:R_US R?
U 1 1 5B864A19
P 5750 2700
AR Path="/5B6010A5/5B864A19" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B864A19" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B864A19" Ref="R201"  Part="1" 
F 0 "R201" H 5682 2609 50  0000 R CNN
F 1 "1k" H 5682 2700 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5790 2690 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 5750 2700 50  0001 C CNN
F 4 "Yageo" V 5750 2700 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 5750 2700 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 5750 2700 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 5682 2791 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 5750 2700 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 5750 2700 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 5750 2700 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    5750 2700
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B8680C6
P 6300 2700
AR Path="/5B6010A5/5B8680C6" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B8680C6" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B8680C6" Ref="R202"  Part="1" 
F 0 "R202" H 6232 2609 50  0000 R CNN
F 1 "1k" H 6232 2700 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6340 2690 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 6300 2700 50  0001 C CNN
F 4 "Yageo" V 6300 2700 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 6300 2700 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 6300 2700 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 6232 2791 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 6300 2700 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 6300 2700 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 6300 2700 50  0001 C CNN "Operating Temperature"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    6300 2700
	-1   0    0    1   
$EndComp
$Comp
L eload:LED D203
U 1 1 5B91A15A
P 7000 4300
F 0 "D203" V 7038 4182 50  0000 R CNN
F 1 "GREEN" V 6947 4182 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 7000 4300 50  0001 C CNN
F 3 "http://katalog.we-online.de/led/datasheet/150120VS75000.pdf" H 7000 4300 50  0001 C CNN
F 4 "20mA" H 1500 2300 50  0001 C CNN "Current"
F 5 "732-4993-1-ND" H 1500 2300 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H 1500 2300 50  0001 C CNN "Manufacturer"
F 7 "150120VS75000" H 1500 2300 50  0001 C CNN "Manufacturer Part Number"
F 8 "2V" H 1500 2300 50  0001 C CNN "Voltage"
F 9 "~" H -100 300 50  0001 C CNN "Stuff"
F 10 "710-156120VS75000" H -100 300 50  0001 C CNN "Mouser Part Number"
	1    7000 4300
	0    -1   -1   0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B91A168
P 7000 3950
AR Path="/5B6010A5/5B91A168" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B91A168" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5B91A168" Ref="R205"  Part="1" 
F 0 "R205" H 6932 3859 50  0000 R CNN
F 1 "1k" H 6932 3950 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7040 3940 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 7000 3950 50  0001 C CNN
F 4 "Yageo" V 7000 3950 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 7000 3950 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 7000 3950 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 6932 4041 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7000 3950 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7000 3950 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7000 3950 50  0001 C CNN "Operating Temperature"
F 11 "~" H -100 300 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -100 300 50  0001 C CNN "Mouser Part Number"
	1    7000 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	7000 4500 7000 4450
Wire Wire Line
	7000 4150 7000 4100
Wire Wire Line
	7000 3700 7000 3800
$Comp
L eload:VBUS_FTDI #PWR0210
U 1 1 5B747E37
P 2700 2800
F 0 "#PWR0210" H 2700 2650 50  0001 C CNN
F 1 "VBUS_FTDI" H 2705 2973 50  0000 C CNN
F 2 "" H 2700 2800 50  0001 C CNN
F 3 "" H 2700 2800 50  0001 C CNN
	1    2700 2800
	1    0    0    -1  
$EndComp
$Comp
L eload:VBUS_FTDI #PWR0201
U 1 1 5B74828D
P 4700 1900
F 0 "#PWR0201" H 4700 1750 50  0001 C CNN
F 1 "VBUS_FTDI" H 4705 2073 50  0000 C CNN
F 2 "" H 4700 1900 50  0001 C CNN
F 3 "" H 4700 1900 50  0001 C CNN
	1    4700 1900
	1    0    0    -1  
$EndComp
$Comp
L eload:VBUS_FTDI #PWR0218
U 1 1 5B74D389
P 7000 3700
F 0 "#PWR0218" H 7000 3550 50  0001 C CNN
F 1 "VBUS_FTDI" H 7005 3873 50  0000 C CNN
F 2 "" H 7000 3700 50  0001 C CNN
F 3 "" H 7000 3700 50  0001 C CNN
	1    7000 3700
	1    0    0    -1  
$EndComp
NoConn ~ 4850 2950
NoConn ~ 4850 3050
NoConn ~ 4850 3550
NoConn ~ 4850 3250
Wire Wire Line
	1800 2850 2200 2850
$Comp
L eload:PWR_FLAG #FLG0201
U 1 1 5BBE23B7
P 2200 2850
F 0 "#FLG0201" H 2200 2925 50  0001 C CNN
F 1 "PWR_FLAG" H 2200 3024 50  0000 C CNN
F 2 "" H 2200 2850 50  0001 C CNN
F 3 "~" H 2200 2850 50  0001 C CNN
	1    2200 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 2850 2700 2850
Connection ~ 2200 2850
Text Label 1550 3050 0    50   ~ 0
USB_ISO_D+
Text Label 1550 3150 0    50   ~ 0
USB_ISO_D-
Text Label 3100 3050 0    50   ~ 0
FTDI_D+
Text Label 3100 3150 0    50   ~ 0
FTDI_D-
Text Label 1200 2850 0    50   ~ 0
VBUS_ISO_L
Text Label 5150 3350 0    50   ~ 0
FTDI_LED_TX
Text Label 5150 3450 0    50   ~ 0
FTDI_LED_RX
Connection ~ 4500 2350
$Comp
L eload:ADuM1201AR U201
U 1 1 5B7AE64E
P 8750 3050
F 0 "U201" H 8750 3517 50  0000 C CNN
F 1 "ADuM1201AR" H 8750 3426 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8750 2650 50  0001 C CIN
F 3 "http://www.analog.com/media/en/technical-documentation/data-sheets/ADuM1200_1201.pdf" H 8750 2950 50  0001 C CNN
F 4 "336-4351-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "634-SI8422AB-D-IS " H 0   0   50  0001 C CNN "Mouser Part Number"
	1    8750 3050
	1    0    0    -1  
$EndComp
$Comp
L eload:C C205
U 1 1 5B7AE853
P 7300 2400
F 0 "C205" H 7415 2446 50  0000 L CNN
F 1 "0.1uF" H 7415 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7338 2250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 7300 2400 50  0001 C CNN
F 4 "399-1177-1-ND" H 2800 250 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 2800 250 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 2800 250 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 2800 250 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 2800 250 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 2800 250 50  0001 C CNN "Tolerance"
F 10 "50V" H 2800 250 50  0001 C CNN "Voltage"
F 11 "~" H 2800 250 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 2800 250 50  0001 C CNN "Mouser Part Number"
	1    7300 2400
	1    0    0    -1  
$EndComp
$Comp
L eload:C C206
U 1 1 5B7AE981
P 7750 2400
F 0 "C206" H 7865 2446 50  0000 L CNN
F 1 "0.1uF" H 7865 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7788 2250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 7750 2400 50  0001 C CNN
F 4 "399-1177-1-ND" H 3250 250 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 3250 250 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 3250 250 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 3250 250 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 3250 250 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 3250 250 50  0001 C CNN "Tolerance"
F 10 "50V" H 3250 250 50  0001 C CNN "Voltage"
F 11 "~" H 3250 250 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 3250 250 50  0001 C CNN "Mouser Part Number"
	1    7750 2400
	1    0    0    -1  
$EndComp
$Comp
L eload:C C203
U 1 1 5B7AE9EF
P 9500 2350
F 0 "C203" H 9615 2396 50  0000 L CNN
F 1 "0.1uF" H 9615 2305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9538 2200 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 9500 2350 50  0001 C CNN
F 4 "399-1177-1-ND" H 5000 200 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 5000 200 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 5000 200 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 5000 200 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 5000 200 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 5000 200 50  0001 C CNN "Tolerance"
F 10 "50V" H 5000 200 50  0001 C CNN "Voltage"
F 11 "~" H 5000 200 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 5000 200 50  0001 C CNN "Mouser Part Number"
	1    9500 2350
	1    0    0    -1  
$EndComp
$Comp
L eload:C C204
U 1 1 5B7AEA7F
P 9950 2350
F 0 "C204" H 10065 2396 50  0000 L CNN
F 1 "0.1uF" H 10065 2305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9988 2200 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 9950 2350 50  0001 C CNN
F 4 "399-1177-1-ND" H 5450 200 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 5450 200 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 5450 200 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 5450 200 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 5450 200 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 5450 200 50  0001 C CNN "Tolerance"
F 10 "50V" H 5450 200 50  0001 C CNN "Voltage"
F 11 "~" H 5450 200 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 5450 200 50  0001 C CNN "Mouser Part Number"
	1    9950 2350
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDISO #PWR0220
U 1 1 5B7AF0D4
P 7000 4500
F 0 "#PWR0220" H 7000 4250 50  0001 C CNN
F 1 "GNDISO" H 7005 4327 50  0000 C CNN
F 2 "" H 7000 4500 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 7000 4500 50  0001 C CNN
	1    7000 4500
	1    0    0    -1  
$EndComp
Text Label 7650 3150 0    50   ~ 0
FTDI_TXD_ISO
Wire Wire Line
	7650 3150 8250 3150
Text Label 7650 2950 0    50   ~ 0
FTDI_RXD_ISO
Wire Wire Line
	7650 2950 8250 2950
$Comp
L eload:GNDD #PWR0214
U 1 1 5B7C158B
P 9350 3300
F 0 "#PWR0214" H 9350 3050 50  0001 C CNN
F 1 "GNDD" H 9355 3127 50  0000 C CNN
F 2 "" H 9350 3300 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 9350 3300 50  0001 C CNN
	1    9350 3300
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDISO #PWR0213
U 1 1 5B7C1AA1
P 8150 3300
F 0 "#PWR0213" H 8150 3050 50  0001 C CNN
F 1 "GNDISO" H 8155 3127 50  0000 C CNN
F 2 "" H 8150 3300 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 8150 3300 50  0001 C CNN
	1    8150 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3250 8150 3250
Wire Wire Line
	8150 3250 8150 3300
Wire Wire Line
	9250 3250 9350 3250
Wire Wire Line
	9350 3250 9350 3300
Wire Wire Line
	9450 3150 9250 3150
Wire Wire Line
	9450 2950 9250 2950
$Comp
L eload:+3V3_FTDI #PWR0211
U 1 1 5B7CD540
P 8150 2800
F 0 "#PWR0211" H 8150 2650 50  0001 C CNN
F 1 "+3V3_FTDI" H 8155 2973 50  0000 C CNN
F 2 "" H 8150 2800 50  0001 C CNN
F 3 "" H 8150 2800 50  0001 C CNN
	1    8150 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2800 8150 2850
Wire Wire Line
	8150 2850 8250 2850
$Comp
L eload:+3V3 #PWR0212
U 1 1 5B7D045C
P 9350 2800
F 0 "#PWR0212" H 9350 2650 50  0001 C CNN
F 1 "+3V3" H 9355 2973 50  0000 C CNN
F 2 "" H 9350 2800 50  0001 C CNN
F 3 "" H 9350 2800 50  0001 C CNN
	1    9350 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 2850 9350 2850
Wire Wire Line
	9350 2850 9350 2800
$Comp
L eload:+3V3_FTDI #PWR0203
U 1 1 5B7D8DDC
P 7200 2200
F 0 "#PWR0203" H 7200 2050 50  0001 C CNN
F 1 "+3V3_FTDI" H 7205 2373 50  0000 C CNN
F 2 "" H 7200 2200 50  0001 C CNN
F 3 "" H 7200 2200 50  0001 C CNN
	1    7200 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2200 7300 2200
Wire Wire Line
	7750 2200 7750 2250
Wire Wire Line
	7300 2250 7300 2200
Connection ~ 7300 2200
Wire Wire Line
	7300 2200 7750 2200
$Comp
L eload:GNDISO #PWR0208
U 1 1 5B7DE7EA
P 7200 2600
F 0 "#PWR0208" H 7200 2350 50  0001 C CNN
F 1 "GNDISO" H 7205 2427 50  0000 C CNN
F 2 "" H 7200 2600 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 7200 2600 50  0001 C CNN
	1    7200 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2600 7300 2600
Wire Wire Line
	7750 2600 7750 2550
Wire Wire Line
	7300 2600 7300 2550
Connection ~ 7300 2600
Wire Wire Line
	7300 2600 7750 2600
$Comp
L eload:+3V3 #PWR0202
U 1 1 5B7E4935
P 10050 2150
F 0 "#PWR0202" H 10050 2000 50  0001 C CNN
F 1 "+3V3" H 10055 2323 50  0000 C CNN
F 2 "" H 10050 2150 50  0001 C CNN
F 3 "" H 10050 2150 50  0001 C CNN
	1    10050 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2150 9950 2150
Wire Wire Line
	9500 2150 9500 2200
Wire Wire Line
	9950 2200 9950 2150
Connection ~ 9950 2150
Wire Wire Line
	9950 2150 9500 2150
$Comp
L eload:GNDD #PWR0207
U 1 1 5B7EAE8C
P 10050 2550
F 0 "#PWR0207" H 10050 2300 50  0001 C CNN
F 1 "GNDD" H 10055 2377 50  0000 C CNN
F 2 "" H 10050 2550 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 10050 2550 50  0001 C CNN
	1    10050 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 2550 9950 2550
Wire Wire Line
	9500 2550 9500 2500
Wire Wire Line
	9950 2550 9950 2500
Connection ~ 9950 2550
Wire Wire Line
	9950 2550 9500 2550
Text Notes 8900 3800 0    50   ~ 0
Can use subsitute\nSI8422AB
$Comp
L eload:CP C?
U 1 1 5B81DA4C
P 4950 2150
AR Path="/5B5FBC06/5B81DA4C" Ref="C?"  Part="1" 
AR Path="/5B6C0220/5B81DA4C" Ref="C?"  Part="1" 
AR Path="/5B5FBC03/5B81DA4C" Ref="C202"  Part="1" 
F 0 "C202" H 5068 2196 50  0000 L CNN
F 1 "10uF" H 5068 2105 50  0000 L CNN
F 2 "Capacitor_Tantalum_SMD:CP_EIA-3216-12_Kemet-S" H 4988 2000 50  0001 C CNN
F 3 "~" H 4950 2150 50  0001 C CNN
	1    4950 2150
	1    0    0    -1  
$EndComp
$Comp
L eload:+3V3_FTDI #PWR0112
U 1 1 5B824BFD
P 6050 2350
F 0 "#PWR0112" H 6050 2200 50  0001 C CNN
F 1 "+3V3_FTDI" H 6055 2523 50  0000 C CNN
F 2 "" H 6050 2350 50  0001 C CNN
F 3 "" H 6050 2350 50  0001 C CNN
	1    6050 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2450 6300 2450
Wire Wire Line
	5750 2450 6050 2450
Connection ~ 6050 2450
Wire Wire Line
	6050 2350 6050 2450
NoConn ~ 1150 3250
$Comp
L eload:PWR_FLAG #FLG0202
U 1 1 5BBDD8C7
P 700 3550
F 0 "#FLG0202" H 700 3625 50  0001 C CNN
F 1 "PWR_FLAG" H 700 3723 50  0001 C CNN
F 2 "" H 700 3550 50  0001 C CNN
F 3 "~" H 700 3550 50  0001 C CNN
	1    700  3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	750  3550 750  3650
Wire Wire Line
	750  3500 750  3550
Connection ~ 750  3550
Wire Wire Line
	700  3550 750  3550
$EndSCHEMATC

EESchema Schematic File Version 4
LIBS:eLoad-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 6
Title ""
Date "2018-08-10"
Rev "0.1"
Comp "Peter Yin"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eload:Q_NMOS_GDS Q?
U 1 1 5B60D4B3
P 8700 3900
AR Path="/5B60D4B3" Ref="Q?"  Part="1" 
AR Path="/5B6010A5/5B60D4B3" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5B60D4B3" Ref="Q404"  Part="1" 
F 0 "Q404" H 8905 3946 50  0000 L CNN
F 1 "IRL530NPBF" H 8905 3855 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8900 4000 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irl530npbf.pdf?fileId=5546d462533600a40153565fad5c2560" H 8700 3900 50  0001 C CNN
F 4 "Infineon Technologies" H 8700 3900 50  0001 C CNN "Manufacturer"
F 5 "IRL530NPBF" H 8700 3900 50  0001 C CNN "Manufacturer Part Number"
F 6 "17A" H -800 600 50  0001 C CNN "Current"
F 7 "IRL530NPBF-ND" H -800 600 50  0001 C CNN "Digi-Key Part Number"
F 8 "-55°C ~ 175°C (TJ)" H -800 600 50  0001 C CNN "Operating Temperature"
F 9 " 79W (Tc)" H -800 600 50  0001 C CNN "Power"
F 10 "100V" H -800 600 50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "942-IRL530NPBF" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    8700 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 5400 3150 5050
$Comp
L eload:C C?
U 1 1 5B60D4C9
P 4650 4050
AR Path="/5B60D4C9" Ref="C?"  Part="1" 
AR Path="/5B6010A5/5B60D4C9" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B60D4C9" Ref="C408"  Part="1" 
F 0 "C408" V 4700 4250 50  0000 C CNN
F 1 "1nF" V 4700 3900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4688 3900 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL21B102KBCNNNC.jsp" H 4650 4050 50  0001 C CNN
F 4 "1276-1105-1-ND" H -2400 250 50  0001 C CNN "Digi-Key Part Number"
F 5 "Samsung Electro-Mechanics" H -2400 250 50  0001 C CNN "Manufacturer"
F 6 "CL21C330JBANNNC" H -2400 250 50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 125°C" H -2400 250 50  0001 C CNN "Operating Temperature"
F 8 " C0G, NP0 " H -2400 250 50  0001 C CNN "Temperature Coefficient"
F 9 "±5%" H -2400 250 50  0001 C CNN "Tolerance"
F 10 "50V" H -2400 250 50  0001 C CNN "Voltage"
F 11 "~" H -2250 0   50  0001 C CNN "Stuff"
F 12 "710-885012007013" H -2250 0   50  0001 C CNN "Mouser Part Number"
	1    4650 4050
	0    -1   -1   0   
$EndComp
$Comp
L eload:BindingPost J?
U 1 1 5B60D545
P 9750 1900
AR Path="/5B6010A5/5B60D545" Ref="J?"  Part="1" 
AR Path="/5B5FBD8A/5B60D545" Ref="J401"  Part="1" 
F 0 "J401" H 9750 1600 50  0000 R CNN
F 1 "PRT-09739" H 9750 1700 50  0000 R CNN
F 2 "eload_fp:MountingHole_3.2mm_M3_Pad" H 9750 1900 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Sparkfun%20PDFs/PRT-09739_Web.pdf" H 9750 1900 50  0001 C CNN
F 4 "SparkFun Electronics" H 9750 1900 50  0001 C CNN "Manufacturer"
F 5 "PRT-09739" H 9750 1900 50  0001 C CNN "Manufacturer Part Number"
F 6 "1568-1664-ND" H 9750 1900 50  0001 C CNN "Digi-Key Part Number"
F 7 "~" H -50 1050 50  0001 C CNN "Stuff"
	1    9750 1900
	-1   0    0    1   
$EndComp
$Comp
L eload:C C?
U 1 1 5B60D638
P 4100 4350
AR Path="/5B6010A5/5B60D638" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B60D638" Ref="C409"  Part="1" 
F 0 "C409" V 4150 4500 50  0000 C CNN
F 1 "10pF" V 4150 4200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4138 4200 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 4100 4350 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 4100 4350 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 4100 4350 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 4100 4350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 4100 4350 50  0001 C CNN "Tolerance"
F 8 "100V" V 4100 4350 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 4100 4350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 4100 4350 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2450 0   50  0001 C CNN "Stuff"
F 12 "710-885012007051" H -2450 0   50  0001 C CNN "Mouser Part Number"
	1    4100 4350
	0    -1   -1   0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B60D662
P 8300 3700
AR Path="/5B6010A5/5B60D662" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B60D662" Ref="R409"  Part="1" 
F 0 "R409" H 8232 3654 50  0000 R CNN
F 1 "100R" H 8232 3745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8340 3690 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 8300 3700 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 8300 3700 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 8300 3700 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 8300 3700 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 8300 3700 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 8300 3700 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 8300 3700 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 8300 3700 50  0001 C CNN "Operating Temperature"
F 11 "~" H -50 -50 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -50 -50 50  0001 C CNN "Mouser Part Number"
	1    8300 3700
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B60D670
P 3550 4050
AR Path="/5B6010A5/5B60D670" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B60D670" Ref="R411"  Part="1" 
F 0 "R411" V 3500 3750 50  0000 L CNN
F 1 "1k" V 3500 4150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3590 4040 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 3550 4050 50  0001 C CNN
F 4 "Yageo" V 3550 4050 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 3550 4050 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 3550 4050 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 3600 4150 50  0000 L CNN "Tolerance"
F 8 "0.125W, 1/8W " V 3550 4050 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 3550 4050 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 3550 4050 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2650 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    3550 4050
	0    1    1    0   
$EndComp
$Comp
L eload:TestPoint TP?
U 1 1 5B60D685
P 5850 3250
AR Path="/5B6010A5/5B60D685" Ref="TP?"  Part="1" 
AR Path="/5B5FBD8A/5B60D685" Ref="TP401"  Part="1" 
F 0 "TP401" H 5750 3500 50  0000 L CNN
F 1 "TestPoint" H 5908 3279 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 6050 3250 50  0001 C CNN
F 3 "~" H 6050 3250 50  0001 C CNN
F 4 "~" H -1900 0   50  0001 C CNN "Stuff"
	1    5850 3250
	1    0    0    -1  
$EndComp
$Comp
L eload:TestPoint TP?
U 1 1 5B60D68C
P 2850 5350
AR Path="/5B6010A5/5B60D68C" Ref="TP?"  Part="1" 
AR Path="/5B5FBD8A/5B60D68C" Ref="TP403"  Part="1" 
F 0 "TP403" H 2900 5500 50  0000 L CNN
F 1 "TestPoint" H 2908 5379 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3050 5350 50  0001 C CNN
F 3 "~" H 3050 5350 50  0001 C CNN
F 4 "~" H -2650 0   50  0001 C CNN "Stuff"
	1    2850 5350
	1    0    0    -1  
$EndComp
$Comp
L eload:TestPoint TP?
U 1 1 5B60D693
P 3050 3450
AR Path="/5B6010A5/5B60D693" Ref="TP?"  Part="1" 
AR Path="/5B5FBD8A/5B60D693" Ref="TP402"  Part="1" 
F 0 "TP402" V 3050 3638 50  0000 L CNN
F 1 "TestPoint" H 3108 3479 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3250 3450 50  0001 C CNN
F 3 "~" H 3250 3450 50  0001 C CNN
F 4 "~" H -2650 0   50  0001 C CNN "Stuff"
	1    3050 3450
	0    -1   -1   0   
$EndComp
$Comp
L eload:BindingPost J?
U 1 1 5B60D6CE
P 10000 5700
AR Path="/5B6010A5/5B60D6CE" Ref="J?"  Part="1" 
AR Path="/5B5FBD8A/5B60D6CE" Ref="J403"  Part="1" 
F 0 "J403" H 10000 5400 50  0000 R CNN
F 1 "PRT-09740" H 10000 5500 50  0000 R CNN
F 2 "eload_fp:MountingHole_3.2mm_M3_Pad" H 10000 5700 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Sparkfun%20PDFs/PRT-09740_Web.pdf" H 10000 5700 50  0001 C CNN
F 4 "SparkFun Electronics" H 10000 5700 50  0001 C CNN "Manufacturer"
F 5 "PRT-09740" H 10000 5700 50  0001 C CNN "Manufacturer Part Number"
F 6 "1568-1665-ND" H 10000 5700 50  0001 C CNN "Digi-Key Part Number"
F 7 "~" H 0   0   50  0001 C CNN "Stuff"
	1    10000 5700
	-1   0    0    1   
$EndComp
Connection ~ 3150 5400
Wire Wire Line
	4400 5300 4800 5300
Wire Wire Line
	4250 6100 4700 6100
$Comp
L eload:C C?
U 1 1 5B60D785
P 4100 6350
AR Path="/5B6010A5/5B60D785" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B60D785" Ref="C411"  Part="1" 
F 0 "C411" V 4150 6550 50  0000 C CNN
F 1 "10pF" V 4150 6150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4138 6200 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 4100 6350 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 4100 6350 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 4100 6350 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 4100 6350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 4100 6350 50  0001 C CNN "Tolerance"
F 8 "100V" V 4100 6350 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 4100 6350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 4100 6350 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2650 0   50  0001 C CNN "Stuff"
F 12 "710-885012007051" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    4100 6350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 6100 4700 6350
Connection ~ 4700 6100
Wire Wire Line
	3500 6350 3500 6100
Wire Wire Line
	3500 6100 3950 6100
Connection ~ 3500 6100
$Comp
L eload:R_US R?
U 1 1 5B60D80E
P 3150 4900
AR Path="/5B6010A5/5B60D80E" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B60D80E" Ref="R416"  Part="1" 
F 0 "R416" H 3082 4854 50  0000 R CNN
F 1 "100R" H 3082 4945 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3190 4890 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 3150 4900 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 3150 4900 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 3150 4900 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 3150 4900 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 3150 4900 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 3150 4900 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 3150 4900 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 3150 4900 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2650 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    3150 4900
	-1   0    0    1   
$EndComp
$Comp
L eload:Q_NMOS_GDS Q?
U 1 1 5B60D81E
P 9850 3900
AR Path="/5B60D81E" Ref="Q?"  Part="1" 
AR Path="/5B6010A5/5B60D81E" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5B60D81E" Ref="Q405"  Part="1" 
F 0 "Q405" H 10055 3946 50  0000 L CNN
F 1 "IRL530NPBF" H 10055 3855 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 10050 4000 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irl530npbf.pdf?fileId=5546d462533600a40153565fad5c2560" H 9850 3900 50  0001 C CNN
F 4 "Infineon Technologies" H 9850 3900 50  0001 C CNN "Manufacturer"
F 5 "IRL530NPBF" H 9850 3900 50  0001 C CNN "Manufacturer Part Number"
F 6 "17A" H 350 600 50  0001 C CNN "Current"
F 7 "IRL530NPBF-ND" H 350 600 50  0001 C CNN "Digi-Key Part Number"
F 8 "-55°C ~ 175°C (TJ)" H 350 600 50  0001 C CNN "Operating Temperature"
F 9 " 79W (Tc)" H 350 600 50  0001 C CNN "Power"
F 10 "100V" H 350 600 50  0001 C CNN "Voltage"
F 11 "~" H 0   0   50  0001 C CNN "Stuff"
F 12 "942-IRL530NPBF" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    9850 3900
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B60D848
P 9450 3700
AR Path="/5B6010A5/5B60D848" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B60D848" Ref="R410"  Part="1" 
F 0 "R410" H 9518 3746 50  0000 L CNN
F 1 "100R" H 9518 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9490 3690 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 9450 3700 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 9450 3700 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 9450 3700 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 9450 3700 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 9450 3700 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 9450 3700 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 9450 3700 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 9450 3700 50  0001 C CNN "Operating Temperature"
F 11 "~" H -50 -50 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -50 -50 50  0001 C CNN "Mouser Part Number"
	1    9450 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4800 8800 4900
Wire Wire Line
	9950 4900 9950 4800
Wire Wire Line
	9950 3700 9950 3050
Wire Wire Line
	8800 3050 8800 3700
Wire Wire Line
	3500 6350 3950 6350
Wire Wire Line
	4250 6350 4700 6350
Wire Wire Line
	8450 3900 8450 3950
Wire Wire Line
	8450 3900 8500 3900
Wire Wire Line
	9600 3900 9600 3950
Connection ~ 9600 3900
Wire Wire Line
	9600 3900 9650 3900
$Comp
L eload:D_TVS D?
U 1 1 5B60D893
P 8450 4100
AR Path="/5B6010A5/5B60D893" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5B60D893" Ref="D404"  Part="1" 
F 0 "D404" V 8359 4022 50  0000 R CNN
F 1 "824501500" V 8450 4022 50  0000 R CNN
F 2 "Diode_SMD:D_SMA" H 8450 4100 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/824501500.pdf" H 8450 4100 50  0001 C CNN
F 4 "43.5A " H 100 -300 50  0001 C CNN "Current"
F 5 "732-9939-1-ND" H 100 -300 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H 100 -300 50  0001 C CNN "Manufacturer"
F 7 "824501500" H 100 -300 50  0001 C CNN "Manufacturer Part Number"
F 8 "-65°C ~ 150°C" H 100 -300 50  0001 C CNN "Operating Temperature"
F 9 "400W " H 100 -300 50  0001 C CNN "Power"
F 10 "5V" V 8541 4022 50  0000 R CNN "Voltage"
F 11 "~" H 0   -50 50  0001 C CNN "Stuff"
F 12 "710-824501500 " H 0   -50 50  0001 C CNN "Mouser Part Number"
	1    8450 4100
	0    1    1    0   
$EndComp
$Comp
L eload:D_TVS D?
U 1 1 5B60D8A1
P 9600 4100
AR Path="/5B6010A5/5B60D8A1" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5B60D8A1" Ref="D405"  Part="1" 
F 0 "D405" V 9509 4022 50  0000 R CNN
F 1 "824501500" V 9600 4022 50  0000 R CNN
F 2 "Diode_SMD:D_SMA" H 9600 4100 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/824501500.pdf" H 9600 4100 50  0001 C CNN
F 4 "43.5A " H 100 -300 50  0001 C CNN "Current"
F 5 "732-9939-1-ND" H 100 -300 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H 100 -300 50  0001 C CNN "Manufacturer"
F 7 "824501500" H 100 -300 50  0001 C CNN "Manufacturer Part Number"
F 8 "-65°C ~ 150°C" H 100 -300 50  0001 C CNN "Operating Temperature"
F 9 "400W " H 100 -300 50  0001 C CNN "Power"
F 10 "5V" V 9691 4022 50  0000 R CNN "Voltage"
F 11 "~" H 0   -50 50  0001 C CNN "Stuff"
F 12 "710-824501500 " H 0   -50 50  0001 C CNN "Mouser Part Number"
	1    9600 4100
	0    1    1    0   
$EndComp
Text Notes 9900 1950 0    39   ~ 0
RED
Text Notes 10100 5750 0    39   ~ 0
BLACK
Wire Wire Line
	2600 5400 2850 5400
Text Notes 4850 4650 0    100  ~ 0
Load Circuit
$Comp
L eload:MCP6001T-I_OT U402
U 1 1 5B734F0A
P 4100 3300
F 0 "U402" H 4150 3600 50  0000 L CNN
F 1 "MCP6001T-I_OT" H 4150 3500 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4000 3100 50  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en011705" H 4100 3500 50  0001 C CNN
F 4 "MCP6001T-I/OTCT-ND" H -2650 0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Microchip Technology" H -2650 0   50  0001 C CNN "Manufacturer"
F 6 "MCP6001T-I/OT" H -2650 0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 85°C" H -2650 0   50  0001 C CNN "Operating Temperature"
F 8 "~" H -2650 0   50  0001 C CNN "Stuff"
F 9 "579-MCP6001T-I/OT" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    4100 3300
	1    0    0    -1  
$EndComp
$Comp
L eload:MCP6V71T-E_OT U403
U 1 1 5B735032
P 4100 5400
F 0 "U403" H 3900 5800 50  0000 R CNN
F 1 "MCP6V71T" H 3900 5700 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4000 5200 50  0001 L CNN
F 3 "http://www.microchip.com/mymicrochip/filehandler.aspx?ddocname=en573422" H 4100 5600 50  0001 C CNN
F 4 "MCP6V71T-E/OTCT-ND" H -2650 0   50  0001 C CNN "Digi-Key Part Number"
F 5 "Microchip Technology" H -2650 0   50  0001 C CNN "Manufacturer"
F 6 "MCP6V71T-E/OT" H -2650 0   50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 125°C" H -2650 0   50  0001 C CNN "Operating Temperature"
F 8 "~" H -2650 0   50  0001 C CNN "Stuff"
F 9 "579-MCP6V71T-E/OT" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    4100 5400
	-1   0    0    -1  
$EndComp
$Comp
L eload:C C?
U 1 1 5B74BD74
P 3700 2850
AR Path="/5B6010A5/5B74BD74" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B74BD74" Ref="C406"  Part="1" 
F 0 "C406" V 3448 2850 50  0000 C CNN
F 1 "0.1uF" V 3539 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3738 2700 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 3700 2850 50  0001 C CNN
F 4 "KEMET" H 3700 2850 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 3700 2850 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 3700 2850 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 3700 2850 50  0001 C CNN "Tolerance"
F 8 "50V" H 3700 2850 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 3700 2850 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 3700 2850 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2650 0   50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    3700 2850
	0    1    1    0   
$EndComp
$Comp
L eload:+5V0 #PWR0411
U 1 1 5B759A6B
P 4000 2800
F 0 "#PWR0411" H 4000 2650 50  0001 C CNN
F 1 "+5V0" H 4005 2973 50  0000 C CNN
F 2 "" H 4000 2800 50  0001 C CNN
F 3 "" H 4000 2800 50  0001 C CNN
	1    4000 2800
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B75EFF9
P 4000 3650
AR Path="/5B6010A5/5B75EFF9" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B75EFF9" Ref="#PWR0414"  Part="1" 
F 0 "#PWR0414" H 4000 3400 50  0001 C CNN
F 1 "GNDA" H 4005 3477 50  0000 C CNN
F 2 "" H 4000 3650 50  0001 C CNN
F 3 "" H 4000 3650 50  0001 C CNN
	1    4000 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3650 4000 3600
$Comp
L eload:GNDA #PWR?
U 1 1 5B763CE8
P 3500 2900
AR Path="/5B6010A5/5B763CE8" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B763CE8" Ref="#PWR0412"  Part="1" 
F 0 "#PWR0412" H 3500 2650 50  0001 C CNN
F 1 "GNDA" H 3505 2727 50  0000 C CNN
F 2 "" H 3500 2900 50  0001 C CNN
F 3 "" H 3500 2900 50  0001 C CNN
	1    3500 2900
	1    0    0    -1  
$EndComp
Connection ~ 3150 4350
Wire Wire Line
	3150 4350 3150 4750
Connection ~ 3150 4050
Wire Wire Line
	3150 4050 3150 4350
Wire Wire Line
	4800 4050 4950 4050
Wire Wire Line
	4950 4050 4950 3300
Wire Wire Line
	4950 4350 4950 4050
Connection ~ 4950 4050
Wire Wire Line
	4400 5500 4700 5500
$Comp
L eload:C C?
U 1 1 5B78201B
P 3900 5000
AR Path="/5B6010A5/5B78201B" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B78201B" Ref="C410"  Part="1" 
F 0 "C410" V 3648 5000 50  0000 C CNN
F 1 "0.1uF" V 3739 5000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3938 4850 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 3900 5000 50  0001 C CNN
F 4 "KEMET" H 3900 5000 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 3900 5000 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 3900 5000 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 3900 5000 50  0001 C CNN "Tolerance"
F 8 "50V" H 3900 5000 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 3900 5000 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 3900 5000 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2650 0   50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    3900 5000
	0    1    1    0   
$EndComp
$Comp
L eload:GNDA #PWR?
U 1 1 5B79E1E6
P 3700 5050
AR Path="/5B79E1E6" Ref="#PWR?"  Part="1" 
AR Path="/5B6010A5/5B79E1E6" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B79E1E6" Ref="#PWR0417"  Part="1" 
F 0 "#PWR0417" H 3700 4800 50  0001 C CNN
F 1 "GNDA" H 3705 4877 50  0000 C CNN
F 2 "" H 3700 5050 50  0001 C CNN
F 3 "" H 3700 5050 50  0001 C CNN
	1    3700 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2900 3500 2850
Wire Wire Line
	3500 2850 3550 2850
Wire Wire Line
	4000 2800 4000 2850
Wire Wire Line
	3850 2850 4000 2850
Connection ~ 4000 2850
Wire Wire Line
	4000 2850 4000 3000
Wire Wire Line
	4200 4950 4200 5000
Wire Wire Line
	3700 5050 3700 5000
Wire Wire Line
	3700 5000 3750 5000
Wire Wire Line
	4050 5000 4200 5000
Connection ~ 4200 5000
Wire Wire Line
	4200 5000 4200 5100
Wire Wire Line
	3150 5400 3500 5400
Wire Wire Line
	3150 4350 3950 4350
Wire Wire Line
	3150 4050 3400 4050
Wire Wire Line
	4200 5750 4200 5700
Wire Wire Line
	3500 5400 3500 6100
Connection ~ 3500 5400
Wire Wire Line
	3500 5400 3800 5400
Wire Wire Line
	4700 5500 4700 6100
Connection ~ 4700 5500
Wire Wire Line
	4700 5500 5000 5500
Wire Wire Line
	3150 3400 3800 3400
Wire Wire Line
	7750 1700 8500 1700
Wire Wire Line
	7750 2100 8450 2100
Wire Wire Line
	8450 1800 8500 1800
Text Label 9400 1900 0    50   ~ 0
LOAD_P
$Comp
L eload:RK7002BMT116 Q?
U 1 1 5B8E2ED6
P 5700 1700
AR Path="/5B6010A5/5B8E2ED6" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5B8E2ED6" Ref="Q401"  Part="1" 
F 0 "Q401" H 5906 1746 50  0000 L CNN
F 1 "RK7002BMT116" H 5906 1655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6500 1950 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RK7002BM/rk7002bmt116-e" H 5900 1750 50  0001 C CNN
F 4 "Rohm Semiconductor" H 6350 2250 50  0001 C CNN "Manufacturer"
F 5 "RK7002BMT116 " H 6250 2050 50  0001 C CNN "Manufacturer Part Number"
F 6 "RK7002BMT116CT-ND" H 6400 2150 50  0001 C CNN "Digi-Key Part Number"
F 7 "~" H 3950 -50 50  0001 C CNN "Stuff"
F 8 "755-RK7002BMT116" H 3950 -50 50  0001 C CNN "Mouser Part Number"
	1    5700 1700
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5B8E2EDD
P 5800 2000
AR Path="/5B6010A5/5B8E2EDD" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8E2EDD" Ref="#PWR0407"  Part="1" 
F 0 "#PWR0407" H 5800 1750 50  0001 C CNN
F 1 "GNDD" H 5805 1827 50  0000 C CNN
F 2 "" H 5800 2000 50  0001 C CNN
F 3 "" H 5800 2000 50  0001 C CNN
	1    5800 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1900 5800 2000
$Comp
L eload:R_US R?
U 1 1 5B8E2EEB
P 5250 1900
AR Path="/5B8E2EEB" Ref="R?"  Part="1" 
AR Path="/5B6010A5/5B8E2EEB" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B8E2EEB" Ref="R404"  Part="1" 
F 0 "R404" H 5300 2000 50  0000 L CNN
F 1 "10k" H 5300 1900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5290 1890 50  0001 L CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 5250 1900 50  0001 L CNN
F 4 "±1%" H 5300 1800 50  0001 L CNN "Tolerance"
F 5 "±100ppm/°C" H 5250 1900 50  0001 L CNN "Temperature Coefficient"
F 6 "CRCW080510K0FKEAC" H 5250 1900 50  0001 L CNN "Manufacturer Part Number"
F 7 "Vishay Dale" H 5250 1900 50  0001 L CNN "Manufacturer"
F 8 " 541-3976-1-ND " H 5250 1900 50  0001 L CNN "Digi-Key Part Number"
F 9 "-55°C ~ 155°C" H 3950 -650 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W " H 3950 -650 50  0001 C CNN "Power"
F 11 "~" H 3950 -50 50  0001 C CNN "Stuff"
F 12 "GWCR0805-10KFT5 " H 3950 -50 50  0001 C CNN "Mouser Part Number"
	1    5250 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1700 5250 1700
Wire Wire Line
	5250 1700 5250 1750
$Comp
L eload:GNDD #PWR?
U 1 1 5B8E2EF4
P 5250 2050
AR Path="/5B6010A5/5B8E2EF4" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8E2EF4" Ref="#PWR0408"  Part="1" 
F 0 "#PWR0408" H 5250 1800 50  0001 C CNN
F 1 "GNDD" H 5255 1877 50  0000 C CNN
F 2 "" H 5250 2050 50  0001 C CNN
F 3 "" H 5250 2050 50  0001 C CNN
	1    5250 2050
	1    0    0    -1  
$EndComp
Text HLabel 5100 1700 0    50   Input ~ 0
RS_SW
Wire Wire Line
	5250 1700 5100 1700
Connection ~ 5250 1700
$Comp
L eload:+5V0 #PWR?
U 1 1 5B8E7230
P 8150 1350
AR Path="/5B6010A5/5B8E7230" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8E7230" Ref="#PWR0405"  Part="1" 
F 0 "#PWR0405" H 8150 1200 50  0001 C CNN
F 1 "+5V0" H 8155 1523 50  0000 C CNN
F 2 "" H 8150 1350 50  0001 C CNN
F 3 "" H 8150 1350 50  0001 C CNN
	1    8150 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 1350 8150 1400
Text Label 7850 2100 0    50   ~ 0
CONN_RS_+
Text Label 7850 1700 0    50   ~ 0
CONN_RS_-
$Comp
L eload:R_US R402
U 1 1 5B9313BD
P 3450 1450
F 0 "R402" V 3400 1250 50  0000 C CNN
F 1 "1MEG" V 3400 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3490 1440 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 3450 1450 50  0001 C CNN
F 4 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 5 "P1MDACT-ND" H -150 -3400 50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -150 -3400 50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB105V" H -150 -3400 50  0001 C CNN "Manufacturer Part Number"
F 8 " -55°C ~ 155°C" H -150 -3400 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -150 -3400 50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -150 -3400 50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 3500 1700 50  0000 C CNN "Tolerance"
F 12 "279-CPF0805B1M0E1" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    3450 1450
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R401
U 1 1 5B9314AB
P 2700 1100
F 0 "R401" H 2632 1009 50  0000 R CNN
F 1 "100k" H 2632 1100 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2740 1090 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 2700 1100 50  0001 C CNN
F 4 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 5 "P100KDACT-ND" H -150 -3400 50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -150 -3400 50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB104V" H -150 -3400 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -150 -3400 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -150 -3400 50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -150 -3400 50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" H 2632 1191 50  0000 R CNN "Tolerance"
F 12 "284-APC0805B100KN" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    2700 1100
	-1   0    0    1   
$EndComp
$Comp
L eload:C C?
U 1 1 5B9B2E8F
P 2050 1250
AR Path="/5B6010A5/5B9B2E8F" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5B9B2E8F" Ref="C402"  Part="1" 
F 0 "C402" V 1798 1250 50  0000 C CNN
F 1 "0.1uF" V 1889 1250 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2088 1100 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 2050 1250 50  0001 C CNN
F 4 "KEMET" H 2050 1250 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 2050 1250 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 2050 1250 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 2050 1250 50  0001 C CNN "Tolerance"
F 8 "50V" H 2050 1250 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 2050 1250 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 2050 1250 50  0001 C CNN "Operating Temperature"
F 11 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    2050 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	2350 1200 2350 1250
Wire Wire Line
	1750 1300 1750 1250
Wire Wire Line
	1750 1250 1900 1250
Wire Wire Line
	2200 1250 2350 1250
Connection ~ 2350 1250
Wire Wire Line
	2350 1250 2350 1350
$Comp
L eload:GNDA #PWR?
U 1 1 5B9BC716
P 1750 1300
AR Path="/5B6010A5/5B9BC716" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B9BC716" Ref="#PWR0404"  Part="1" 
F 0 "#PWR0404" H 1750 1050 50  0001 C CNN
F 1 "GNDA" H 1755 1127 50  0000 C CNN
F 2 "" H 1750 1300 50  0001 C CNN
F 3 "" H 1750 1300 50  0001 C CNN
	1    1750 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4900 9350 4900
$Comp
L eload:R_Shunt_US R419
U 1 1 5BA12763
P 9350 5400
F 0 "R419" H 9263 5354 50  0000 R CNN
F 1 "0R02" H 9263 5445 50  0000 R CNN
F 2 "eload_fp:R2512_CurrentSense" V 9280 5400 50  0001 C CNN
F 3 "http://www.vishay.com/docs/30325/wsk120618.pdf" H 9350 5400 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "541-2745-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "WSK1206R0100FEA18" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-65°C ~ 170°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 9 "0.5W, 1/2W" H 0   0   50  0001 C CNN "Power"
F 10 "±35ppm/°C" H 0   0   50  0001 C CNN "Temperature Coefficient"
F 11 " ±1%" H 0   0   50  0001 C CNN "Tolerance"
F 12 "71-WSK1206R0100FEA18" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    9350 5400
	-1   0    0    1   
$EndComp
Wire Wire Line
	9350 5200 9350 4900
Connection ~ 9350 4900
Wire Wire Line
	9350 4900 9950 4900
Wire Wire Line
	9350 5700 10000 5700
Wire Wire Line
	9350 5600 9350 5700
Wire Wire Line
	3150 3400 3150 3450
Wire Wire Line
	5800 1400 5800 1500
Wire Wire Line
	5800 1400 6900 1400
Text Label 6700 2200 0    50   ~ 0
V_SENSE_+
Text Label 6700 1800 0    50   ~ 0
V_SENSE_-
Wire Wire Line
	6700 2200 7150 2200
Wire Wire Line
	6700 1800 7150 1800
Text Label 4000 1450 0    50   ~ 0
V_SENSE_+
Text Label 4000 1850 0    50   ~ 0
V_SENSE_-
Text HLabel 2600 5400 0    50   Output ~ 0
I_SENSE
Wire Wire Line
	2850 5400 2850 5350
Connection ~ 2850 5400
Wire Wire Line
	2850 5400 3150 5400
Wire Wire Line
	3050 3450 3150 3450
Connection ~ 3150 3450
Wire Wire Line
	3150 3450 3150 4050
Text HLabel 1150 3200 0    50   Input ~ 0
I_SET
Text Label 6100 3300 0    50   ~ 0
CTRL_OUT
Text Label 9450 4900 0    50   ~ 0
I_SUMMING
Text Label 9450 5700 0    50   ~ 0
LOAD_N
Text Label 3350 3400 0    50   ~ 0
I_FB
Wire Wire Line
	7750 1400 7900 1400
Wire Wire Line
	7900 1400 7900 900 
Wire Wire Line
	7900 900  7600 900 
Connection ~ 7900 1400
Wire Wire Line
	7900 1400 8150 1400
Wire Wire Line
	7300 900  6900 900 
Wire Wire Line
	6900 900  6900 1400
Connection ~ 6900 1400
Wire Wire Line
	6900 1400 7150 1400
$Comp
L eload:GNDA #PWR?
U 1 1 5C11F4A5
P 2250 3600
AR Path="/5C11F4A5" Ref="#PWR?"  Part="1" 
AR Path="/5B6010A5/5C11F4A5" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5C11F4A5" Ref="#PWR0413"  Part="1" 
AR Path="/5B5FBC06/5C11F4A5" Ref="#PWR?"  Part="1" 
F 0 "#PWR0413" H 2250 3350 50  0001 C CNN
F 1 "GNDA" H 2255 3427 50  0000 C CNN
F 2 "" H 2250 3600 50  0001 C CNN
F 3 "" H 2250 3600 50  0001 C CNN
	1    2250 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3600 2250 3550
$Comp
L eload:C C?
U 1 1 5C11F4B3
P 2250 3400
AR Path="/5B6010A5/5C11F4B3" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5C11F4B3" Ref="C407"  Part="1" 
AR Path="/5B5FBC06/5C11F4B3" Ref="C?"  Part="1" 
F 0 "C407" H 2365 3446 50  0000 L CNN
F 1 "0.1uF" H 2365 3355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2288 3250 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 2250 3400 50  0001 C CNN
F 4 "KEMET" H 2250 3400 50  0001 C CNN "Manufacturer"
F 5 "C0805C104Z5VACTU" H 2250 3400 50  0001 C CNN "Manufacturer Part Number"
F 6 "399-1177-1-ND" H 2250 3400 50  0001 C CNN "Digi-Key Part Number"
F 7 "-20%, +80%" H 2250 3400 50  0001 C CNN "Tolerance"
F 8 "50V" H 2250 3400 50  0001 C CNN "Voltage"
F 9 "Y5V (F)" H 2250 3400 50  0001 C CNN "Temperature Coefficient"
F 10 "-30°C ~ 85°C" H 2250 3400 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2650 0   50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    2250 3400
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5C11F4C1
P 1500 3200
AR Path="/5B6010A5/5C11F4C1" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5C11F4C1" Ref="R406"  Part="1" 
AR Path="/5B5FBC06/5C11F4C1" Ref="R?"  Part="1" 
F 0 "R406" V 1550 3300 50  0000 L CNN
F 1 "1k" V 1550 3000 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1540 3190 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 1500 3200 50  0001 C CNN
F 4 "Yageo" V 1500 3200 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 1500 3200 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 1500 3200 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 1450 2950 50  0000 L CNN "Tolerance"
F 8 "0.125W, 1/8W " V 1500 3200 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 1500 3200 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 1500 3200 50  0001 C CNN "Operating Temperature"
F 11 "~" H -3050 0   50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -3050 0   50  0001 C CNN "Mouser Part Number"
	1    1500 3200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2250 3250 2250 3200
Text Label 2550 3200 0    50   ~ 0
I_CTRL
Connection ~ 2250 3200
Wire Wire Line
	1150 3200 1350 3200
Wire Wire Line
	2250 3200 3000 3200
$Comp
L eload:LMV321RILT U401
U 1 1 5B9B098D
P 2250 1650
F 0 "U401" H 2650 1550 50  0000 C CNN
F 1 "LMV321RILT" H 2500 1450 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 2150 1450 50  0001 L CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/b9/d8/3b/8f/a1/8f/46/01/CD00079372.pdf/files/CD00079372.pdf/jcr:content/translations/en.CD00079372.pdf" H 2250 1850 50  0001 C CNN
F 4 "497-4942-1-ND" H -150 -3400 50  0001 C CNN "Digi-Key Part Number"
F 5 "STMicroelectronics" H -150 -3400 50  0001 C CNN "Manufacturer"
F 6 "LMV321RILT" H -150 -3400 50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 125°C" H -150 -3400 50  0001 C CNN "Operating Temperature"
F 8 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 9 "511-LMV321RILT " H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    2250 1650
	-1   0    0    -1  
$EndComp
$Comp
L eload:R_US R403
U 1 1 5B9CCEAC
P 3450 1850
F 0 "R403" V 3400 1650 50  0000 C CNN
F 1 "1MEG" V 3400 2100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3490 1840 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 3450 1850 50  0001 C CNN
F 4 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 5 "P1MDACT-ND" H -150 -3400 50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -150 -3400 50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB105V" H -150 -3400 50  0001 C CNN "Manufacturer Part Number"
F 8 " -55°C ~ 155°C" H -150 -3400 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -150 -3400 50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -150 -3400 50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 3500 2100 50  0000 C CNN "Tolerance"
F 12 "279-CPF0805B1M0E1" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    3450 1850
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R405
U 1 1 5B9CDDC2
P 2200 2400
F 0 "R405" V 2250 2650 50  0000 C CNN
F 1 "100k" V 2250 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2240 2390 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 2200 2400 50  0001 C CNN
F 4 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 5 "P100KDACT-ND" H -150 -3400 50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -150 -3400 50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB104V" H -150 -3400 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -150 -3400 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -150 -3400 50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -150 -3400 50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 2150 2150 50  0000 C CNN "Tolerance"
F 12 "284-APC0805B100KN" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    2200 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3300 1450 3100 1450
Wire Wire Line
	2950 1450 2950 1500
Wire Wire Line
	3300 1850 3100 1850
Wire Wire Line
	2950 1850 2950 1800
Wire Wire Line
	2950 1850 2700 1850
Wire Wire Line
	2700 1850 2700 1750
Wire Wire Line
	2700 1750 2550 1750
Connection ~ 2950 1850
Wire Wire Line
	2950 1450 2700 1450
Wire Wire Line
	2700 1450 2700 1550
Wire Wire Line
	2700 1550 2550 1550
Connection ~ 2950 1450
Wire Wire Line
	2700 1450 2700 1250
Connection ~ 2700 1450
$Comp
L eload:GNDA #PWR?
U 1 1 5B9F5F3C
P 2250 650
AR Path="/5B6010A5/5B9F5F3C" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B9F5F3C" Ref="#PWR0401"  Part="1" 
F 0 "#PWR0401" H 2250 400 50  0001 C CNN
F 1 "GNDA" H 2255 477 50  0000 C CNN
F 2 "" H 2250 650 50  0001 C CNN
F 3 "" H 2250 650 50  0001 C CNN
	1    2250 650 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2250 550  2250 650 
Wire Wire Line
	2700 2400 2350 2400
Wire Wire Line
	2050 2400 1750 2400
Wire Wire Line
	1750 1650 1950 1650
Wire Wire Line
	2350 2000 2350 1950
Connection ~ 2700 1850
Wire Wire Line
	2700 1850 2700 2400
Wire Wire Line
	1750 1650 1750 2400
Wire Wire Line
	3600 1850 4000 1850
Wire Wire Line
	4000 1450 3600 1450
Text HLabel 1300 1650 0    50   Output ~ 0
V_SENSE
Wire Wire Line
	1750 1650 1450 1650
Connection ~ 1750 1650
$Comp
L eload:C C?
U 1 1 5BA686A9
P 2200 2650
AR Path="/5B6010A5/5BA686A9" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5BA686A9" Ref="C405"  Part="1" 
F 0 "C405" V 2250 2850 50  0000 C CNN
F 1 "10pF" V 2250 2450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2238 2500 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 2200 2650 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 2200 2650 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 2200 2650 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 2200 2650 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 2200 2650 50  0001 C CNN "Tolerance"
F 8 "100V" V 2200 2650 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 2200 2650 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 2200 2650 50  0001 C CNN "Operating Temperature"
F 11 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 12 "710-885012007051" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    2200 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 2400 2700 2650
Wire Wire Line
	2700 2650 2350 2650
Connection ~ 2700 2400
Wire Wire Line
	2050 2650 1750 2650
Wire Wire Line
	1750 2650 1750 2400
Connection ~ 1750 2400
$Comp
L eload:C C?
U 1 1 5BA7A1F2
P 2950 1650
AR Path="/5B6010A5/5BA7A1F2" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5BA7A1F2" Ref="C403"  Part="1" 
F 0 "C403" H 3065 1696 50  0000 L CNN
F 1 "10pF" H 3065 1605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2988 1500 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 2950 1650 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 2950 1650 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 2950 1650 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 2950 1650 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 2950 1650 50  0001 C CNN "Tolerance"
F 8 "100V" V 2950 1650 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 2950 1650 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 2950 1650 50  0001 C CNN "Operating Temperature"
F 11 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 12 "710-885012007051" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    2950 1650
	1    0    0    -1  
$EndComp
$Comp
L eload:C C?
U 1 1 5BA82EC5
P 3100 1150
AR Path="/5B6010A5/5BA82EC5" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5BA82EC5" Ref="C401"  Part="1" 
F 0 "C401" H 3215 1196 50  0000 L CNN
F 1 "10pF" H 3215 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3138 1000 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 3100 1150 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 3100 1150 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 3100 1150 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 3100 1150 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 3100 1150 50  0001 C CNN "Tolerance"
F 8 "100V" V 3100 1150 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 3100 1150 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 3100 1150 50  0001 C CNN "Operating Temperature"
F 11 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 12 "710-885012007051" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    3100 1150
	1    0    0    -1  
$EndComp
$Comp
L eload:C C?
U 1 1 5BA82F6B
P 3100 2150
AR Path="/5B6010A5/5BA82F6B" Ref="C?"  Part="1" 
AR Path="/5B5FBD8A/5BA82F6B" Ref="C404"  Part="1" 
F 0 "C404" H 3215 2196 50  0000 L CNN
F 1 "10pF" H 3215 2105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3138 2000 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/885012007076.pdf" H 3100 2150 50  0001 C CNN
F 4 "Wurth Electronics Inc." V 3100 2150 50  0001 C CNN "Manufacturer"
F 5 "885012007076" V 3100 2150 50  0001 C CNN "Manufacturer Part Number"
F 6 "732-12223-1-ND" V 3100 2150 50  0001 C CNN "Digi-Key Part Number"
F 7 "±5%" V 3100 2150 50  0001 C CNN "Tolerance"
F 8 "100V" V 3100 2150 50  0001 C CNN "Voltage"
F 9 "C0G, NP0" V 3100 2150 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 125°C" V 3100 2150 50  0001 C CNN "Operating Temperature"
F 11 "~" H -150 -3400 50  0001 C CNN "Stuff"
F 12 "710-885012007051" H -150 -3400 50  0001 C CNN "Mouser Part Number"
	1    3100 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2000 3100 1850
Connection ~ 3100 1850
Wire Wire Line
	3100 1850 2950 1850
Wire Wire Line
	3100 1450 3100 1300
Connection ~ 3100 1450
Wire Wire Line
	3100 1450 2950 1450
Wire Wire Line
	2700 550  2700 950 
Wire Wire Line
	2250 550  2700 550 
$Comp
L eload:GNDA #PWR?
U 1 1 5BAAF078
P 3350 750
AR Path="/5B6010A5/5BAAF078" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BAAF078" Ref="#PWR0402"  Part="1" 
F 0 "#PWR0402" H 3350 500 50  0001 C CNN
F 1 "GNDA" H 3355 577 50  0000 C CNN
F 2 "" H 3350 750 50  0001 C CNN
F 3 "" H 3350 750 50  0001 C CNN
	1    3350 750 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 1000 3100 700 
Wire Wire Line
	3100 700  3350 700 
Wire Wire Line
	3350 700  3350 750 
$Comp
L eload:GNDA #PWR?
U 1 1 5BAB825E
P 3100 2350
AR Path="/5B6010A5/5BAB825E" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BAB825E" Ref="#PWR0409"  Part="1" 
F 0 "#PWR0409" H 3100 2100 50  0001 C CNN
F 1 "GNDA" H 3105 2177 50  0000 C CNN
F 2 "" H 3100 2350 50  0001 C CNN
F 3 "" H 3100 2350 50  0001 C CNN
	1    3100 2350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3100 2300 3100 2350
$Comp
L eload:R_US R415
U 1 1 5BD1312B
P 9950 4650
F 0 "R415" H 10018 4741 50  0000 L CNN
F 1 "0R1" H 10018 4650 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P7.62mm_Vertical" V 9990 4640 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/throughhole/Yageo_LR_PNP_2013.pdf" H 9950 4650 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "0.1AECT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "PNP300JR-73-0R1" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-40°C ~ 200°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 9 "3W" H 10018 4559 50  0000 L CNN "Power"
F 10 "±5%" H 0   0   50  0001 C CNN "Tolerance"
F 11 "660-MOSX3CT631RR10J" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    9950 4650
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R414
U 1 1 5BD131ED
P 8800 4650
F 0 "R414" H 8868 4741 50  0000 L CNN
F 1 "0R1" H 8868 4650 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P7.62mm_Vertical" V 8840 4640 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/throughhole/Yageo_LR_PNP_2013.pdf" H 8800 4650 50  0001 C CNN
F 4 "~" H 0   0   50  0001 C CNN "Stuff"
F 5 "0.1AECT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "PNP300JR-73-0R1" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-40°C ~ 200°C" H 0   0   50  0001 C CNN "Operating Temperature"
F 9 "3W" H 8868 4559 50  0000 L CNN "Power"
F 10 "±5%" H 0   0   50  0001 C CNN "Tolerance"
F 11 "660-MOSX3CT631RR10J" H 0   0   50  0001 C CNN "Mouser Part Number"
	1    8800 4650
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R421
U 1 1 5B7603D2
P 4100 6100
F 0 "R421" V 4050 5800 50  0000 C CNN
F 1 "93k1" V 4050 6400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4140 6090 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 4100 6100 50  0001 C CNN
F 4 "~" H -2650 0   50  0001 C CNN "Stuff"
F 5 "P71.5KDACT-ND" H -2650 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -2650 0   50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB7152V" H -2650 0   50  0001 C CNN "Manufacturer Part Number"
F 8 " -55°C ~ 155°C" H -2650 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -2650 0   50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -2650 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 4150 6400 50  0000 C CNN "Tolerance"
F 12 "756-PCF0805R71K5BI" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    4100 6100
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R420
U 1 1 5B760841
P 5150 5500
F 0 "R420" V 5100 5300 50  0000 C CNN
F 1 "2k37" V 5100 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5190 5490 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 5150 5500 50  0001 C CNN
F 4 "~" H -2650 0   50  0001 C CNN "Stuff"
F 5 "P1.1KDACT-ND" H -2650 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -2650 0   50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB112V" H -2650 0   50  0001 C CNN "Manufacturer Part Number"
F 8 " -55°C ~ 155°C" H -2650 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -2650 0   50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -2650 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 5200 5750 50  0000 C CNN "Tolerance"
F 12 "279-CPF0805B1K1E1" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    5150 5500
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R418
U 1 1 5B7608EB
P 5150 5300
F 0 "R418" V 5100 5100 50  0000 C CNN
F 1 "23k7" V 5100 5550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5190 5290 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 5150 5300 50  0001 C CNN
F 4 "~" H -2650 0   50  0001 C CNN "Stuff"
F 5 "P1.1KDACT-ND" H -2650 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -2650 0   50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB112V" H -2650 0   50  0001 C CNN "Manufacturer Part Number"
F 8 " -55°C ~ 155°C" H -2650 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -2650 0   50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -2650 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 5200 5550 50  0000 C CNN "Tolerance"
F 12 "279-CPF0805B1K1E1" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    5150 5300
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R417
U 1 1 5B762206
P 5100 5000
F 0 "R417" V 5050 4800 50  0000 C CNN
F 1 "93k1" V 5050 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5140 4990 50  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDM0000/AOA0000C307.pdf" H 5100 5000 50  0001 C CNN
F 4 "DNS" V 5000 5000 50  0000 C CNN "Stuff"
F 5 "P71.5KDACT-ND" H -2650 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Panasonic Electronic Components" H -2650 0   50  0001 C CNN "Manufacturer"
F 7 "ERA-6AEB7152V" H -2650 0   50  0001 C CNN "Manufacturer Part Number"
F 8 " -55°C ~ 155°C" H -2650 0   50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W" H -2650 0   50  0001 C CNN "Power"
F 10 "±25ppm/°C" H -2650 0   50  0001 C CNN "Temperature Coefficient"
F 11 "±0.1%" V 5150 5350 50  0000 C CNN "Tolerance"
F 12 "756-PCF0805R71K5BI" H -2650 0   50  0001 C CNN "Mouser Part Number"
	1    5100 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 5300 4800 5000
Wire Wire Line
	4800 5000 4950 5000
Connection ~ 4800 5300
Wire Wire Line
	4800 5300 5000 5300
$Comp
L eload:GNDA #PWR?
U 1 1 5B76B8C9
P 5650 5000
AR Path="/5B76B8C9" Ref="#PWR?"  Part="1" 
AR Path="/5B6010A5/5B76B8C9" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B76B8C9" Ref="#PWR0416"  Part="1" 
F 0 "#PWR0416" H 5650 4750 50  0001 C CNN
F 1 "GNDA" H 5655 4827 50  0000 C CNN
F 2 "" H 5650 5000 50  0001 C CNN
F 3 "" H 5650 5000 50  0001 C CNN
	1    5650 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5000 5650 5000
Text Label 8600 4300 0    50   ~ 0
L_D3
Text Label 9750 4300 0    50   ~ 0
L_D4
Text Label 8350 3900 0    50   ~ 0
L_G3
Text Label 9500 3900 0    50   ~ 0
L_G4
Text Label 8700 5300 0    50   ~ 0
SHUNT+
Text Label 4450 5300 0    50   ~ 0
I_OA_+
Text Label 4450 5500 0    50   ~ 0
I_OA_-
Text Label 3850 4050 0    50   ~ 0
CTRL_FB_RC
Connection ~ 4950 3300
Wire Wire Line
	4400 3300 4950 3300
Wire Wire Line
	3700 4050 4500 4050
Wire Wire Line
	4250 4350 4950 4350
Text Label 6150 1400 0    50   ~ 0
RS_R_D
Text Label 2750 1450 0    50   ~ 0
RS_+
Text Label 2750 1850 0    50   ~ 0
RS_-
$Comp
L eload:Conn_01x02 J402
U 1 1 5B8C5375
P 8700 1800
F 0 "J402" H 8619 1475 50  0000 C CNN
F 1 "Conn_01x02" H 8619 1566 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 8700 1800 50  0001 C CNN
F 3 "~" H 8700 1800 50  0001 C CNN
	1    8700 1800
	1    0    0    1   
$EndComp
$Comp
L eload:Fuse F401
U 1 1 5B7BD37C
P 9350 2300
F 0 "F401" H 9290 2254 50  0000 R CNN
F 1 "Fuse" H 9290 2345 50  0000 R CNN
F 2 "eload_fp:Fuse_ClipSet_5x20mm" V 9280 2300 50  0001 C CNN
F 3 "~" H 9350 2300 50  0001 C CNN
	1    9350 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	7750 1900 7850 1900
Wire Wire Line
	8450 2100 8450 1800
Wire Wire Line
	9350 2150 9350 2050
Wire Wire Line
	9350 1900 9750 1900
Text Label 9350 2950 1    50   ~ 0
LOAD_FUSE
$Comp
L eload:+3V3_A #PWR?
U 1 1 5BD31D26
P 2350 1200
AR Path="/5B5FBC06/5BD31D26" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BD31D26" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BD31D26" Ref="#PWR0403"  Part="1" 
F 0 "#PWR0403" H 2350 1050 50  0001 C CNN
F 1 "+3V3_A" H 2355 1373 50  0000 C CNN
F 2 "" H 2350 1200 50  0001 C CNN
F 3 "" H 2350 1200 50  0001 C CNN
	1    2350 1200
	1    0    0    -1  
$EndComp
$Comp
L eload:+3V3_A #PWR?
U 1 1 5BD391C1
P 4200 4950
AR Path="/5B5FBC06/5BD391C1" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BD391C1" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5BD391C1" Ref="#PWR0415"  Part="1" 
F 0 "#PWR0415" H 4200 4800 50  0001 C CNN
F 1 "+3V3_A" H 4205 5123 50  0000 C CNN
F 2 "" H 4200 4950 50  0001 C CNN
F 3 "" H 4200 4950 50  0001 C CNN
	1    4200 4950
	1    0    0    -1  
$EndComp
Connection ~ 8450 3900
Wire Wire Line
	8300 3900 8300 3850
Wire Wire Line
	8300 3900 8450 3900
Wire Wire Line
	9450 3900 9450 3850
Wire Wire Line
	9450 3900 9600 3900
$Comp
L eload:Q_NMOS_GDS Q?
U 1 1 5BAEC790
P 6400 3900
AR Path="/5BAEC790" Ref="Q?"  Part="1" 
AR Path="/5B6010A5/5BAEC790" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5BAEC790" Ref="Q402"  Part="1" 
F 0 "Q402" H 6605 3946 50  0000 L CNN
F 1 "IRL530NPBF" H 6605 3855 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6600 4000 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irl530npbf.pdf?fileId=5546d462533600a40153565fad5c2560" H 6400 3900 50  0001 C CNN
F 4 "Infineon Technologies" H 6400 3900 50  0001 C CNN "Manufacturer"
F 5 "IRL530NPBF" H 6400 3900 50  0001 C CNN "Manufacturer Part Number"
F 6 "17A" H -3100 600 50  0001 C CNN "Current"
F 7 "IRL530NPBF-ND" H -3100 600 50  0001 C CNN "Digi-Key Part Number"
F 8 "-55°C ~ 175°C (TJ)" H -3100 600 50  0001 C CNN "Operating Temperature"
F 9 " 79W (Tc)" H -3100 600 50  0001 C CNN "Power"
F 10 "100V" H -3100 600 50  0001 C CNN "Voltage"
F 11 "~" H -2300 0   50  0001 C CNN "Stuff"
F 12 "942-IRL530NPBF" H -2300 0   50  0001 C CNN "Mouser Part Number"
	1    6400 3900
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BAEC7A0
P 6000 3700
AR Path="/5B6010A5/5BAEC7A0" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BAEC7A0" Ref="R407"  Part="1" 
F 0 "R407" H 5932 3654 50  0000 R CNN
F 1 "100R" H 5932 3745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6040 3690 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 6000 3700 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 6000 3700 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 6000 3700 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 6000 3700 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 6000 3700 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 6000 3700 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 6000 3700 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 6000 3700 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2350 -50 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -2350 -50 50  0001 C CNN "Mouser Part Number"
	1    6000 3700
	-1   0    0    1   
$EndComp
$Comp
L eload:Q_NMOS_GDS Q?
U 1 1 5BAEC7B0
P 7550 3900
AR Path="/5BAEC7B0" Ref="Q?"  Part="1" 
AR Path="/5B6010A5/5BAEC7B0" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5BAEC7B0" Ref="Q403"  Part="1" 
F 0 "Q403" H 7755 3946 50  0000 L CNN
F 1 "IRL530NPBF" H 7755 3855 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7750 4000 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irl530npbf.pdf?fileId=5546d462533600a40153565fad5c2560" H 7550 3900 50  0001 C CNN
F 4 "Infineon Technologies" H 7550 3900 50  0001 C CNN "Manufacturer"
F 5 "IRL530NPBF" H 7550 3900 50  0001 C CNN "Manufacturer Part Number"
F 6 "17A" H -1950 600 50  0001 C CNN "Current"
F 7 "IRL530NPBF-ND" H -1950 600 50  0001 C CNN "Digi-Key Part Number"
F 8 "-55°C ~ 175°C (TJ)" H -1950 600 50  0001 C CNN "Operating Temperature"
F 9 " 79W (Tc)" H -1950 600 50  0001 C CNN "Power"
F 10 "100V" H -1950 600 50  0001 C CNN "Voltage"
F 11 "~" H -2300 0   50  0001 C CNN "Stuff"
F 12 "942-IRL530NPBF" H -2300 0   50  0001 C CNN "Mouser Part Number"
	1    7550 3900
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BAEC7C0
P 7150 3700
AR Path="/5B6010A5/5BAEC7C0" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BAEC7C0" Ref="R408"  Part="1" 
F 0 "R408" H 7218 3746 50  0000 L CNN
F 1 "100R" H 7218 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7190 3690 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7150 3700 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7150 3700 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7150 3700 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7150 3700 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7150 3700 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7150 3700 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7150 3700 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7150 3700 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2350 -50 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -2350 -50 50  0001 C CNN "Mouser Part Number"
	1    7150 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4800 6500 4900
Wire Wire Line
	7650 4900 7650 4800
Wire Wire Line
	7650 3700 7650 3050
Wire Wire Line
	6500 3050 6500 3700
Wire Wire Line
	6150 3900 6150 3950
Wire Wire Line
	6150 3900 6200 3900
Wire Wire Line
	7300 3900 7300 3950
Connection ~ 7300 3900
Wire Wire Line
	7300 3900 7350 3900
$Comp
L eload:D_TVS D?
U 1 1 5BAEC7F1
P 7300 4100
AR Path="/5B6010A5/5BAEC7F1" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5BAEC7F1" Ref="D403"  Part="1" 
F 0 "D403" V 7209 4022 50  0000 R CNN
F 1 "824501500" V 7300 4022 50  0000 R CNN
F 2 "Diode_SMD:D_SMA" H 7300 4100 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/824501500.pdf" H 7300 4100 50  0001 C CNN
F 4 "43.5A " H -2200 -300 50  0001 C CNN "Current"
F 5 "732-9939-1-ND" H -2200 -300 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -2200 -300 50  0001 C CNN "Manufacturer"
F 7 "824501500" H -2200 -300 50  0001 C CNN "Manufacturer Part Number"
F 8 "-65°C ~ 150°C" H -2200 -300 50  0001 C CNN "Operating Temperature"
F 9 "400W " H -2200 -300 50  0001 C CNN "Power"
F 10 "5V" V 7391 4022 50  0000 R CNN "Voltage"
F 11 "~" H -2300 -50 50  0001 C CNN "Stuff"
F 12 "710-824501500 " H -2300 -50 50  0001 C CNN "Mouser Part Number"
	1    7300 4100
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R413
U 1 1 5BAEC800
P 7650 4650
F 0 "R413" H 7718 4741 50  0000 L CNN
F 1 "0R1" H 7718 4650 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P7.62mm_Vertical" V 7690 4640 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/throughhole/Yageo_LR_PNP_2013.pdf" H 7650 4650 50  0001 C CNN
F 4 "~" H -2300 0   50  0001 C CNN "Stuff"
F 5 "0.1AECT-ND" H -2300 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Yageo" H -2300 0   50  0001 C CNN "Manufacturer"
F 7 "PNP300JR-73-0R1" H -2300 0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-40°C ~ 200°C" H -2300 0   50  0001 C CNN "Operating Temperature"
F 9 "3W" H 7718 4559 50  0000 L CNN "Power"
F 10 "±5%" H -2300 0   50  0001 C CNN "Tolerance"
F 11 "660-MOSX3CT631RR10J" H -2300 0   50  0001 C CNN "Mouser Part Number"
	1    7650 4650
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R412
U 1 1 5BAEC80F
P 6500 4650
F 0 "R412" H 6568 4741 50  0000 L CNN
F 1 "0R1" H 6568 4650 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0516_L15.5mm_D5.0mm_P7.62mm_Vertical" V 6540 4640 50  0001 C CNN
F 3 "http://www.yageo.com/NewPortal/yageodocoutput?fileName=/pdf/throughhole/Yageo_LR_PNP_2013.pdf" H 6500 4650 50  0001 C CNN
F 4 "~" H -2300 0   50  0001 C CNN "Stuff"
F 5 "0.1AECT-ND" H -2300 0   50  0001 C CNN "Digi-Key Part Number"
F 6 "Yageo" H -2300 0   50  0001 C CNN "Manufacturer"
F 7 "PNP300JR-73-0R1" H -2300 0   50  0001 C CNN "Manufacturer Part Number"
F 8 "-40°C ~ 200°C" H -2300 0   50  0001 C CNN "Operating Temperature"
F 9 "3W" H 6568 4559 50  0000 L CNN "Power"
F 10 "±5%" H -2300 0   50  0001 C CNN "Tolerance"
F 11 "660-MOSX3CT631RR10J" H -2300 0   50  0001 C CNN "Mouser Part Number"
	1    6500 4650
	1    0    0    -1  
$EndComp
Text Label 6300 4300 0    50   ~ 0
L_D1
Text Label 7450 4300 0    50   ~ 0
L_D2
Text Label 6050 3900 0    50   ~ 0
L_G1
Text Label 7200 3900 0    50   ~ 0
L_G2
Connection ~ 6150 3900
Wire Wire Line
	6000 3900 6000 3850
Wire Wire Line
	6000 3900 6150 3900
Wire Wire Line
	7150 3900 7150 3850
Wire Wire Line
	7150 3900 7300 3900
Wire Wire Line
	6500 3050 7650 3050
Wire Wire Line
	8800 3050 7650 3050
Connection ~ 8800 3050
Connection ~ 7650 3050
Wire Wire Line
	9450 3300 9450 3550
Wire Wire Line
	8300 3550 8300 3300
Connection ~ 8300 3300
Wire Wire Line
	8300 3300 9450 3300
Wire Wire Line
	7150 3550 7150 3300
Connection ~ 7150 3300
Wire Wire Line
	7150 3300 8300 3300
Wire Wire Line
	6000 3550 6000 3300
Wire Wire Line
	6000 3300 7150 3300
Wire Wire Line
	6500 4900 7650 4900
Connection ~ 8800 4900
Connection ~ 7650 4900
Wire Wire Line
	7650 4900 8800 4900
Wire Wire Line
	5300 5300 9200 5300
Text Label 8700 5500 0    50   ~ 0
SHUNT-
$Comp
L eload:D_TVS D?
U 1 1 5BAEC7E1
P 6150 4100
AR Path="/5B6010A5/5BAEC7E1" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5BAEC7E1" Ref="D402"  Part="1" 
F 0 "D402" V 6059 4022 50  0000 R CNN
F 1 "824501500" V 6150 4022 50  0000 R CNN
F 2 "Diode_SMD:D_SMA" H 6150 4100 50  0001 C CNN
F 3 "http://katalog.we-online.de/pbs/datasheet/824501500.pdf" H 6150 4100 50  0001 C CNN
F 4 "43.5A " H -2200 -300 50  0001 C CNN "Current"
F 5 "732-9939-1-ND" H -2200 -300 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H -2200 -300 50  0001 C CNN "Manufacturer"
F 7 "824501500" H -2200 -300 50  0001 C CNN "Manufacturer Part Number"
F 8 "-65°C ~ 150°C" H -2200 -300 50  0001 C CNN "Operating Temperature"
F 9 "400W " H -2200 -300 50  0001 C CNN "Power"
F 10 "5V" V 6241 4022 50  0000 R CNN "Voltage"
F 11 "~" H -2300 -50 50  0001 C CNN "Stuff"
F 12 "710-824501500 " H -2300 -50 50  0001 C CNN "Mouser Part Number"
	1    6150 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 4100 6500 4300
Wire Wire Line
	6150 4250 6150 4300
Wire Wire Line
	6150 4300 6500 4300
Connection ~ 6500 4300
Wire Wire Line
	6500 4300 6500 4500
Wire Wire Line
	7650 4100 7650 4300
Wire Wire Line
	7300 4250 7300 4300
Wire Wire Line
	7300 4300 7650 4300
Connection ~ 7650 4300
Wire Wire Line
	7650 4300 7650 4500
Wire Wire Line
	8800 4100 8800 4300
Wire Wire Line
	8450 4250 8450 4300
Wire Wire Line
	8450 4300 8800 4300
Connection ~ 8800 4300
Wire Wire Line
	8800 4300 8800 4500
Wire Wire Line
	9950 4100 9950 4300
Wire Wire Line
	9600 4250 9600 4300
Wire Wire Line
	9600 4300 9950 4300
Connection ~ 9950 4300
Wire Wire Line
	9950 4300 9950 4500
Text Label 7850 1900 0    50   ~ 0
LOAD_N
$Comp
L eload:G6K-2 K401
U 1 1 5B857D8F
P 7450 1800
F 0 "K401" V 6683 1800 50  0000 C CNN
F 1 "G6K-2" V 6774 1800 50  0000 C CNN
F 2 "Relay_SMD:Relay_DPDT_Omron_G6K-2F-Y" H 8100 1750 50  0001 L CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g6k.pdf" H 7250 1800 50  0001 C CNN
	1    7450 1800
	0    1    1    0   
$EndComp
$Comp
L eload:-0V232 #PWR0109
U 1 1 5B9DE12D
P 4200 5750
F 0 "#PWR0109" H 4200 5600 50  0001 C CNN
F 1 "-0V232" H 4205 5923 50  0000 C CNN
F 2 "" H 4200 5750 50  0001 C CNN
F 3 "" H 4200 5750 50  0001 C CNN
	1    4200 5750
	-1   0    0    1   
$EndComp
$Comp
L eload:-0V232 #PWR0110
U 1 1 5B9DE3A1
P 2350 2000
F 0 "#PWR0110" H 2350 1850 50  0001 C CNN
F 1 "-0V232" H 2355 2173 50  0000 C CNN
F 2 "" H 2350 2000 50  0001 C CNN
F 3 "" H 2350 2000 50  0001 C CNN
	1    2350 2000
	-1   0    0    1   
$EndComp
$Comp
L eload:D_Schottky D?
U 1 1 5B80FCBB
P 7450 900
AR Path="/5B6C0220/5B80FCBB" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5B80FCBB" Ref="D401"  Part="1" 
F 0 "D401" H 7450 684 50  0000 C CNN
F 1 "DSS13UTR" H 7450 775 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123F" H 7450 900 50  0001 C CNN
F 3 "http://www.smc-diodes.com/propdf/DSS12U%20THRU%20DSS125U%20N1873%20REV.A.pdf" H 7450 900 50  0001 C CNN
F 4 "1A" H -1350 -2000 50  0001 C CNN "Current"
F 5 "1655-1926-1-ND" H -1350 -2000 50  0001 C CNN "Digi-Key Part Number"
F 6 "SMC Diode Solutions" H -1350 -2000 50  0001 C CNN "Manufacturer"
F 7 "DSS13UTR" H -1350 -2000 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 150°C" H -1350 -2000 50  0001 C CNN "Operating Temperature"
F 9 "30V" H -1350 -2000 50  0001 C CNN "Voltage"
F 10 "~" H -1350 -2000 50  0001 C CNN "Stuff"
F 11 "841-PMEG4010EGWX" H -1350 -2000 50  0001 C CNN "Mouser Part Number"
	1    7450 900 
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B834E5E
P 5500 3300
AR Path="/5B6010A5/5B834E5E" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B834E5E" Ref="R422"  Part="1" 
F 0 "R422" V 5705 3300 50  0000 C CNN
F 1 "100R" V 5614 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5540 3290 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 5500 3300 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 5500 3300 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 5500 3300 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 5500 3300 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 5500 3300 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 5500 3300 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 5500 3300 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 5500 3300 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2850 -450 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -2850 -450 50  0001 C CNN "Mouser Part Number"
	1    5500 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 3300 5850 3300
Connection ~ 6000 3300
Text Label 4600 3300 0    50   ~ 0
CTRL_OUT_R
$Comp
L eload:GNDA #PWR?
U 1 1 5B81E0B2
P 5650 6000
AR Path="/5B81E0B2" Ref="#PWR?"  Part="1" 
AR Path="/5B6010A5/5B81E0B2" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B81E0B2" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 5650 5750 50  0001 C CNN
F 1 "GNDA" H 5655 5827 50  0000 C CNN
F 2 "" H 5650 6000 50  0001 C CNN
F 3 "" H 5650 6000 50  0001 C CNN
	1    5650 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5500 9200 5500
Wire Wire Line
	5300 5500 5650 5500
Connection ~ 5650 5500
Wire Wire Line
	5650 5600 5650 5500
$Comp
L eload:GNDA #PWR?
U 1 1 5B833BFC
P 9350 5750
AR Path="/5B833BFC" Ref="#PWR?"  Part="1" 
AR Path="/5B6010A5/5B833BFC" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B833BFC" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 9350 5500 50  0001 C CNN
F 1 "GNDA" H 9355 5577 50  0000 C CNN
F 2 "" H 9350 5750 50  0001 C CNN
F 3 "" H 9350 5750 50  0001 C CNN
	1    9350 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 5750 9350 5700
Connection ~ 9350 5700
$Comp
L eload:TestPoint TP?
U 1 1 5B849F80
P 1450 1650
AR Path="/5B6010A5/5B849F80" Ref="TP?"  Part="1" 
AR Path="/5B5FBD8A/5B849F80" Ref="TP404"  Part="1" 
F 0 "TP404" H 1350 1900 50  0000 L CNN
F 1 "TestPoint" H 1508 1679 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1650 1650 50  0001 C CNN
F 3 "~" H 1650 1650 50  0001 C CNN
F 4 "~" H -6300 -1600 50  0001 C CNN "Stuff"
	1    1450 1650
	1    0    0    -1  
$EndComp
Connection ~ 1450 1650
Wire Wire Line
	1450 1650 1300 1650
Wire Wire Line
	5850 3250 5850 3300
Connection ~ 5850 3300
Wire Wire Line
	5850 3300 6000 3300
Wire Wire Line
	4950 3300 5350 3300
Wire Wire Line
	8800 3050 9350 3050
Wire Wire Line
	9350 2450 9350 3050
Connection ~ 9350 3050
Wire Wire Line
	9350 3050 9950 3050
Wire Wire Line
	8850 2300 8850 2050
Wire Wire Line
	8850 2050 9350 2050
Wire Wire Line
	7750 2300 8850 2300
Connection ~ 9350 2050
Wire Wire Line
	9350 2050 9350 1900
$Comp
L eload:R_US R?
U 1 1 5B866F95
P 5650 5750
AR Path="/5B6C0220/5B866F95" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B866F95" Ref="R423"  Part="1" 
F 0 "R423" H 5582 5704 50  0000 R CNN
F 1 "0R" H 5582 5795 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5690 5740 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" H 5650 5750 50  0001 C CNN
F 4 "~" H 1800 2600 50  0001 C CNN "Stuff"
F 5 "660-RK73Z2ATTD" H 1800 2600 50  0001 C CNN "Mouser Part Number"
F 6 "311-0.0ARCT-ND" H 1800 2600 50  0001 C CNN "Digi-Key Part Number"
F 7 "Yageo" H 1800 2600 50  0001 C CNN "Manufacturer"
F 8 "RC0805JR-070RL" H 1800 2600 50  0001 C CNN "Manufacturer Part Number"
F 9 " -55°C ~ 155°C" H 1800 2600 50  0001 C CNN "Operating Temperature"
F 10 "0.125W, 1/8W" H 1800 2600 50  0001 C CNN "Power"
	1    5650 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 5900 5650 6000
$Comp
L eload:R_US R?
U 1 1 5B8AAA7A
P 1900 3450
AR Path="/5B6010A5/5B8AAA7A" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B8AAA7A" Ref="R424"  Part="1" 
AR Path="/5B5FBC06/5B8AAA7A" Ref="R?"  Part="1" 
F 0 "R424" H 1968 3587 50  0000 L CNN
F 1 "1k" H 1968 3496 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1940 3440 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 1900 3450 50  0001 C CNN
F 4 "Yageo" V 1900 3450 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 1900 3450 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 1900 3450 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 1968 3405 50  0000 L CNN "Tolerance"
F 8 "0.125W, 1/8W " V 1900 3450 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 1900 3450 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 1900 3450 50  0001 C CNN "Operating Temperature"
F 11 "DNS" H 1968 3314 50  0000 L CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -2650 250 50  0001 C CNN "Mouser Part Number"
	1    1900 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3200 1900 3200
Wire Wire Line
	1900 3300 1900 3200
Connection ~ 1900 3200
Wire Wire Line
	1900 3200 2250 3200
$Comp
L eload:GNDA #PWR?
U 1 1 5B8D298C
P 1900 3650
AR Path="/5B8D298C" Ref="#PWR?"  Part="1" 
AR Path="/5B6010A5/5B8D298C" Ref="#PWR?"  Part="1" 
AR Path="/5B5FBD8A/5B8D298C" Ref="#PWR0406"  Part="1" 
AR Path="/5B5FBC06/5B8D298C" Ref="#PWR?"  Part="1" 
F 0 "#PWR0406" H 1900 3400 50  0001 C CNN
F 1 "GNDA" H 1905 3477 50  0000 C CNN
F 2 "" H 1900 3650 50  0001 C CNN
F 3 "" H 1900 3650 50  0001 C CNN
	1    1900 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3600 1900 3650
$Comp
L eload:TestPoint TP?
U 1 1 5B8DCAF7
P 3000 3150
AR Path="/5B6010A5/5B8DCAF7" Ref="TP?"  Part="1" 
AR Path="/5B5FBD8A/5B8DCAF7" Ref="TP405"  Part="1" 
F 0 "TP405" H 2900 3400 50  0000 L CNN
F 1 "TestPoint" H 3058 3179 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 3200 3150 50  0001 C CNN
F 3 "~" H 3200 3150 50  0001 C CNN
F 4 "~" H -4750 -100 50  0001 C CNN "Stuff"
	1    3000 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3150 3000 3200
Connection ~ 3000 3200
Wire Wire Line
	3000 3200 3800 3200
$EndSCHEMATC

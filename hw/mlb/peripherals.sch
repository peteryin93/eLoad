EESchema Schematic File Version 4
LIBS:eLoad-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 6 6
Title ""
Date "2018-08-10"
Rev "0.1"
Comp "Peter Yin"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eload:Micro_SD_Card_Det J602
U 1 1 5BCDF5B7
P 8250 1950
F 0 "J602" H 8200 3017 50  0000 C CNN
F 1 "Micro_SD_Card_Det" H 8200 2926 50  0000 C CNN
F 2 "eload_fp:GCT-MEM2055-00-190-01-A" H 10300 2650 50  0001 C CNN
F 3 "" H 8250 2050 50  0001 C CNN
F 4 "~" H 150 200 50  0001 C CNN "Stuff"
	1    8250 1950
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BAFBA67
P 1950 7400
AR Path="/5B5FBC06/5BAFBA67" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBA67" Ref="R613"  Part="1" 
F 0 "R613" H 2018 7446 50  0000 L CNN
F 1 "R_US" H 2018 7355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1990 7390 50  0001 C CNN
F 3 "~" H 1950 7400 50  0001 C CNN
F 4 "~" H 550 2750 50  0001 C CNN "Stuff"
	1    1950 7400
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BAFBA6E
P 1950 6950
AR Path="/5B5FBC06/5BAFBA6E" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBA6E" Ref="R612"  Part="1" 
F 0 "R612" H 2018 6996 50  0000 L CNN
F 1 "R_US" H 2018 6905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1990 6940 50  0001 C CNN
F 3 "~" H 1950 6950 50  0001 C CNN
F 4 "~" H 550 2750 50  0001 C CNN "Stuff"
	1    1950 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 6900 2450 6750
Wire Wire Line
	2450 6750 2200 6750
Wire Wire Line
	1950 6750 1950 6800
Wire Wire Line
	1950 7100 1950 7150
Wire Wire Line
	1950 7150 2450 7150
Wire Wire Line
	2450 7150 2450 7000
$Comp
L eload:+3V3 #PWR?
U 1 1 5BAFBA82
P 2200 6700
AR Path="/5B5FBC06/5BAFBA82" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBA82" Ref="#PWR0614"  Part="1" 
F 0 "#PWR0614" H 2200 6550 50  0001 C CNN
F 1 "+3V3" H 2205 6873 50  0000 C CNN
F 2 "" H 2200 6700 50  0001 C CNN
F 3 "" H 2200 6700 50  0001 C CNN
	1    2200 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6700 2200 6750
Connection ~ 2200 6750
Wire Wire Line
	2200 6750 1950 6750
Wire Wire Line
	1950 7250 1950 7200
Connection ~ 1950 7150
Wire Wire Line
	1950 7550 1950 7600
Wire Wire Line
	1650 7200 1950 7200
Connection ~ 1950 7200
Wire Wire Line
	1950 7200 1950 7150
$Comp
L eload:Q_PMOS_GSD Q?
U 1 1 5BAFBA97
P 4800 4500
AR Path="/5B6C0220/5BAFBA97" Ref="Q?"  Part="1" 
AR Path="/5B5FBC06/5BAFBA97" Ref="Q?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBA97" Ref="Q601"  Part="1" 
F 0 "Q601" H 5006 4546 50  0000 L CNN
F 1 "IRLML2246TRPBF" H 5006 4455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5000 4600 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irlml2246pbf.pdf?fileId=5546d462533600a401535664c91f25f2" H 4800 4500 50  0001 C CNN
F 4 "IRLML2246TRPBFCT-ND" H 2850 3150 50  0001 C CNN "Digi-Key Part Number"
F 5 "Infineon Technologies" H 2850 3150 50  0001 C CNN "Manufacturer"
F 6 "IRLML2246TRPBF" H 2850 3150 50  0001 C CNN "Manufacturer Part Number"
F 7 "-55°C ~ 150°C (TJ)" H 2850 3150 50  0001 C CNN "Operating Temperature"
F 8 "1.3W (Ta)" H 2850 3150 50  0001 C CNN "Power"
F 9 "20V" H 2850 3150 50  0001 C CNN "Voltage"
F 10 "~" H 2850 3150 50  0001 C CNN "Stuff"
F 11 "755-RSC002P03T316" H 2850 3150 50  0001 C CNN "Mouser Part Number"
	1    4800 4500
	1    0    0    -1  
$EndComp
$Comp
L eload:+12V0 #PWR?
U 1 1 5BAFBADB
P 4900 4000
AR Path="/5B5FBC06/5BAFBADB" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBADB" Ref="#PWR0601"  Part="1" 
F 0 "#PWR0601" H 4900 3850 50  0001 C CNN
F 1 "+12V0" H 4905 4173 50  0000 C CNN
F 2 "" H 4900 4000 50  0001 C CNN
F 3 "" H 4900 4000 50  0001 C CNN
	1    4900 4000
	1    0    0    -1  
$EndComp
$Comp
L eload:Conn_01x03 J?
U 1 1 5BAFBAF9
P 2850 7000
AR Path="/5B5FBC06/5BAFBAF9" Ref="J?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBAF9" Ref="J603"  Part="1" 
F 0 "J603" H 3000 7000 50  0000 C CNN
F 1 "Conn_01x03" H 3200 7100 50  0000 C CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 2850 7000 50  0001 C CNN
F 3 "~" H 2850 7000 50  0001 C CNN
F 4 "~" H 550 2750 50  0001 C CNN "Stuff"
	1    2850 7000
	1    0    0    1   
$EndComp
Wire Wire Line
	2650 6900 2450 6900
Wire Wire Line
	2650 7000 2450 7000
Wire Wire Line
	2650 7100 2600 7100
Wire Wire Line
	2600 7100 2600 7200
Wire Wire Line
	4900 4700 4900 4800
$Comp
L eload:RK7002BMT116 Q?
U 1 1 5BAFBB11
P 4250 4800
AR Path="/5B6010A5/5BAFBB11" Ref="Q?"  Part="1" 
AR Path="/5B5FBD8A/5BAFBB11" Ref="Q?"  Part="1" 
AR Path="/5B5FBC06/5BAFBB11" Ref="Q?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBB11" Ref="Q602"  Part="1" 
F 0 "Q602" H 4456 4846 50  0000 L CNN
F 1 "RK7002BMT116" H 4456 4755 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5050 5050 50  0001 C CNN
F 3 "https://www.rohm.com/datasheet/RK7002BM/rk7002bmt116-e" H 4450 4850 50  0001 C CNN
F 4 "Rohm Semiconductor" H 4900 5350 50  0001 C CNN "Manufacturer"
F 5 "RK7002BMT116 " H 4800 5150 50  0001 C CNN "Manufacturer Part Number"
F 6 "RK7002BMT116CT-ND" H 4950 5250 50  0001 C CNN "Digi-Key Part Number"
F 7 "~" H 2850 3150 50  0001 C CNN "Stuff"
F 8 "755-RK7002BMT116" H 2850 3150 50  0001 C CNN "Mouser Part Number"
	1    4250 4800
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BAFBB18
P 4450 4300
AR Path="/5B5FBC06/5BAFBB18" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBB18" Ref="R601"  Part="1" 
F 0 "R601" H 4382 4254 50  0000 R CNN
F 1 "10k" H 4382 4345 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4490 4290 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 4450 4300 50  0001 C CNN
F 4 "~" H 2850 3150 50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H 2850 3150 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 2850 3150 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H 2850 3150 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 2850 3150 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 2850 3150 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 2850 3150 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 2850 3150 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H 2850 3150 50  0001 C CNN "Mouser Part Number"
	1    4450 4300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 4000 4900 4050
Wire Wire Line
	4450 4150 4450 4050
Wire Wire Line
	4450 4050 4900 4050
Connection ~ 4900 4050
Wire Wire Line
	4900 4050 4900 4300
Wire Wire Line
	4450 4450 4450 4500
Wire Wire Line
	4450 4500 4600 4500
Wire Wire Line
	4450 4500 4350 4500
Wire Wire Line
	4350 4500 4350 4600
Connection ~ 4450 4500
$Comp
L eload:GNDD #PWR?
U 1 1 5BAFBB29
P 4350 5050
AR Path="/5B5FBC06/5BAFBB29" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BAFBB29" Ref="#PWR0604"  Part="1" 
F 0 "#PWR0604" H 4350 4800 50  0001 C CNN
F 1 "GNDD" H 4355 4877 50  0000 C CNN
F 2 "" H 4350 5050 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4350 5050 50  0001 C CNN
	1    4350 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 5050 4350 5000
Text HLabel 1650 7200 0    50   Output ~ 0
TEMP_SENSE
Text HLabel 3050 4800 0    50   Input ~ 0
FAN_SW
$Comp
L eload:FT24C04A U601
U 1 1 5C04C44F
P 2850 1600
F 0 "U601" H 2850 1750 50  0000 L CNN
F 1 "FT24C04A" H 2850 1650 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2850 1600 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21703d.pdf" H 2850 1600 50  0001 C CNN
F 4 "1219-1010-ND" H -5450 -3500 50  0001 C CNN "Digi-Key Part Number"
F 5 "Fremont Micro Devices USA" H -5450 -3500 50  0001 C CNN "Manufacturer"
F 6 "FT24C04A-USG-B" H -5450 -3500 50  0001 C CNN "Manufacturer Part Number"
F 7 "-40°C ~ 85°C (TA)" H -5450 -3500 50  0001 C CNN "Operating Temperature"
F 8 "~" H -3200 -3100 50  0001 C CNN "Stuff"
F 9 "556-AT24C01C-SSHM-B" H -3200 -3100 50  0001 C CNN "Mouser Part Number"
	1    2850 1600
	1    0    0    -1  
$EndComp
$Comp
L eload:R_US R616
U 1 1 5C04C456
P 2250 1600
F 0 "R616" H 2182 1509 50  0000 R CNN
F 1 "10k" H 2182 1600 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2290 1590 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 2250 1600 50  0001 C CNN
F 4 "DNS" H 2182 1691 50  0000 R CNN "Stuff"
F 5 " 541-3976-1-ND " H -3200 -3100 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H -3200 -3100 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H -3200 -3100 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -3200 -3100 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H -3200 -3100 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H -3200 -3100 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H -3200 -3100 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H -3200 -3100 50  0001 C CNN "Mouser Part Number"
	1    2250 1600
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R615
U 1 1 5C04C45D
P 1900 1600
F 0 "R615" H 1832 1509 50  0000 R CNN
F 1 "10k" H 1832 1600 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1940 1590 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1900 1600 50  0001 C CNN
F 4 "DNS" H 1832 1691 50  0000 R CNN "Stuff"
F 5 " 541-3976-1-ND " H -3200 -3100 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H -3200 -3100 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H -3200 -3100 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -3200 -3100 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H -3200 -3100 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H -3200 -3100 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H -3200 -3100 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H -3200 -3100 50  0001 C CNN "Mouser Part Number"
	1    1900 1600
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R614
U 1 1 5C04C464
P 1600 1600
F 0 "R614" H 1350 1500 50  0000 L CNN
F 1 "10k" H 1400 1600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1640 1590 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 1600 1600 50  0001 C CNN
F 4 "DNS" H 1400 1700 50  0000 L CNN "Stuff"
F 5 " 541-3976-1-ND " H -3200 -3100 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H -3200 -3100 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H -3200 -3100 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H -3200 -3100 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H -3200 -3100 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H -3200 -3100 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H -3200 -3100 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H -3200 -3100 50  0001 C CNN "Mouser Part Number"
	1    1600 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 1400 1600 1450
Wire Wire Line
	1900 1450 1900 1400
Connection ~ 1900 1400
Wire Wire Line
	1900 1400 1600 1400
Wire Wire Line
	2250 1450 2250 1400
Connection ~ 2250 1400
Wire Wire Line
	2250 1400 1900 1400
Wire Wire Line
	2250 1800 2250 1750
Wire Wire Line
	1900 1900 1900 1750
Wire Wire Line
	1600 2000 1600 1750
$Comp
L eload:GNDD #PWR0619
U 1 1 5C04C49C
P 3600 2050
F 0 "#PWR0619" H 3600 1800 50  0001 C CNN
F 1 "GNDD" H 3605 1877 50  0000 C CNN
F 2 "" H 3600 2050 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 3600 2050 50  0001 C CNN
	1    3600 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2000 3600 2000
Wire Wire Line
	3600 2000 3600 2050
Text Label 3750 1800 0    50   ~ 0
I2C_SCL
Text Label 3750 1700 0    50   ~ 0
I2C_SDA
NoConn ~ 3550 1900
Wire Wire Line
	2550 1700 2750 1700
$Comp
L eload:GNDD #PWR0610
U 1 1 5B743DBA
P 9200 2550
F 0 "#PWR0610" H 9200 2300 50  0001 C CNN
F 1 "GNDD" H 9205 2377 50  0000 C CNN
F 2 "" H 9200 2550 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 9200 2550 50  0001 C CNN
	1    9200 2550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9050 2500 9200 2500
Text HLabel 5750 1950 0    50   Input ~ 0
SD_MCU_SCK
Text HLabel 5750 1750 0    50   Input ~ 0
SD_MCU_DI
Text HLabel 5750 2150 0    50   Output ~ 0
SD_MCU_DO
Text HLabel 5750 1650 0    50   Input ~ 0
SD_MCU_CS
Text HLabel 5950 3300 0    50   Output ~ 0
SD_MCU_DET
Wire Wire Line
	9200 2500 9200 2550
$Comp
L eload:+3V3 #PWR?
U 1 1 5B7834DA
P 7200 1550
AR Path="/5B5FBC06/5B7834DA" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5B7834DA" Ref="#PWR0608"  Part="1" 
F 0 "#PWR0608" H 7200 1400 50  0001 C CNN
F 1 "+3V3" H 7205 1723 50  0000 C CNN
F 2 "" H 7200 1550 50  0001 C CNN
F 3 "" H 7200 1550 50  0001 C CNN
	1    7200 1550
	1    0    0    -1  
$EndComp
NoConn ~ 7350 1550
NoConn ~ 7350 2250
$Comp
L eload:GNDD #PWR0618
U 1 1 5B7A1F43
P 1200 1800
F 0 "#PWR0618" H 1200 1550 50  0001 C CNN
F 1 "GNDD" H 1205 1627 50  0000 C CNN
F 2 "" H 1200 1800 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 1200 1800 50  0001 C CNN
	1    1200 1800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6700 1200 6700 1150
$Comp
L eload:R_US R610
U 1 1 5B7CA48F
P 6200 3000
F 0 "R610" H 6268 3046 50  0000 L CNN
F 1 "10k" H 6268 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6240 2990 50  0001 C CNN
F 3 "http://www.vishay.com/docs/28773/crcwce3.pdf" H 6200 3000 50  0001 C CNN
F 4 "~" H 100 1050 50  0001 C CNN "Stuff"
F 5 " 541-3976-1-ND " H 100 1050 50  0001 C CNN "Digi-Key Part Number"
F 6 "Vishay Dale" H 100 1050 50  0001 C CNN "Manufacturer"
F 7 "CRCW080510K0FKEAC" H 100 1050 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 155°C" H 100 1050 50  0001 C CNN "Operating Temperature"
F 9 "0.125W, 1/8W " H 100 1050 50  0001 C CNN "Power"
F 10 "±100ppm/°C" H 100 1050 50  0001 C CNN "Temperature Coefficient"
F 11 "±1%" H 100 1050 50  0001 C CNN "Tolerance"
F 12 "GWCR0805-10KFT5 " H 100 1050 50  0001 C CNN "Mouser Part Number"
	1    6200 3000
	1    0    0    -1  
$EndComp
$Comp
L eload:C C602
U 1 1 5B7F2D8B
P 6700 1000
F 0 "C602" H 6815 1046 50  0000 L CNN
F 1 "0.1uF" H 6815 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6738 850 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 6700 1000 50  0001 C CNN
F 4 "399-1177-1-ND" H 1600 -950 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H 1600 -950 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H 1600 -950 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H 1600 -950 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H 1600 -950 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H 1600 -950 50  0001 C CNN "Tolerance"
F 10 "50V" H 1600 -950 50  0001 C CNN "Voltage"
F 11 "~" H 1600 -950 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H 1600 -950 50  0001 C CNN "Mouser Part Number"
	1    6700 1000
	1    0    0    -1  
$EndComp
$Comp
L eload:C C603
U 1 1 5B7F2F39
P 1200 1600
F 0 "C603" H 1085 1554 50  0000 R CNN
F 1 "0.1uF" H 1085 1645 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1238 1450 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0805C104Z5VACTU.pdf" H 1200 1600 50  0001 C CNN
F 4 "399-1177-1-ND" H -7050 -2900 50  0001 C CNN "Digi-Key Part Number"
F 5 "KEMET" H -7050 -2900 50  0001 C CNN "Manufacturer"
F 6 "C0805C104Z5VACTU" H -7050 -2900 50  0001 C CNN "Manufacturer Part Number"
F 7 "-30°C ~ 85°C" H -7050 -2900 50  0001 C CNN "Operating Temperature"
F 8 "Y5V (F)" H -7050 -2900 50  0001 C CNN "Temperature Coefficient"
F 9 "-20%, +80%" H -7050 -2900 50  0001 C CNN "Tolerance"
F 10 "50V" H -7050 -2900 50  0001 C CNN "Voltage"
F 11 "~" H -4550 -3850 50  0001 C CNN "Stuff"
F 12 "77-VJ0805Y104JXXAC" H -4550 -3850 50  0001 C CNN "Mouser Part Number"
	1    1200 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 1800 1200 1750
$Comp
L eload:+3V3 #PWR?
U 1 1 5BB01A3F
P 10150 1250
AR Path="/5B5FBC03/5BB01A3F" Ref="#PWR?"  Part="1" 
AR Path="/5B6C0220/5BB01A3F" Ref="#PWR?"  Part="1" 
AR Path="/5B87D636/5BB01A3F" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BB01A3F" Ref="#PWR0606"  Part="1" 
F 0 "#PWR0606" H 10150 1100 50  0001 C CNN
F 1 "+3V3" H 10155 1423 50  0000 C CNN
F 2 "" H 10150 1250 50  0001 C CNN
F 3 "" H 10150 1250 50  0001 C CNN
	1    10150 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 1250 10150 1350
Wire Wire Line
	10150 1700 10150 1650
$Comp
L eload:R_US R?
U 1 1 5BB01A4E
P 10150 1500
AR Path="/5B6010A5/5BB01A4E" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BB01A4E" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5BB01A4E" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5BB01A4E" Ref="R?"  Part="1" 
AR Path="/5B87D636/5BB01A4E" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BB01A4E" Ref="R604"  Part="1" 
F 0 "R604" H 10082 1409 50  0000 R CNN
F 1 "1k" H 10082 1500 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10190 1490 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 10150 1500 50  0001 C CNN
F 4 "Yageo" V 10150 1500 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 10150 1500 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 10150 1500 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 10082 1591 50  0000 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 10150 1500 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 10150 1500 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 10150 1500 50  0001 C CNN "Operating Temperature"
F 11 "~" H 150 200 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H 150 200 50  0001 C CNN "Mouser Part Number"
	1    10150 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6200 3300 6200 3150
Text Label 6600 3300 0    50   ~ 0
SD_MCU_DET
Text Label 10000 2300 2    50   ~ 0
SD_MCU_DET
Wire Wire Line
	10000 2300 10150 2300
Wire Wire Line
	10150 2000 10150 2300
Text HLabel 4300 1700 2    50   BiDi ~ 0
I2C_SDA
Text HLabel 4300 1800 2    50   BiDi ~ 0
I2C_SCL
$Comp
L eload:LED D?
U 1 1 5BCC9ADD
P 10150 1850
AR Path="/5B5FBC03/5BCC9ADD" Ref="D?"  Part="1" 
AR Path="/5B6C0220/5BCC9ADD" Ref="D?"  Part="1" 
AR Path="/5B5FBC06/5BCC9ADD" Ref="D?"  Part="1" 
AR Path="/5BCDC4C3/5BCC9ADD" Ref="D602"  Part="1" 
F 0 "D602" V 10188 1733 50  0000 R CNN
F 1 "BLUE" V 10097 1733 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Castellated" H 10150 1850 50  0001 C CNN
F 3 "~http://katalog.we-online.de/led/datasheet/150120BS75000.pdf" H 10150 1850 50  0001 C CNN
F 4 "20mA" H 7250 -650 50  0001 C CNN "Current"
F 5 "732-4989-1-ND" H 7250 -650 50  0001 C CNN "Digi-Key Part Number"
F 6 "Wurth Electronics Inc." H 7250 -650 50  0001 C CNN "Manufacturer"
F 7 "150120BS75000" H 7250 -650 50  0001 C CNN "Manufacturer Part Number"
F 8 "3.2V" H 7250 -650 50  0001 C CNN "Voltage"
F 9 "~" H 150 200 50  0001 C CNN "Stuff"
F 10 "710-150120BS75000" H 150 200 50  0001 C CNN "Mouser Part Number"
	1    10150 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2250 1400 2550 1400
Wire Wire Line
	2550 1400 2550 1700
Wire Wire Line
	2250 1800 2750 1800
Wire Wire Line
	1900 1900 2750 1900
Wire Wire Line
	1600 2000 2750 2000
Connection ~ 1600 1400
Wire Wire Line
	3550 1700 4300 1700
Wire Wire Line
	3550 1800 4300 1800
$Comp
L eload:GNDD #PWR0622
U 1 1 5B8531A0
P 4600 7200
F 0 "#PWR0622" H 4600 6950 50  0001 C CNN
F 1 "GNDD" H 4605 7027 50  0000 C CNN
F 2 "" H 4600 7200 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 4600 7200 50  0001 C CNN
	1    4600 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 7200 4600 7150
Wire Wire Line
	4600 7150 4700 7150
Text Label 4550 6850 2    50   ~ 0
I2C_SDA
Text Label 4550 6750 2    50   ~ 0
I2C_SCL
Wire Wire Line
	4700 6750 4550 6750
Wire Wire Line
	4700 6850 4550 6850
$Comp
L eload:R_US R?
U 1 1 5BA610BA
P 3400 4800
AR Path="/5B6010A5/5BA610BA" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BA610BA" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BA610BA" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BA610BA" Ref="R603"  Part="1" 
F 0 "R603" V 3350 4600 50  0000 C CNN
F 1 "100R" V 3350 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3440 4790 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 3400 4800 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 3400 4800 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 3400 4800 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 3400 4800 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 3400 4800 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 3400 4800 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 3400 4800 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 3400 4800 50  0001 C CNN "Operating Temperature"
F 11 "~" H -3300 2550 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H -3300 2550 50  0001 C CNN "Mouser Part Number"
	1    3400 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 4800 4050 4800
Wire Wire Line
	3050 4800 3250 4800
Text Label 3750 4800 0    50   ~ 0
FAN_SW_RG
Text Label 2300 1800 0    50   ~ 0
EEPROM_A0
Text Label 2300 1900 0    50   ~ 0
EEPROM_A1
Text Label 2300 2000 0    50   ~ 0
EEPROM_A2
Text Label 4350 4500 2    50   ~ 0
FAN_PMOS_SW
$Comp
L eload:+3V3 #PWR?
U 1 1 5BBC6621
P 1100 1400
AR Path="/5B5FBC06/5BBC6621" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BBC6621" Ref="#PWR0616"  Part="1" 
F 0 "#PWR0616" H 1100 1250 50  0001 C CNN
F 1 "+3V3" H 1105 1573 50  0000 C CNN
F 2 "" H 1100 1400 50  0001 C CNN
F 3 "" H 1100 1400 50  0001 C CNN
	1    1100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1400 1200 1400
Wire Wire Line
	1200 1450 1200 1400
Connection ~ 1200 1400
Wire Wire Line
	1200 1400 1600 1400
$Comp
L eload:+5V0 #PWR?
U 1 1 5BBD6C3A
P 3700 7000
AR Path="/5B6C0220/5BBD6C3A" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BBD6C3A" Ref="#PWR0621"  Part="1" 
F 0 "#PWR0621" H 3700 6850 50  0001 C CNN
F 1 "+5V0" H 3705 7173 50  0000 C CNN
F 2 "" H 3700 7000 50  0001 C CNN
F 3 "" H 3700 7000 50  0001 C CNN
	1    3700 7000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 7000 3700 7050
$Comp
L eload:Conn_01x05 J604
U 1 1 5BBDBC63
P 4900 6950
F 0 "J604" H 4980 6992 50  0000 L CNN
F 1 "Conn_01x05" H 4980 6901 50  0000 L CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-05A_1x05_P2.54mm_Vertical" H 4900 6950 50  0001 C CNN
F 3 "~" H 4900 6950 50  0001 C CNN
	1    4900 6950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 7050 4700 7050
$Comp
L eload:+3V3 #PWR?
U 1 1 5BBE7BA4
P 4000 6900
AR Path="/5B5FBC06/5BBE7BA4" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BBE7BA4" Ref="#PWR0620"  Part="1" 
F 0 "#PWR0620" H 4000 6750 50  0001 C CNN
F 1 "+3V3" H 4005 7073 50  0000 C CNN
F 2 "" H 4000 6900 50  0001 C CNN
F 3 "" H 4000 6900 50  0001 C CNN
	1    4000 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6900 4000 6950
Wire Wire Line
	4000 6950 4700 6950
Wire Wire Line
	5950 3300 6200 3300
$Comp
L eload:+3V3 #PWR?
U 1 1 5BBF8951
P 6200 2800
AR Path="/5B5FBC06/5BBF8951" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BBF8951" Ref="#PWR0612"  Part="1" 
F 0 "#PWR0612" H 6200 2650 50  0001 C CNN
F 1 "+3V3" H 6205 2973 50  0000 C CNN
F 2 "" H 6200 2800 50  0001 C CNN
F 3 "" H 6200 2800 50  0001 C CNN
	1    6200 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 2800 6200 2850
Wire Wire Line
	7350 2450 7200 2450
Wire Wire Line
	7200 2450 7200 3300
Connection ~ 6200 3300
Wire Wire Line
	6200 3300 7200 3300
Text HLabel 5100 5100 0    50   Output ~ 0
FAN_TACH
$Comp
L eload:R_US R?
U 1 1 5B85B910
P 6200 1650
AR Path="/5B6010A5/5B85B910" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B85B910" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B85B910" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5B85B910" Ref="R605"  Part="1" 
F 0 "R605" V 6150 1450 50  0000 C CNN
F 1 "100R" V 6150 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6240 1640 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 6200 1650 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 6200 1650 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 6200 1650 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 6200 1650 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 6200 1650 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 6200 1650 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 6200 1650 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 6200 1650 50  0001 C CNN "Operating Temperature"
F 11 "~" H 3600 -2550 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 3600 -2550 50  0001 C CNN "Mouser Part Number"
	1    6200 1650
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B85B920
P 6200 1750
AR Path="/5B6010A5/5B85B920" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B85B920" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B85B920" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5B85B920" Ref="R606"  Part="1" 
F 0 "R606" V 6150 1550 50  0000 C CNN
F 1 "100R" V 6150 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6240 1740 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 6200 1750 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 6200 1750 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 6200 1750 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 6200 1750 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 6200 1750 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 6200 1750 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 6200 1750 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 6200 1750 50  0001 C CNN "Operating Temperature"
F 11 "~" H 3600 -2550 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 3600 -2550 50  0001 C CNN "Mouser Part Number"
	1    6200 1750
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B85B930
P 6200 2150
AR Path="/5B6010A5/5B85B930" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B85B930" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B85B930" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5B85B930" Ref="R608"  Part="1" 
F 0 "R608" V 6150 1950 50  0000 C CNN
F 1 "100R" V 6150 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6240 2140 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 6200 2150 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 6200 2150 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 6200 2150 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 6200 2150 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 6200 2150 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 6200 2150 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 6200 2150 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 6200 2150 50  0001 C CNN "Operating Temperature"
F 11 "~" H 3600 -2250 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 3600 -2250 50  0001 C CNN "Mouser Part Number"
	1    6200 2150
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5B85B940
P 6200 1950
AR Path="/5B6010A5/5B85B940" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5B85B940" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5B85B940" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5B85B940" Ref="R607"  Part="1" 
F 0 "R607" V 6150 1750 50  0000 C CNN
F 1 "100R" V 6150 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6240 1940 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 6200 1950 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 6200 1950 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 6200 1950 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 6200 1950 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 6200 1950 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 6200 1950 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 6200 1950 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 6200 1950 50  0001 C CNN "Operating Temperature"
F 11 "~" H 3600 -2550 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 3600 -2550 50  0001 C CNN "Mouser Part Number"
	1    6200 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	6050 1650 5750 1650
Wire Wire Line
	6050 1750 5750 1750
Wire Wire Line
	6050 1950 5750 1950
Text Label 6550 1950 0    50   ~ 0
SD_MCU_SCK_R
Text Label 6550 2150 0    50   ~ 0
SD_MCU_DO_R
Text Label 6550 1750 0    50   ~ 0
SD_MCU_DI_R
Text Label 6550 1650 0    50   ~ 0
SD_MCU_CS_R
$Comp
L eload:GNDD #PWR0609
U 1 1 5B8A282F
P 7200 2200
F 0 "#PWR0609" H 7200 1950 50  0001 C CNN
F 1 "GNDD" H 7205 2027 50  0000 C CNN
F 2 "" H 7200 2200 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 7200 2200 50  0001 C CNN
	1    7200 2200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7350 2050 7200 2050
Wire Wire Line
	7200 2050 7200 2200
Wire Wire Line
	7350 2150 6350 2150
Wire Wire Line
	6350 1950 7350 1950
Wire Wire Line
	6350 1750 7350 1750
Wire Wire Line
	6350 1650 7350 1650
Wire Wire Line
	6050 2150 5750 2150
Wire Wire Line
	7350 1850 7200 1850
Wire Wire Line
	7200 1850 7200 1550
$Comp
L eload:C C?
U 1 1 5B8E8AF4
P 6300 1000
AR Path="/5B6C0220/5B8E8AF4" Ref="C?"  Part="1" 
AR Path="/5BCDC4C3/5B8E8AF4" Ref="C601"  Part="1" 
F 0 "C601" H 6415 1046 50  0000 L CNN
F 1 "1uF" H 6415 955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6338 850 50  0001 C CNN
F 3 "~" H 6300 1000 50  0001 C CNN
	1    6300 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 1200 6300 1150
$Comp
L eload:GNDD #PWR0605
U 1 1 5B8EB553
P 6150 1200
F 0 "#PWR0605" H 6150 950 50  0001 C CNN
F 1 "GNDD" H 6155 1027 50  0000 C CNN
F 2 "" H 6150 1200 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 6150 1200 50  0001 C CNN
	1    6150 1200
	-1   0    0    -1  
$EndComp
$Comp
L eload:+3V3 #PWR?
U 1 1 5B8EE359
P 6150 800
AR Path="/5B5FBC06/5B8EE359" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5B8EE359" Ref="#PWR0603"  Part="1" 
F 0 "#PWR0603" H 6150 650 50  0001 C CNN
F 1 "+3V3" H 6155 973 50  0000 C CNN
F 2 "" H 6150 800 50  0001 C CNN
F 3 "" H 6150 800 50  0001 C CNN
	1    6150 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 800  6300 800 
Wire Wire Line
	6700 800  6700 850 
Wire Wire Line
	6700 1200 6300 1200
Connection ~ 6300 1200
Wire Wire Line
	6300 1200 6150 1200
Wire Wire Line
	6300 850  6300 800 
Connection ~ 6300 800 
Wire Wire Line
	6300 800  6700 800 
$Comp
L eload:GNDA #PWR0115
U 1 1 5B8F46C4
P 2600 7200
F 0 "#PWR0115" H 2600 6950 50  0001 C CNN
F 1 "GNDA" H 2605 7027 50  0000 C CNN
F 2 "" H 2600 7200 50  0001 C CNN
F 3 "" H 2600 7200 50  0001 C CNN
	1    2600 7200
	1    0    0    -1  
$EndComp
$Comp
L eload:GNDA #PWR0116
U 1 1 5B8F47CC
P 1950 7600
F 0 "#PWR0116" H 1950 7350 50  0001 C CNN
F 1 "GNDA" H 1955 7427 50  0000 C CNN
F 2 "" H 1950 7600 50  0001 C CNN
F 3 "" H 1950 7600 50  0001 C CNN
	1    1950 7600
	1    0    0    -1  
$EndComp
$Comp
L eload:Conn_01x04 J601
U 1 1 5B9022DD
P 8300 4800
F 0 "J601" H 8380 4700 50  0000 L CNN
F 1 "Conn_01x04" H 8380 4791 50  0000 L CNN
F 2 "Connector:FanPinHeader_1x04_P2.54mm_Vertical" H 8300 4800 50  0001 C CNN
F 3 "~" H 8300 4800 50  0001 C CNN
	1    8300 4800
	1    0    0    1   
$EndComp
$Comp
L eload:GNDD #PWR?
U 1 1 5BB082A5
P 8000 5300
AR Path="/5B5FBC06/5BB082A5" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BB082A5" Ref="#PWR0611"  Part="1" 
F 0 "#PWR0611" H 8000 5050 50  0001 C CNN
F 1 "GNDD" H 8005 5127 50  0000 C CNN
F 2 "" H 8000 5300 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 8000 5300 50  0001 C CNN
	1    8000 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 4900 8000 4900
Wire Wire Line
	8000 4900 8000 5250
Wire Wire Line
	7800 5200 7800 5250
Wire Wire Line
	7800 5250 8000 5250
Wire Wire Line
	8000 5250 8000 5300
Connection ~ 8000 5250
$Comp
L eload:D_Schottky D?
U 1 1 5BB082BA
P 7800 5050
AR Path="/5B6C0220/5BB082BA" Ref="D?"  Part="1" 
AR Path="/5B5FBD8A/5BB082BA" Ref="D?"  Part="1" 
AR Path="/5BCDC4C3/5BB082BA" Ref="D601"  Part="1" 
F 0 "D601" V 7754 4972 50  0000 R CNN
F 1 "DSS13UTR" V 7845 4972 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-123F" H 7800 5050 50  0001 C CNN
F 3 "http://www.smc-diodes.com/propdf/DSS12U%20THRU%20DSS125U%20N1873%20REV.A.pdf" H 7800 5050 50  0001 C CNN
F 4 "1A" H -1000 2150 50  0001 C CNN "Current"
F 5 "1655-1926-1-ND" H -1000 2150 50  0001 C CNN "Digi-Key Part Number"
F 6 "SMC Diode Solutions" H -1000 2150 50  0001 C CNN "Manufacturer"
F 7 "DSS13UTR" H -1000 2150 50  0001 C CNN "Manufacturer Part Number"
F 8 "-55°C ~ 150°C" H -1000 2150 50  0001 C CNN "Operating Temperature"
F 9 "30V" H -1000 2150 50  0001 C CNN "Voltage"
F 10 "~" H -1000 2150 50  0001 C CNN "Stuff"
F 11 "841-PMEG4010EGWX" H -1000 2150 50  0001 C CNN "Mouser Part Number"
	1    7800 5050
	0    1    1    0   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BB15ECF
P 5850 4500
AR Path="/5B6010A5/5BB15ECF" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BB15ECF" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5BB15ECF" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5BB15ECF" Ref="R?"  Part="1" 
AR Path="/5B87D636/5BB15ECF" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BB15ECF" Ref="R609"  Part="1" 
F 0 "R609" H 5782 4409 50  0000 R CNN
F 1 "1k" H 5782 4500 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5890 4490 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 5850 4500 50  0001 C CNN
F 4 "Yageo" V 5850 4500 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 5850 4500 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 5850 4500 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 5782 4591 50  0001 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 5850 4500 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 5850 4500 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 5850 4500 50  0001 C CNN "Operating Temperature"
F 11 "~" H -4150 3200 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -4150 3200 50  0001 C CNN "Mouser Part Number"
	1    5850 4500
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R?
U 1 1 5BB1897C
P 7400 4400
AR Path="/5B6010A5/5BB1897C" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BB1897C" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5BB1897C" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5BB1897C" Ref="R?"  Part="1" 
AR Path="/5B87D636/5BB1897C" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BB1897C" Ref="R602"  Part="1" 
F 0 "R602" H 7332 4309 50  0000 R CNN
F 1 "1k" H 7332 4400 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7440 4390 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 7400 4400 50  0001 C CNN
F 4 "Yageo" V 7400 4400 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 7400 4400 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 7400 4400 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 7332 4491 50  0001 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7400 4400 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7400 4400 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7400 4400 50  0001 C CNN "Operating Temperature"
F 11 "~" H -2600 3100 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -2600 3100 50  0001 C CNN "Mouser Part Number"
	1    7400 4400
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 4600 7400 4550
$Comp
L eload:+3V3 #PWR?
U 1 1 5BB1B8B4
P 7400 4200
AR Path="/5B5FBC06/5BB1B8B4" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BB1B8B4" Ref="#PWR0602"  Part="1" 
F 0 "#PWR0602" H 7400 4050 50  0001 C CNN
F 1 "+3V3" H 7405 4373 50  0000 C CNN
F 2 "" H 7400 4200 50  0001 C CNN
F 3 "" H 7400 4200 50  0001 C CNN
	1    7400 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4200 7400 4250
Text HLabel 6700 4600 0    50   Input ~ 0
FAN_PWM
$Comp
L eload:+12V0 #PWR?
U 1 1 5BB21FBE
P 5850 4250
AR Path="/5B5FBC06/5BB21FBE" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BB21FBE" Ref="#PWR0607"  Part="1" 
F 0 "#PWR0607" H 5850 4100 50  0001 C CNN
F 1 "+12V0" H 5855 4423 50  0000 C CNN
F 2 "" H 5850 4250 50  0001 C CNN
F 3 "" H 5850 4250 50  0001 C CNN
	1    5850 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4250 5850 4350
$Comp
L eload:R_US R?
U 1 1 5BB359BD
P 5400 5350
AR Path="/5B6010A5/5BB359BD" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BB359BD" Ref="R?"  Part="1" 
AR Path="/5B5FBC03/5BB359BD" Ref="R?"  Part="1" 
AR Path="/5B6C0220/5BB359BD" Ref="R?"  Part="1" 
AR Path="/5B87D636/5BB359BD" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BB359BD" Ref="R618"  Part="1" 
F 0 "R618" H 5332 5259 50  0000 R CNN
F 1 "1k" H 5332 5350 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5440 5340 50  0001 C CNN
F 3 "http://www.yageo.com/documents/recent/PYu-RC_Group_51_RoHS_L_9.pdf" V 5400 5350 50  0001 C CNN
F 4 "Yageo" V 5400 5350 50  0001 C CNN "Manufacturer"
F 5 "RC0805FR-071KL" V 5400 5350 50  0001 C CNN "Manufacturer Part Number"
F 6 "311-1.00KCRCT-ND" V 5400 5350 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" H 5332 5441 50  0001 R CNN "Tolerance"
F 8 "0.125W, 1/8W " V 5400 5350 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 5400 5350 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 5400 5350 50  0001 C CNN "Operating Temperature"
F 11 "~" H -4600 4050 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-1K0FT5" H -4600 4050 50  0001 C CNN "Mouser Part Number"
	1    5400 5350
	-1   0    0    1   
$EndComp
$Comp
L eload:R_US R617
U 1 1 5BB36346
P 5650 5100
F 0 "R617" V 5445 5100 50  0000 C CNN
F 1 "2k" V 5536 5100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5690 5090 50  0001 C CNN
F 3 "~" H 5650 5100 50  0001 C CNN
	1    5650 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 4700 5850 5100
Wire Wire Line
	5500 5100 5400 5100
Wire Wire Line
	5400 5100 5400 5200
$Comp
L eload:GNDD #PWR?
U 1 1 5BB4F18A
P 5400 5550
AR Path="/5B5FBC06/5BB4F18A" Ref="#PWR?"  Part="1" 
AR Path="/5BCDC4C3/5BB4F18A" Ref="#PWR0613"  Part="1" 
F 0 "#PWR0613" H 5400 5300 50  0001 C CNN
F 1 "GNDD" H 5405 5377 50  0000 C CNN
F 2 "" H 5400 5550 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX11612-MAX11617.pdf" H 5400 5550 50  0001 C CNN
	1    5400 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 5500 5400 5550
Wire Wire Line
	5100 5100 5400 5100
Connection ~ 5400 5100
Text Label 6300 4700 0    50   ~ 0
FAN_TACH_IN
Text Label 6300 4800 0    50   ~ 0
FAN_PWR
Wire Wire Line
	7800 4900 7800 4800
Wire Wire Line
	7800 4800 8100 4800
Wire Wire Line
	5800 5100 5850 5100
$Comp
L eload:R_US R?
U 1 1 5BB74658
P 7050 4600
AR Path="/5B6010A5/5BB74658" Ref="R?"  Part="1" 
AR Path="/5B5FBD8A/5BB74658" Ref="R?"  Part="1" 
AR Path="/5B5FBC06/5BB74658" Ref="R?"  Part="1" 
AR Path="/5BCDC4C3/5BB74658" Ref="R611"  Part="1" 
F 0 "R611" V 7000 4400 50  0000 C CNN
F 1 "100R" V 7000 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7090 4590 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/chip-resistor/RC2012F101CS.jsp" H 7050 4600 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" V 7050 4600 50  0001 C CNN "Manufacturer"
F 5 "RC2012F101CS" V 7050 4600 50  0001 C CNN "Manufacturer Part Number"
F 6 "1276-5224-1-ND" V 7050 4600 50  0001 C CNN "Digi-Key Part Number"
F 7 "±1%" V 7050 4600 50  0001 C CNN "Tolerance"
F 8 "0.125W, 1/8W " V 7050 4600 50  0001 C CNN "Power"
F 9 "±100ppm/°C" V 7050 4600 50  0001 C CNN "Temperature Coefficient"
F 10 "-55°C ~ 155°C" V 7050 4600 50  0001 C CNN "Operating Temperature"
F 11 "~" H 50  1100 50  0001 C CNN "Stuff"
F 12 "756-GWCR0805-100RFT5" H 50  1100 50  0001 C CNN "Mouser Part Number"
	1    7050 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	7200 4600 7400 4600
Connection ~ 7400 4600
Wire Wire Line
	6900 4600 6700 4600
Wire Wire Line
	5850 4700 5850 4650
Connection ~ 5850 4700
Text Label 7550 4600 0    50   ~ 0
FAN_PWM_OUT
Wire Wire Line
	7400 4600 8100 4600
Wire Wire Line
	5850 4700 8100 4700
Wire Wire Line
	4900 4800 7800 4800
Connection ~ 7800 4800
$EndSCHEMATC
